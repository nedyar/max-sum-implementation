\BOOKMARK [0][-]{chapter.1}{Introduction}{}% 1
\BOOKMARK [1][-]{section.1.1}{Key Features}{chapter.1}% 2
\BOOKMARK [1][-]{section.1.2}{Other Java Frameworks}{chapter.1}% 3
\BOOKMARK [2][-]{subsection.1.2.1}{Watchmaker Framework}{section.1.2}% 4
\BOOKMARK [2][-]{subsection.1.2.2}{ECJ}{section.1.2}% 5
\BOOKMARK [2][-]{subsection.1.2.3}{jMetal}{section.1.2}% 6
\BOOKMARK [2][-]{subsection.1.2.4}{Opt4J}{section.1.2}% 7
\BOOKMARK [2][-]{subsection.1.2.5}{Others}{section.1.2}% 8
\BOOKMARK [1][-]{section.1.3}{Reporting Bugs}{chapter.1}% 9
\BOOKMARK [1][-]{section.1.4}{Getting Help}{chapter.1}% 10
\BOOKMARK [-1][-]{part.1}{I Beginner's Guide - Installing and Using the MOEA Framework}{}% 11
\BOOKMARK [0][-]{chapter.2}{Installation Instructions}{part.1}% 12
\BOOKMARK [1][-]{section.2.1}{Understanding the License}{chapter.2}% 13
\BOOKMARK [1][-]{section.2.2}{Which Distribution is Right for Me?}{chapter.2}% 14
\BOOKMARK [1][-]{section.2.3}{Obtaining a Copy}{chapter.2}% 15
\BOOKMARK [1][-]{section.2.4}{Installing Dependencies}{chapter.2}% 16
\BOOKMARK [2][-]{subsection.2.4.1}{Java 6+ \(Required\)}{section.2.4}% 17
\BOOKMARK [2][-]{subsection.2.4.2}{Eclipse or NetBeans \(Optional\)}{section.2.4}% 18
\BOOKMARK [2][-]{subsection.2.4.3}{Apache Ant \(Optional\)}{section.2.4}% 19
\BOOKMARK [1][-]{section.2.5}{Importing into Eclipse}{chapter.2}% 20
\BOOKMARK [1][-]{section.2.6}{Importing into NetBeans}{chapter.2}% 21
\BOOKMARK [1][-]{section.2.7}{Testing your Installation}{chapter.2}% 22
\BOOKMARK [1][-]{section.2.8}{Distribution Contents}{chapter.2}% 23
\BOOKMARK [2][-]{subsection.2.8.1}{Compiled Binary Contents}{section.2.8}% 24
\BOOKMARK [2][-]{subsection.2.8.2}{Source Code Contents}{section.2.8}% 25
\BOOKMARK [1][-]{section.2.9}{Conclusion}{chapter.2}% 26
\BOOKMARK [0][-]{chapter.3}{Executor, Instrumenter, Analyzer}{part.1}% 27
\BOOKMARK [1][-]{section.3.1}{Executor}{chapter.3}% 28
\BOOKMARK [1][-]{section.3.2}{Instrumenter}{chapter.3}% 29
\BOOKMARK [1][-]{section.3.3}{Analyzer}{chapter.3}% 30
\BOOKMARK [1][-]{section.3.4}{Conclusion}{chapter.3}% 31
\BOOKMARK [0][-]{chapter.4}{Diagnostic Tool}{part.1}% 32
\BOOKMARK [1][-]{section.4.1}{Running the Diagnostic Tool}{chapter.4}% 33
\BOOKMARK [1][-]{section.4.2}{Layout of the GUI}{chapter.4}% 34
\BOOKMARK [1][-]{section.4.3}{Quantile Plots vs Individual Traces}{chapter.4}% 35
\BOOKMARK [1][-]{section.4.4}{Viewing Approximation Set Dynamics}{chapter.4}% 36
\BOOKMARK [1][-]{section.4.5}{Statistical Results}{chapter.4}% 37
\BOOKMARK [1][-]{section.4.6}{Advanced Use}{chapter.4}% 38
\BOOKMARK [2][-]{subsection.4.6.1}{Improving Performance and Memory Efficiency}{section.4.6}% 39
\BOOKMARK [0][-]{chapter.5}{Defining New Problems}{part.1}% 40
\BOOKMARK [1][-]{section.5.1}{Java}{chapter.5}% 41
\BOOKMARK [1][-]{section.5.2}{Referencing the Problem}{chapter.5}% 42
\BOOKMARK [2][-]{subsection.5.2.1}{By Class}{section.5.2}% 43
\BOOKMARK [2][-]{subsection.5.2.2}{By Class Name}{section.5.2}% 44
\BOOKMARK [2][-]{subsection.5.2.3}{By Name}{section.5.2}% 45
\BOOKMARK [2][-]{subsection.5.2.4}{With a ProblemProvider}{section.5.2}% 46
\BOOKMARK [2][-]{subsection.5.2.5}{With the global.properties File}{section.5.2}% 47
\BOOKMARK [1][-]{section.5.3}{C/C++}{chapter.5}% 48
\BOOKMARK [1][-]{section.5.4}{Scripting Language}{chapter.5}% 49
\BOOKMARK [1][-]{section.5.5}{Conclusion}{chapter.5}% 50
\BOOKMARK [0][-]{chapter.6}{Representing Decision Variables}{part.1}% 51
\BOOKMARK [1][-]{section.6.1}{Floating-Point Values}{chapter.6}% 52
\BOOKMARK [1][-]{section.6.2}{Integers}{chapter.6}% 53
\BOOKMARK [1][-]{section.6.3}{Bit Strings}{chapter.6}% 54
\BOOKMARK [1][-]{section.6.4}{Permutations}{chapter.6}% 55
\BOOKMARK [1][-]{section.6.5}{Programs \(Expression Trees\)}{chapter.6}% 56
\BOOKMARK [1][-]{section.6.6}{Grammars}{chapter.6}% 57
\BOOKMARK [1][-]{section.6.7}{Variation Operators}{chapter.6}% 58
\BOOKMARK [2][-]{subsection.6.7.1}{Initialization}{section.6.7}% 59
\BOOKMARK [2][-]{subsection.6.7.2}{Variation \(Mutation \046 Crossover\)}{section.6.7}% 60
\BOOKMARK [1][-]{section.6.8}{Conclusion}{chapter.6}% 61
\BOOKMARK [-1][-]{part.2}{II Advanced Guide - Large-Scale Experiments and other Advanced Topics}{}% 62
\BOOKMARK [0][-]{chapter.7}{Comparative Studies}{part.2}% 63
\BOOKMARK [1][-]{section.7.1}{What are Comparative Studies?}{chapter.7}% 64
\BOOKMARK [2][-]{subsection.7.1.1}{Problem Domain}{section.7.1}% 65
\BOOKMARK [2][-]{subsection.7.1.2}{Goals of Optimization}{section.7.1}% 66
\BOOKMARK [2][-]{subsection.7.1.3}{Parameterization}{section.7.1}% 67
\BOOKMARK [2][-]{subsection.7.1.4}{Algorithm Selection}{section.7.1}% 68
\BOOKMARK [1][-]{section.7.2}{Executing Commands}{chapter.7}% 69
\BOOKMARK [1][-]{section.7.3}{Parameter Description File}{chapter.7}% 70
\BOOKMARK [1][-]{section.7.4}{Generating Parameter Samples}{chapter.7}% 71
\BOOKMARK [1][-]{section.7.5}{Evaluation}{chapter.7}% 72
\BOOKMARK [1][-]{section.7.6}{Check Completion}{chapter.7}% 73
\BOOKMARK [1][-]{section.7.7}{Reference Set Generation}{chapter.7}% 74
\BOOKMARK [1][-]{section.7.8}{Metric Calculation}{chapter.7}% 75
\BOOKMARK [1][-]{section.7.9}{Averaging Metrics}{chapter.7}% 76
\BOOKMARK [1][-]{section.7.10}{Analysis}{chapter.7}% 77
\BOOKMARK [2][-]{subsection.7.10.1}{Best}{section.7.10}% 78
\BOOKMARK [2][-]{subsection.7.10.2}{Attainment}{section.7.10}% 79
\BOOKMARK [2][-]{subsection.7.10.3}{Efficiency}{section.7.10}% 80
\BOOKMARK [1][-]{section.7.11}{Set Contribution}{chapter.7}% 81
\BOOKMARK [1][-]{section.7.12}{Sobol Analysis}{chapter.7}% 82
\BOOKMARK [1][-]{section.7.13}{Example Script File \(Unix/Linux\)}{chapter.7}% 83
\BOOKMARK [1][-]{section.7.14}{PBS Job Scripting \(Unix\)}{chapter.7}% 84
\BOOKMARK [1][-]{section.7.15}{Troubleshooting}{chapter.7}% 85
\BOOKMARK [0][-]{chapter.8}{Advanced Topics}{part.2}% 86
\BOOKMARK [1][-]{section.8.1}{PISA Integration}{chapter.8}% 87
\BOOKMARK [2][-]{subsection.8.1.1}{Adding a PISA Selector}{section.8.1}% 88
\BOOKMARK [2][-]{subsection.8.1.2}{Troubleshooting}{section.8.1}% 89
\BOOKMARK [2][-]{subsection.8.1.3}{Customization}{section.8.1}% 90
\BOOKMARK [1][-]{section.8.2}{Configuring Hypervolume Calculation}{chapter.8}% 91
\BOOKMARK [1][-]{section.8.3}{Storing Large Datasets}{chapter.8}% 92
\BOOKMARK [2][-]{subsection.8.3.1}{Writing Result Files}{section.8.3}% 93
\BOOKMARK [2][-]{subsection.8.3.2}{Extract Information from Result Files}{section.8.3}% 94
\BOOKMARK [1][-]{section.8.4}{Dealing with Maximized Objectives}{chapter.8}% 95
\BOOKMARK [-1][-]{part.3}{III Developer's Guide - Extending and Contributing to the MOEA Framework}{}% 96
\BOOKMARK [0][-]{chapter.9}{Developer Guide}{part.3}% 97
\BOOKMARK [1][-]{section.9.1}{Version Numbers}{chapter.9}% 98
\BOOKMARK [1][-]{section.9.2}{Release Cycle}{chapter.9}% 99
\BOOKMARK [1][-]{section.9.3}{API Deprecation}{chapter.9}% 100
\BOOKMARK [1][-]{section.9.4}{Code Style}{chapter.9}% 101
\BOOKMARK [1][-]{section.9.5}{Licensing}{chapter.9}% 102
\BOOKMARK [1][-]{section.9.6}{Web Presence}{chapter.9}% 103
\BOOKMARK [1][-]{section.9.7}{Ways to Contribute}{chapter.9}% 104
\BOOKMARK [2][-]{subsection.9.7.1}{Translations}{section.9.7}% 105
\BOOKMARK [0][-]{chapter.10}{Errors and Warning Messages}{part.3}% 106
\BOOKMARK [1][-]{section.10.1}{Errors}{chapter.10}% 107
\BOOKMARK [1][-]{section.10.2}{Warnings}{chapter.10}% 108
\BOOKMARK [0][-]{chapter*.22}{Credits}{part.3}% 109
\BOOKMARK [0][-]{chapter*.23}{GNU Free Documentation License}{part.3}% 110
