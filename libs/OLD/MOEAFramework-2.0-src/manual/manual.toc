\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Key Features}{2}{section.1.1}
\contentsline {paragraph}{Fast, reliable implementations of many state-of-the-art multiobjective evolutionary algorithms.}{2}{section*.6}
\contentsline {paragraph}{Extensible with custom algorithms, problems and operators.}{2}{section*.7}
\contentsline {paragraph}{Modular design for constructing new optimization algorithms from existing components.}{2}{section*.8}
\contentsline {paragraph}{Permissive open source license.}{2}{section*.9}
\contentsline {paragraph}{Fully documented source code.}{2}{section*.10}
\contentsline {paragraph}{Extensive support available online.}{3}{section*.11}
\contentsline {paragraph}{Over 1000 test cases to ensure validity.}{3}{section*.12}
\contentsline {section}{\numberline {1.2}Other Java Frameworks}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Watchmaker Framework}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}ECJ}{4}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}jMetal}{5}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Opt4J}{5}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}Others}{6}{subsection.1.2.5}
\contentsline {section}{\numberline {1.3}Reporting Bugs}{6}{section.1.3}
\contentsline {section}{\numberline {1.4}Getting Help}{7}{section.1.4}
\contentsline {part}{I\hspace {1em}Beginner's Guide - Installing and Using the MOEA Framework}{9}{part.1}
\contentsline {chapter}{\numberline {2}Installation Instructions}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Understanding the License}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Which Distribution is Right for Me?}{12}{section.2.2}
\contentsline {paragraph}{Compiled Binaries}{12}{section*.13}
\contentsline {paragraph}{All-in-One Executable}{12}{section*.14}
\contentsline {paragraph}{Source Code}{12}{section*.15}
\contentsline {section}{\numberline {2.3}Obtaining a Copy}{12}{section.2.3}
\contentsline {section}{\numberline {2.4}Installing Dependencies}{13}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Java 6+ (Required)}{13}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Eclipse or NetBeans (Optional)}{14}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Apache Ant (Optional)}{14}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}Importing into Eclipse}{15}{section.2.5}
\contentsline {section}{\numberline {2.6}Importing into NetBeans}{15}{section.2.6}
\contentsline {section}{\numberline {2.7}Testing your Installation}{17}{section.2.7}
\contentsline {paragraph}{Compiled Binaries}{17}{section*.16}
\contentsline {paragraph}{All-in-One Executable}{17}{section*.17}
\contentsline {paragraph}{Source Code}{17}{section*.18}
\contentsline {section}{\numberline {2.8}Distribution Contents}{17}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Compiled Binary Contents}{17}{subsection.2.8.1}
\contentsline {subsection}{\numberline {2.8.2}Source Code Contents}{19}{subsection.2.8.2}
\contentsline {section}{\numberline {2.9}Conclusion}{20}{section.2.9}
\contentsline {chapter}{\numberline {3}Executor, Instrumenter, Analyzer}{21}{chapter.3}
\contentsline {section}{\numberline {3.1}Executor}{21}{section.3.1}
\contentsline {section}{\numberline {3.2}Instrumenter}{26}{section.3.2}
\contentsline {section}{\numberline {3.3}Analyzer}{30}{section.3.3}
\contentsline {section}{\numberline {3.4}Conclusion}{34}{section.3.4}
\contentsline {chapter}{\numberline {4}Diagnostic Tool}{35}{chapter.4}
\contentsline {section}{\numberline {4.1}Running the Diagnostic Tool}{35}{section.4.1}
\contentsline {paragraph}{Compiled Binaries}{35}{section*.19}
\contentsline {paragraph}{All-in-One Executable}{35}{section*.20}
\contentsline {paragraph}{Source Code}{36}{section*.21}
\contentsline {section}{\numberline {4.2}Layout of the GUI}{36}{section.4.2}
\contentsline {section}{\numberline {4.3}Quantile Plots vs Individual Traces}{37}{section.4.3}
\contentsline {section}{\numberline {4.4}Viewing Approximation Set Dynamics}{40}{section.4.4}
\contentsline {section}{\numberline {4.5}Statistical Results}{42}{section.4.5}
\contentsline {section}{\numberline {4.6}Advanced Use}{44}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Improving Performance and Memory Efficiency}{44}{subsection.4.6.1}
\contentsline {chapter}{\numberline {5}Defining New Problems}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}Java}{46}{section.5.1}
\contentsline {section}{\numberline {5.2}Referencing the Problem}{48}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}By Class}{48}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}By Class Name}{49}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}By Name}{49}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}With a ProblemProvider}{50}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}With the \texttt {global.properties} File}{51}{subsection.5.2.5}
\contentsline {section}{\numberline {5.3}C/C++}{52}{section.5.3}
\contentsline {section}{\numberline {5.4}Scripting Language}{57}{section.5.4}
\contentsline {section}{\numberline {5.5}Conclusion}{59}{section.5.5}
\contentsline {chapter}{\numberline {6}Representing Decision Variables}{61}{chapter.6}
\contentsline {section}{\numberline {6.1}Floating-Point Values}{61}{section.6.1}
\contentsline {section}{\numberline {6.2}Integers}{63}{section.6.2}
\contentsline {section}{\numberline {6.3}Bit Strings}{64}{section.6.3}
\contentsline {section}{\numberline {6.4}Permutations}{65}{section.6.4}
\contentsline {section}{\numberline {6.5}Programs (Expression Trees)}{66}{section.6.5}
\contentsline {section}{\numberline {6.6}Grammars}{68}{section.6.6}
\contentsline {section}{\numberline {6.7}Variation Operators}{70}{section.6.7}
\contentsline {subsection}{\numberline {6.7.1}Initialization}{71}{subsection.6.7.1}
\contentsline {subsection}{\numberline {6.7.2}Variation (Mutation \& Crossover)}{71}{subsection.6.7.2}
\contentsline {section}{\numberline {6.8}Conclusion}{74}{section.6.8}
\contentsline {part}{II\hspace {1em}Advanced Guide - Large-Scale Experiments and other Advanced Topics}{75}{part.2}
\contentsline {chapter}{\numberline {7}Comparative Studies}{77}{chapter.7}
\contentsline {section}{\numberline {7.1}What are Comparative Studies?}{77}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Problem Domain}{78}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Goals of Optimization}{78}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}Parameterization}{78}{subsection.7.1.3}
\contentsline {subsection}{\numberline {7.1.4}Algorithm Selection}{79}{subsection.7.1.4}
\contentsline {section}{\numberline {7.2}Executing Commands}{79}{section.7.2}
\contentsline {section}{\numberline {7.3}Parameter Description File}{81}{section.7.3}
\contentsline {section}{\numberline {7.4}Generating Parameter Samples}{81}{section.7.4}
\contentsline {section}{\numberline {7.5}Evaluation}{82}{section.7.5}
\contentsline {section}{\numberline {7.6}Check Completion}{83}{section.7.6}
\contentsline {section}{\numberline {7.7}Reference Set Generation}{84}{section.7.7}
\contentsline {section}{\numberline {7.8}Metric Calculation}{84}{section.7.8}
\contentsline {section}{\numberline {7.9}Averaging Metrics}{85}{section.7.9}
\contentsline {section}{\numberline {7.10}Analysis}{85}{section.7.10}
\contentsline {subsection}{\numberline {7.10.1}Best}{86}{subsection.7.10.1}
\contentsline {subsection}{\numberline {7.10.2}Attainment}{86}{subsection.7.10.2}
\contentsline {subsection}{\numberline {7.10.3}Efficiency}{87}{subsection.7.10.3}
\contentsline {section}{\numberline {7.11}Set Contribution}{87}{section.7.11}
\contentsline {section}{\numberline {7.12}Sobol Analysis}{88}{section.7.12}
\contentsline {section}{\numberline {7.13}Example Script File (Unix/Linux)}{91}{section.7.13}
\contentsline {section}{\numberline {7.14}PBS Job Scripting (Unix)}{94}{section.7.14}
\contentsline {section}{\numberline {7.15}Troubleshooting}{95}{section.7.15}
\contentsline {chapter}{\numberline {8}Advanced Topics}{101}{chapter.8}
\contentsline {section}{\numberline {8.1}PISA Integration}{101}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Adding a PISA Selector}{103}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}Troubleshooting}{104}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}Customization}{105}{subsection.8.1.3}
\contentsline {section}{\numberline {8.2}Configuring Hypervolume Calculation}{106}{section.8.2}
\contentsline {section}{\numberline {8.3}Storing Large Datasets}{107}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Writing Result Files}{108}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Extract Information from Result Files}{109}{subsection.8.3.2}
\contentsline {section}{\numberline {8.4}Dealing with Maximized Objectives}{111}{section.8.4}
\contentsline {part}{III\hspace {1em}Developer's Guide - Extending and Contributing to the MOEA Framework}{113}{part.3}
\contentsline {chapter}{\numberline {9}Developer Guide}{115}{chapter.9}
\contentsline {section}{\numberline {9.1}Version Numbers}{115}{section.9.1}
\contentsline {section}{\numberline {9.2}Release Cycle}{116}{section.9.2}
\contentsline {section}{\numberline {9.3}API Deprecation}{116}{section.9.3}
\contentsline {section}{\numberline {9.4}Code Style}{117}{section.9.4}
\contentsline {section}{\numberline {9.5}Licensing}{121}{section.9.5}
\contentsline {section}{\numberline {9.6}Web Presence}{121}{section.9.6}
\contentsline {section}{\numberline {9.7}Ways to Contribute}{122}{section.9.7}
\contentsline {subsection}{\numberline {9.7.1}Translations}{122}{subsection.9.7.1}
\contentsline {chapter}{\numberline {10}Errors and Warning Messages}{125}{chapter.10}
\contentsline {section}{\numberline {10.1}Errors}{125}{section.10.1}
\contentsline {section}{\numberline {10.2}Warnings}{134}{section.10.2}
\contentsline {chapter}{Credits}{139}{chapter*.22}
\contentsline {chapter}{GNU Free Documentation License}{141}{chapter*.23}
