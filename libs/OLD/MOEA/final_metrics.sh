#!/bin/bash

# ./final_metrics.sh <APPROX_SET> <REF_SET> <NUM_OBJ> <OUT_FILE>
# ./final_metrics.sh Yriver_basic_BORG_1.out Yriver_basic.reference 3 prova.txt

# input-output
INFILE=$1
REF_FILE=$2
NOBJ=$3
OUTFILE=$4


java ${MOEA_ARGS} ${METRICS_ARGS} --reference ${REF_FILE}  --input ${INFILE} -d ${NOBJ} --output ${OUTFILE} --force
