package xml;

import java.util.List;

import org.jdom2.Element;

public class XmlScope extends Element {
	private static final long serialVersionUID = 1068916152276249727L;

	public XmlScope(List<String> variablesScope) {
		super("scope");
		for (String string : variablesScope) {
			XmlVar xmlVar = new XmlVar(string);
			this.addContent(xmlVar);
		}
	}
}