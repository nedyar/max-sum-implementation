package xml;

import org.jdom2.Element;

public class XmlVariable extends Element {
	private static final long serialVersionUID = 6487536150803367808L;

	public XmlVariable(String name, int position, String domain){
		super("variable");
		this.setAttribute("name", name);
		String positionStr = "" + position;
		this.setAttribute("pos", positionStr);
		XmlDomain xmlDomain = new XmlDomain(domain);
		this.addContent(xmlDomain);
	}	
}
