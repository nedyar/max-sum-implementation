package xml;

import org.jdom2.Element;

public class XmlDomain extends Element {
	private static final long serialVersionUID = -2222434682886040610L;

	public XmlDomain(String domain){
		super("domain");
		this.setText(domain);
	}
}
