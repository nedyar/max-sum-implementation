package xml;

import org.jdom2.Element;

public class XmlVar extends Element {
	private static final long serialVersionUID = -7770131283211813635L;

	public XmlVar(String variableName) {
		super("var");
		this.setText(variableName);
	}	
}
