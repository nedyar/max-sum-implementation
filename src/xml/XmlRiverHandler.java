package xml;

import graph.FunctionCompact;
import graph.FunctionCompactHard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jdom2.Comment;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jgrapht.graph.DefaultWeightedEdge;

public class XmlRiverHandler extends XmlHandler {
	private Element cuttedEdgesEl;
	private Element variables;
	private Element functions;

	/**
	 * Load an xml file of existent model
	 * @param xmlFilePath
	 * @throws JDOMException
	 * @throws IOException
	 */
	public XmlRiverHandler(String xmlFilePath) throws JDOMException, IOException{
		super(xmlFilePath);
		variables = new Element("variablesDefault");
		functions = new Element("functionsDefault");
		cuttedEdgesEl = null;
	}

	public void loadXmlFile() throws JDOMException, IOException{
		super.loadXmlFile();
		this.variables = root.getChild("variables");
		this.functions = root.getChild("functions");
	}

	/**
	 * Create a new model in a xml file
	 * @param path
	 * @param objectives
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public void createModel (int objectives) {
		super.root = new Element("graph");
		super.root.setAttribute("objectives", "" + objectives);
		super.document.addContent(root);
		variables = new Element("variables");
		functions = new Element("functions");
		super.root.addContent(variables);
		super.root.addContent(functions);			
	}

	/**
	 * Create a new model given a existent river structure
	 * @param path
	 * @param objectives
	 * @param riverStructure
	 * @param riverNodes
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	//per scrivere nell'xml anche la struttura del fiume
	public void createModelFromRiver (int objectives, String riverStructure, String riverNodes) {
		super.root = new Element("graph");

		super.root.setAttribute("objectives", "" + objectives);

		Comment comment =  new Comment("River Structure: " + riverStructure);
		Comment comment2 = new Comment("Nodes: " + riverNodes);
		super.root.addContent(comment);
		super.root.addContent(comment2);
		super.document.addContent(root);
		variables = new Element("variables");
		functions = new Element("functions");
		super.root.addContent(variables);
		super.root.addContent(functions);		
	}

	public void addFunction(String name, int position, int arity,List<String> variablesScope, List<String> utilitiesExpr, List<String> booleansExpr){
		XmlFunction xmlFunction = new XmlFunction(name, position, arity, variablesScope, utilitiesExpr, booleansExpr);
		this.functions.addContent(xmlFunction);
	}

	public void addVariable(String name, int position, String domain){
		XmlVariable xmlVariable = new XmlVariable(name, position, domain);
		this.variables.addContent(xmlVariable);
	}	

	public void createAcyclicXml(Set<DefaultWeightedEdge> cuttedEdges, String newPath) throws IOException{
		if (cuttedEdgesEl == null){
			cuttedEdgesEl = new Element("cutted_edges");
			super.root.addContent(cuttedEdgesEl);			
		}

		for (DefaultWeightedEdge defaultWeightedEdge : cuttedEdges) {
			//Aggiungo al modello xml gli archi tagliati
			Element edgeEl = new Element("edge");
			edgeEl.setText(defaultWeightedEdge.toString());
			cuttedEdgesEl.addContent(edgeEl);

			String tempLinkedNodes = defaultWeightedEdge.toString();

			//elimino parentesi e spazi vuoti
			tempLinkedNodes = tempLinkedNodes.substring(1, tempLinkedNodes.length() - 1);			
			tempLinkedNodes = tempLinkedNodes.replace(" ", "");

			//ottengo sorgente e destinazione in formato stringa
			String linkedNodes[] = tempLinkedNodes.split(":");

			//messo cos� non � il massimo, ma dovrebbe funzionare cmq
			String variable;
			String function;
			if (linkedNodes[0].length() > 0 && linkedNodes[0].charAt(0) == 'x') {
				variable = linkedNodes[0];
				function = linkedNodes[1];
			}
			else {
				variable = linkedNodes[1];
				function = linkedNodes[0];
			}

			Element functionEl = this.findFunction(function);
			/*
			 * decremento arity
			 * elimino variabile dallo scope
			 * elimino variabile dalle espressioni, compreso il segno affianco!!! (trick: sostituisco con 0)
			 */
			int arity = Integer.parseInt(functionEl.getAttribute("arity").getValue());
			arity--;
			functionEl.setAttribute("arity", "" + arity);

			Element scope = functionEl.getChild("scope");
			List<Element> vars = scope.getChildren("var");
			Element toRemove = null;
			for (Element element : vars) {
				if (element.getText().equals(variable)){
					toRemove = element;
				}
			}
			scope.removeContent(toRemove);

			Element utilities = functionEl.getChild("utilities");
			List<Element> exprs = utilities.getChildren("expr");
			for (Element element : exprs) {
				String value = element.getText();
				value = value.replaceAll(variable, "0");
				element.setText(value);
			}

			Element booleans = functionEl.getChild("booleans");
			exprs = null;
			exprs = booleans.getChildren("expr");
			for (Element element : exprs) {
				String value = element.getText();
				value = value.replaceAll(variable, "0");
				element.setText(value);
			}
			super.saveDocument(newPath);			
		}
	}

	private Element findFunction(String functionName){
		Element out = null;
		List<Element> functionChildrens = functions.getChildren("function");
		for (Element element : functionChildrens) {
			String tempName = element.getAttribute("name").getValue();
			if (tempName.equals(functionName)){
				out = element;
			}
		}
		return out;
	}

	public ArrayList<String> getVariables(){
		ArrayList<String> out = new ArrayList<String>();

		List<Element> variablesChildren = variables.getChildren("variable");
		for (Element element : variablesChildren) {
			String tempName = element.getAttribute("name").getValue();			
			out.add(tempName);			
		}
		return out;
	}

	public ArrayList<FunctionCompact> getFunctions(){
		ArrayList<FunctionCompact> out = new ArrayList<FunctionCompact>();

		List<Element> functionChildrens = functions.getChildren("function");
		for (Element element : functionChildrens) {
			String tempName = element.getAttribute("name").getValue();

			Element booleansEl = element.getChild("booleans");
			List<Element> booleanElements = booleansEl.getChildren("expr");
			String expression = booleansEl.getChildText("expr");

			Element utilitiesEl = element.getChild("utilities");
			List<Element> utilityElements = utilitiesEl.getChildren("expr");

			String hardExpression = null;
			FunctionCompact functionCompact;

			for (int i = 0; i < utilityElements.size(); i++) {
				Element exprEl = utilityElements.get(i);
				try {
					if (Double.parseDouble(exprEl.getText()) < -10000.0){
						hardExpression = booleanElements.get(i).getText();
					}
				}
				catch (NumberFormatException e){

				}
			}

			if (hardExpression != null){
				functionCompact = new FunctionCompactHard(tempName,  this.getFunctionScope(element), expression, hardExpression);
			}
			else {
				functionCompact = new FunctionCompact(tempName, this.getFunctionScope(element), expression);
			}
			out.add(functionCompact);
		}
		return out;
	}

	private ArrayList<String> getFunctionScope(Element functionEl){
		ArrayList<String> out = new ArrayList<String>();

		List<Element> scopeChildrens = functionEl.getChild("scope").getChildren("var");
		for (Element element : scopeChildrens) {
			String tempName = element.getText();			
			out.add(tempName);			
		}
		return out;
	}

	public ArrayList<String> getFunctionScope(String functionName){
		List<Element> children = functions.getChildren();
		for (Element el : children) {

			if (el.getAttributeValue("name").equals(functionName)){
				return getFunctionScope(el);
			}
		}
		return null;
	}

	public ArrayList<String> getCuttedEdges(){
		ArrayList<String> out = new ArrayList<String>();
		try{
			if (cuttedEdgesEl == null){
				cuttedEdgesEl = root.getChild("cutted_edges");
			}
			List<Element> cuttedEdges = this.cuttedEdgesEl.getChildren("edge");
			for (Element element : cuttedEdges) {
				out.add(element.getText());
			}
		}
		catch (NullPointerException e){
		}
		return out;		
	}

	public String getFunctionExpression(String functionName){
		ArrayList<FunctionCompact> functions = getFunctions();
		for (FunctionCompact functionCompact : functions) {
			if (functionCompact.getFunctionName().equals(functionName)){
				return functionCompact.getExpression(); 
			}
		}		
		return null;
	}

	public String getHardFunctionExpression(String functionName) {
		ArrayList<FunctionCompact> functions = getFunctions();
		for (FunctionCompact functionCompact : functions) {
			if (functionCompact instanceof FunctionCompactHard){
				if (functionCompact.getFunctionName().equals(functionName)){
					return ((FunctionCompactHard) functionCompact).getHardExpression(); 
				}

			}
		}		
		return null;
	}
}
