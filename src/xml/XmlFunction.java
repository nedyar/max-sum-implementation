package xml;

import java.util.List;

import org.jdom2.Element;

public class XmlFunction extends Element {
	private static final long serialVersionUID = -6689929759407513218L;

	public XmlFunction(String name, int position, int arity,List<String> variablesScope, List<String> utilitiesExpr, List<String> booleansExpr) {
		super("function");
		String positionStr = "" + position;
		String arityStr = "" + arity;
		this.setAttribute("name", name);
		this.setAttribute("pos", positionStr);
		this.setAttribute("arity", arityStr);
		
		this.addContent(new XmlScope(variablesScope));
		this.addContent(new XmlUtilities(utilitiesExpr));
		this.addContent(new XmlBooleans(booleansExpr));			
	}	
}
