package xml;

import java.io.IOException;

import org.jdom2.Element;
import org.jdom2.JDOMException;

import math.RangeInt;
import parameters.FlowType;
import riverTree.NodeType;

public class XMLParamHandler extends XmlHandler {
	
	public XMLParamHandler(String xmlFilePath) {
		super(xmlFilePath);
		try {
			super.loadXmlFile();
		} catch (JDOMException | IOException e) {
			e.printStackTrace();
		}
	}

	public double getLevel(FlowType flowType){
		String valueStr = super.root.getChild("dams").getChild("level").getChildText(flowType.toString());
		return Double.parseDouble(valueStr);
	}
	
	//valore portata iniziale
	public  double getInitialFlow(FlowType flowType, int agents){
		String valueStr = super.root.getChild("initialFlow").getChildText(flowType.toString());
		return Double.parseDouble(valueStr);
	}
	
	private Element getAgentEl(NodeType nodeType){
		String elementName;
		switch (nodeType) {
		case CITY:
			elementName = "cities";
			break;
		case DAM:
			elementName = "dams";
			break;
		default:
			elementName = "";
			break;
		}
		return super.root.getChild(elementName);
	}
	
	//valori funzione obbiettivo
	public  double getA(NodeType nodeType){
		String valueStr = this.getAgentEl(nodeType).getChildText("a");				
		return Double.parseDouble(valueStr);
	}	
	
	public  double getB(NodeType nodeType){	
		String valueStr = this.getAgentEl(nodeType).getChildText("b");				
		return Double.parseDouble(valueStr);		
	}
	
	public  double getC(NodeType nodeType){	
		String valueStr = this.getAgentEl(nodeType).getChildText("c");				
		return Double.parseDouble(valueStr);		
	}
	
	//valore portata minima necessaria ai nodi citt�
	public  double getMinDrain(){
		String valueStr = this.getAgentEl(NodeType.CITY).getChildText("minDrain");				
		return Double.parseDouble(valueStr);		
	}
	
	//valore portata minima in uscita da un nodo
	public double getMinOut(){
		String valueStr = root.getChildText("minOut");				
		return Double.parseDouble(valueStr);		
	}

	private Element getRiverEl(){
		return root.getChild("river");
	}	
	
	public int getDomainSize() {
		String valueStr = this.getRiverEl().getChildText("domainSize");
		return Integer.parseInt(valueStr);
	}

	public int getNumberOfIterations() {
		String valueStr = this.getRiverEl().getChildText("numberOfIterations");
		return Integer.parseInt(valueStr);
	}

	public RangeInt getNumberOfCities() {
		String minStr = this.getRiverEl().getChild("numberOfCities").getChildText("min");
		String maxStr = this.getRiverEl().getChild("numberOfCities").getChildText("max");
		return new RangeInt(Integer.parseInt(minStr), Integer.parseInt(maxStr));
	}

	public RangeInt getNumberOfDams() {
		String minStr = this.getRiverEl().getChild("numberOfDams").getChildText("min");
		String maxStr = this.getRiverEl().getChild("numberOfDams").getChildText("max");
		return new RangeInt(Integer.parseInt(minStr), Integer.parseInt(maxStr));
	}

	public RangeInt getNumberOfRivers() {
		String minStr = this.getRiverEl().getChild("numberOfRivers").getChildText("min");
		String maxStr = this.getRiverEl().getChild("numberOfRivers").getChildText("max");
		return new RangeInt(Integer.parseInt(minStr), Integer.parseInt(maxStr));
	}	
}
