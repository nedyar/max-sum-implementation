package xml;

import org.jdom2.Element;

public class XmlExpr extends Element {
	private static final long serialVersionUID = -146525148299979588L;

	public XmlExpr(String expression) {
		super("expr");
		this.setText(expression);
	}
}
