package xml;

import java.io.FileWriter;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class XmlHandler {
	String path;	
	protected Document document;
	protected Element root;		

	/**
	 * Create an empty 
	 * @param xmlFilePath
	 */
	public XmlHandler(String xmlFilePath) {
		path = xmlFilePath;
		document = new Document();
		root = new Element("defaultXml");
	}

	public void loadXmlFile() throws JDOMException, IOException{
		SAXBuilder builder = new SAXBuilder();

		document = builder.build(path);
		root = document.getRootElement();
	}

	public void saveDocument() {
		saveDocument(path);
	}

	public void saveDocument(String xmlPath) {
		try {
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			FileWriter fileWriter = new FileWriter(xmlPath);
			xmlOutput.output(document, fileWriter);
		} catch (IOException e) {
			System.err.println("Impossibile salvare il file " + path);
		}
	}	
}