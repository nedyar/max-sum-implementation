package xml;

import java.util.List;

import org.jdom2.Element;

public class XmlBooleans extends Element {
	private static final long serialVersionUID = 7393204828490758136L;

	public XmlBooleans(List<String> booleansExpr){
		super("booleans");
		for (String string : booleansExpr) {
			XmlExpr xmlExpr = new XmlExpr(string);
			this.addContent(xmlExpr);
		}
	}
}
