package xml;

import java.util.List;

import org.jdom2.Element;

public class XmlUtilities extends Element {
	private static final long serialVersionUID = 6250469278893557949L;

	public XmlUtilities(List<String> utilitesExpr){
		super("utilities");
		for (String string : utilitesExpr) {
			XmlExpr xmlExpr = new XmlExpr(string);
			this.addContent(xmlExpr);
		}
	}
}
