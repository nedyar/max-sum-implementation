package xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Element;
import org.jdom2.JDOMException;

import graph.MSTweightCriterion;
import hypervolume.HyperTest;
import hypervolume.OptimizationType;

public class XmlOptimizationHandler extends XmlHandler {
	private HashMap<MSTweightCriterion, Element> criterionRoot;

	public XmlOptimizationHandler(String xmlFilePath) {
		super(xmlFilePath);
		criterionRoot = new HashMap<MSTweightCriterion, Element>();
	}

	public void loadXmlFile() {
		try{
			super.loadXmlFile();
			if (!super.root.getName().equals("OptimizationLog")) {
				this.createXmlFile();
			}
			else{
				for (MSTweightCriterion criterion : MSTweightCriterion.values()) {

					Element paretoEl = super.root.getChild("pareto_" + criterion.toString());

					if (paretoEl != null){
						criterionRoot.put(criterion, paretoEl);
					}
					else {
						paretoEl = new Element("pareto_" + criterion.toString());
						super.root.addContent(paretoEl);
						criterionRoot.put(criterion, paretoEl);
					}
				}
			}
		}
		catch (JDOMException | IOException e) {
			this.createXmlFile();
		}
	}
	public void createXmlFile () {
		super.root = new Element("OptimizationLog");
		super.document.addContent(root);

		for (MSTweightCriterion criterion : MSTweightCriterion.values()) {
			Element pareto = new Element("pareto_" + criterion.toString());
			criterionRoot.put(criterion, pareto);
			super.root.addContent(pareto);
		}
		this.saveDocument();
	}

	public void addPareto(ArrayList<ArrayList<Float>> solutionVectors, ArrayList<ArrayList<Float>> variableTuples,
			MSTweightCriterion mstCriterion){

		Element pareto = criterionRoot.get(mstCriterion);
		Element stringPareto = new Element("string_EnricoOutput");	
		Element utilityPareto = new Element("utility");
		Element costPareto = new Element("cost");

		if (solutionVectors.size() == variableTuples.size()){
			for (int i = 0; i <= variableTuples.size() - 1; i++) {
				String row = "x = " + variableTuples.get(i) + ", s = " + solutionVectors.get(i); 
				Element rowEl = new Element("row");
				rowEl.setText(row);
				stringPareto.addContent(rowEl);			
			}
		}

		//Creates a string containing for each row a Pareto element with values separated by whitespace
		String frontierUtility = "";
		String frontierCost = "";
		for (ArrayList<Float> arrayList : solutionVectors) {
			for (Float float1 : arrayList) {
				frontierUtility = frontierUtility + float1 + " ";
				frontierCost = frontierCost + (float1 * (-1)) + " ";
			}
			frontierUtility = frontierUtility + "\n";
			frontierCost = frontierCost + "\n";
		}

		//add text to specific xml part
		utilityPareto.setText(frontierUtility);
		utilityPareto.setAttribute("elements", "" + solutionVectors.size());
		costPareto.setText(frontierCost);	
		costPareto.setAttribute("elements", "" + solutionVectors.size());		

		pareto.addContent(stringPareto);
		pareto.addContent(utilityPareto);
		pareto.addContent(costPareto);		

		super.saveDocument();
	}	

	public void addOptimizationExecutionTimeInSec(double execTime, MSTweightCriterion criterion){
		Element criterionEl = criterionRoot.get(criterion);

		Element execTimeEl = new Element("B-MOMS_Execution_time_in_seconds");
		execTimeEl.setText("" + execTime);
		criterionEl.addContent(execTimeEl);
	}

	public void addMSTexecutionTimeInSec(double execTime, MSTweightCriterion criterion){			
		Element criterionEl = criterionRoot.get(criterion);

		Element mstExecTimeEl = new Element("MST_Execution_time_in_seconds");
		mstExecTimeEl.setText("" + execTime);
		criterionEl.addContent(mstExecTimeEl);
	}

	public void addNumberOfMessages(MSTweightCriterion criterion, int number){
		Element criterionEl = criterionRoot.get(criterion);

		Element numbersEl = new Element("messages_numbers");
		numbersEl.setText("" + number);
		criterionEl.addContent(numbersEl);

		super.saveDocument();
	}

	//Check if the cost of a specified criterion it's already calculated
	public boolean alreadyComputed(MSTweightCriterion criterion) {
		Element criterionEl = criterionRoot.get(criterion);
		try {
			Element costEl = criterionEl.getChild("cost");
			costEl.getAttribute("elements");
			return true;
		}
		catch (NullPointerException e){
			return false;
		}
	}	

	public List<String> getFrontierEnrico(MSTweightCriterion weightCriterion){
		List<String> out = new ArrayList<String>();

		Element element = super.root.getChild("pareto_"  + weightCriterion.toString());
		try{
			List<Element> rows = element.getChild("string_EnricoOutput").getChildren("row");

			for (Element row : rows) {
				out.add(row.getText());
			}
		}
		catch (NullPointerException e){

		}
		return out;
	}

	public double[][] getFrontierCost(MSTweightCriterion weightCriterion,
			OptimizationType optimizationType) throws NumberFormatException{
		Element element = super.root.getChild("pareto_"  + weightCriterion.toString());

		String temp = optimizationType.toString();
		Element costEl = element.getChild(temp);

		if (costEl == null){
			System.err.println("Soluzioni non trovate per " + super.path + " " + weightCriterion + " " +
					optimizationType.toString());
			return null;
		}

		Attribute attr = costEl.getAttribute("elements");
		if (attr != null){
			int elements = Integer.parseInt(attr.getValue());
			if (costEl == null || elements == 0){
				return null;
			}
		}
		else {
			System.err.println("Soluzioni non trovate per " + super.path + " " + weightCriterion + " " +
					optimizationType.toString());
		}

		String rows[] = costEl.getText().split("\n");
		String tempRow[] = rows[0].split(" ");

		int cols = tempRow.length;
		double out[][] = new double[rows.length][cols];

		int i = 0;
		for (String string : rows) {
			String row[] = string.split(" ");
			int j = 0;
			for (String string2 : row) {
				try{
					out[i][j] = Double.parseDouble(string2);
				}
				catch (NumberFormatException e){
					System.err.println(this.path);
				}
				j++;
			}
			i++;		
		}		
		return out;
	}

	public void changeHypervolume(MSTweightCriterion criterion, String hypervolume, boolean feasible) 
			throws IOException {

		String elementName;
		if (feasible){
			elementName = "feasible_hypervolume";
		}
		else {
			elementName = "hypervolume";
		}
		changeTextElement(criterion, hypervolume, elementName);	
	}

	public void changeGenerationalDistance(MSTweightCriterion criterion,
			String genDistStr, boolean feasible) {
		String elementName;
		if (feasible){
			elementName = "feasible_generational_distance";
		}
		else {
			elementName = "generational_distance";
		}
		changeTextElement(criterion, genDistStr, elementName);		
	}

	public void changeEpsilonIndicator(MSTweightCriterion criterion,
			String epsilonStr, boolean feasible) {
		String elementName;
		if (feasible){
			elementName = "feasible_epsilon_indicator";
		}
		else {
			elementName = "epsilon_indicator";
		}
		changeTextElement(criterion, epsilonStr, elementName);			
	}

	public String getNumberOfSolution(MSTweightCriterion criterion, OptimizationType type/*, boolean feasible*/){
		String elementName = type.toString();
		Element critEl = super.root.getChild("pareto_" + criterion.toString());

		Element costEl = critEl.getChild(elementName);

		if (costEl != null){
			return costEl.getAttributeValue("elements");
		}
		return null;
	}

	public String retriveStringHypervolume(MSTweightCriterion criterion, boolean feasible){
		String out = null;
		String elementName;
		if (feasible){
			elementName = "feasible_hypervolume";
		}
		else {
			elementName = "hypervolume";
		}
		Element critEl = super.root.getChild("pareto_" + criterion.toString());

		Element hyperEl = critEl.getChild(elementName);

		if (hyperEl != null){
			out = hyperEl.getText();
		}		
		return out;
	}

	public String retriveGenericMeasureString(String elementName, MSTweightCriterion criterion){
		String out = null;
		Element critEl = super.root.getChild("pareto_" + criterion.toString());

		Element hyperEl = critEl.getChild(elementName);

		if (hyperEl != null){
			out = hyperEl.getText();
		}		
		return out;
	}


	public Double retriveHypervolume(MSTweightCriterion criterion, boolean feasible){
		Double out = null;
		out = Double.parseDouble(this.retriveStringHypervolume(criterion, feasible));		
		return out;
	}	

	public void addFeasibleCosts(MSTweightCriterion criterion, double costs[][]) throws IOException{
		Element critEl = super.root.getChild("pareto_" + criterion.toString());

		Element feasibleCostEl = critEl.getChild("feasible_cost");
		if (feasibleCostEl == null){
			feasibleCostEl = new Element("feasible_cost");
			critEl.addContent(feasibleCostEl);
		}
		if (costs == null) {
			feasibleCostEl.setAttribute("elements", "0");
		}
		else {
			feasibleCostEl.setAttribute("elements", "" + costs.length);
			feasibleCostEl.setText(this.getStringParetoRepresentation(costs));
		}
		super.saveDocument();
	}

	public void addViolations(MSTweightCriterion criterion, List<Float> violations) throws IOException{
		Element critEl = super.root.getChild("pareto_" + criterion.toString());

		Element violationsEl = critEl.getChild("violations");
		if (violationsEl == null){
			violationsEl = new Element("violations");
			critEl.addContent(violationsEl);
		}

		Element violationEl = new Element("v");
		violationEl.setText(violations.toString());

		violationsEl.addContent(violationEl);
		super.saveDocument();
	}

	private String getStringParetoRepresentation(double[][] matrix){
		String out = "";
		for (double ds[] : matrix) {
			for (double d : ds) {
				out = out + d + " ";
			}
			out = out + "\n";
		}
		return out;
	}

	public void clearViolations() {
		//Non dovrei aver creato violazioni nel caso aciclico
		for (MSTweightCriterion criterion : MSTweightCriterion.values2()){
			try{
				Element critEl = super.root.getChild("pareto_" + criterion.toString());

				Element violationsEl = critEl.getChild("violations");
				if (violationsEl != null){

					violationsEl.removeChildren("v");
				}
			}
			catch (NullPointerException e){
			}
		}
		super.saveDocument();
	}

	public void addMean(MSTweightCriterion criterion, double mean) {
		changeTextElement(criterion, "" + mean, "mean_violation");
	}

	public void addStandardDeviation(MSTweightCriterion criterion, double stDev) {
		changeTextElement(criterion, "" + stDev, "stDev_violation");
	}

	public void addMaxViolation(MSTweightCriterion criterion, double max) {
		changeTextElement(criterion, "" + max, "max_violation");
	}

	private void changeTextElement(MSTweightCriterion criterion, String text, String elementName) {
		Element critEl = super.root.getChild("pareto_" + criterion.toString());

		Element element = critEl.getChild(elementName);
		if (element == null) {
			element = new Element(elementName);
			critEl.addContent(element);
		}
		element.setText(text);
		super.saveDocument();
	}

	public String getMetric(MSTweightCriterion criterion, String metricStr) {
		Element critEl = super.root.getChild("pareto_" + criterion.toString());
		Element element = critEl.getChild(metricStr);
		if (element != null) {
			return element.getText();
		}		
		return null;
	}

	public void clearCriterionSection(MSTweightCriterion mstCriterion) {
		Element critEl = super.root.getChild("pareto_" + mstCriterion.toString());
		critEl.removeContent();	
		super.saveDocument();
	}

	public void addBfExecutionTimeInSec(double execTimeSec) {
		Element bfTimeEl = root.getChild("Bf_exec_time_in_seconds");
		if (bfTimeEl == null){
			bfTimeEl = new Element("Bf_exec_time_in_seconds");
			root.addContent(bfTimeEl);
		}
		bfTimeEl.setText("" + execTimeSec);		
		super.saveDocument();
	}

	public void changeHyperTest(MSTweightCriterion criterion,
			List<HyperTest> tests, boolean feasible) {
		Element critEl = super.root.getChild("pareto_" + criterion.toString());
		String elementName;
		if (feasible){
			elementName = "feasible_hyper_tests";
		}
		else {
			elementName = "hyper_tests";
		}

		Element hyperTestEl = critEl.getChild(elementName);

		if (hyperTestEl == null){
			hyperTestEl = new Element(elementName);
			critEl.addContent(hyperTestEl);
		}

		//elimino il contenuto vecchio
		hyperTestEl.removeContent();

		for (HyperTest hyperTest : tests) {
			Element rowEl = new Element("row");
			rowEl.setAttribute(new Attribute("col_index", hyperTest.getColIndex().toString()));

			rowEl.setText(hyperTest.getRowVariableNamesStr());

			hyperTestEl.addContent(rowEl);
		}
		super.saveDocument();
	}

	public String getBruteforceExecTime() {
		try{
			return root.getChild("Bf_exec_time_in_seconds").getText();
		}
		catch (NullPointerException e){
			return null;
		}
	}

	public boolean isAcyclic() {
		Element acyclicEl = criterionRoot.get(MSTweightCriterion.ACYCLIC);

		try {
			Element costEl = acyclicEl.getChild("cost");
			String elementsStr = costEl.getAttribute("elements").getValue();
			if (Integer.parseInt(elementsStr) >= 0){
				return true;
			}
		}
		catch (NullPointerException e) {
			//do nothing and so return false
		}
		return false;
	}

	public void addDominance(MSTweightCriterion criterion, Boolean dominated) {

		this.changeTextElement(criterion, dominated.toString(), "bf_dominates");		
	}
}
