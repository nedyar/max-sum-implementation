package Debug;

import java.util.List;

public class Debug {
	
	private static boolean debugMode = true;
	
	/**
	 * If {@code print} is true this method will print to standard output the {@code message} given as parameter, 
	 * otherwise nothing will be printed.
	 * @param debugMessage The message that have to be printed.
	 * @param print Value that state if the local debug mode is active. It means that if it's {@code true} the message
	 *  will be printed for sure. 
	 */
	public static void print(String debugMessage, boolean print){		
		if (print){
			System.out.println("### DEBUG:  " + debugMessage);
		}
	}
	
	/**
	 * If global debug mode is on this method print to standard output the {@code message} given as parameter, 
	 * otherwise nothing will be printed.
	 * @param debugMessage The message that have to be printed.
	 */
	public static void print(String debugMessage){
		if (debugMode){
			System.out.println("### DEBUG:  " + debugMessage);
		}
	}
	
	public static void print(List<?> list) {
		for (Object object : list) {
			Debug.print(object.toString());
		}
	}
	
	public static void print(double[][] costs){
		for (double[] cost : costs) {
			Debug.print(cost.toString());
		}
	}
}
