package Debug;

import java.util.Vector;

/**
 * Contain information about messages exchanged by threads
 * @author Stefano
 *
 */
public class Messages {
	private Vector<String> qMessages;
	private Vector<String> rMessages;	
	
	public Messages() {
		qMessages = new Vector<String>();
		rMessages = new Vector<String>();
	} 
		
	public Vector<String> getqMessages() {
		return qMessages;
	}

	public Vector<String> getrMessages() {
		return rMessages;
	}

	public synchronized void addQMessage(String message){
		qMessages.add(message);
	}
	
	public synchronized void addRMessage(String message){
		rMessages.add(message);
	}
	
	public synchronized int getRNumbers(){
		return rMessages.size();
	}
	
	public synchronized int getQNumbers(){
		return qMessages.size();
	}
	
	public synchronized int getNumbers(){
		return rMessages.size() + qMessages.size();
	}
}
