package Debug;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class Log {
	private String path;
	private Document document;
	protected Element rootEl;
	private Element messagesEl;

	public Log(String path) {		
		this.path = path;
		document = new Document();		
		rootEl = new Element("OptimizationLog");

		rootEl.setAttribute("name", "TODO");
		messagesEl = new Element("messages");

		rootEl.addContent(messagesEl);		
		document.addContent(rootEl);		

		try {
			this.saveDocument();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Save the xml file into the field <i>path</i>
	 * @throws IOException
	 */
	protected void saveDocument() throws IOException{
		XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat());
		FileWriter fileWriter = new FileWriter(path);
		xmlOutput.output(document, fileWriter);
	}

	/**
	 * Add an xml element called "Message" using the parameter as text
	 * @param message The message to add
	 */
	public void addMessage(String message) {
		this.messagesEl.addContent(new Element("Message").setText(message));
		try {
			this.saveDocument();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	

	/**
	 * Add various xml elements called "Message" using the parameter as text
	 * @param message The List of messages to add
	 */
	public  void addMessages(List<String> messages) {
		for (String message : messages) {
			this.messagesEl.addContent(new Element("Message").setText(message));
		}
		try {
			this.saveDocument();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Clean the file deleting all the old contents.
	 */
	public void cleanFile() {
		BufferedWriter bufferedWriter;
		try {
			bufferedWriter = new BufferedWriter(new FileWriter(this.path));
			PrintWriter printerWriter = new PrintWriter(bufferedWriter);
			printerWriter.append("");
			printerWriter.flush();
			printerWriter.close();	

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * If a log is generated but useless it can be deleted.
	 * @return True if the deletion worked correctly.
	 */
	public boolean deleteLog(){
		File logFile = new File(path);
		return logFile.delete();
	}
}
