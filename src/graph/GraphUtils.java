package graph;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import javax.xml.transform.TransformerConfigurationException;

import org.jgrapht.Graph;
import org.jgrapht.ext.EdgeNameProvider;
import org.jgrapht.ext.GraphMLExporter;
import org.jgrapht.ext.VertexNameProvider;
import org.xml.sax.SAXException;

public class GraphUtils<V, E> {
	private Graph<V, E> graph;	
	
	public GraphUtils(Graph<V, E> graph) {
		super();
		this.graph = graph;
	}

	public void export(String path) throws TransformerConfigurationException, SAXException, IOException{
		Writer writer = new FileWriter(path + ".graphml");
		VertexNameProvider<V> labelVNP = new MyVertexLabelNameProvider<V>();
		EdgeNameProvider<E> labelENP = new MyEdgeLabelNameProvider<E>();

		GraphMLExporter<V, E> exporter = new GraphMLExporter<V, E>();
		exporter = new GraphMLExporter<>(labelVNP, labelVNP, labelENP, labelENP);
		exporter.export(writer, graph);
	}
}
