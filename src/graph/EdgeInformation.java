package graph;

import org.jgrapht.graph.DefaultWeightedEdge;

public class EdgeInformation {
	private DefaultWeightedEdge edge;
	private String variableName;
	private FunctionCompact functionCompact;
		
	public EdgeInformation(DefaultWeightedEdge edge, String variableName, FunctionCompact functionCompact) {
		super();
		this.edge = edge;
		this.variableName = variableName;
		this.functionCompact = functionCompact;
	}

	public DefaultWeightedEdge getEdge() {
		return edge;
	}

	public String getVariableName() {
		return variableName;
	}

	public FunctionCompact getFunctionCompact() {
		return functionCompact;
	}	
	
	public String toString(){
		return edge.toString() + " expr: " + functionCompact.getExpression();
	}
}