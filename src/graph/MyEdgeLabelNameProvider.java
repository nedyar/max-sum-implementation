package graph;

import org.jgrapht.ext.EdgeNameProvider;

public class MyEdgeLabelNameProvider<E> implements EdgeNameProvider<E> {

	public String getEdgeName(E arg0) {
		return arg0.toString();
	}
}
