package graph;

import org.jgrapht.ext.VertexNameProvider;

public class MyVertexLabelNameProvider<V> implements VertexNameProvider<V> {

	@Override
	public String getVertexName(V arg0) {
		return arg0.toString();
	}
}
