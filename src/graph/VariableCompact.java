package graph;

import java.util.ArrayList;

//Compat representation of a variable from the graph point of view
public class VariableCompact {
	private String variableName;
	private ArrayList<String> scope;
	
	public VariableCompact(String variableName, ArrayList<String> scope) {
		super();
		this.variableName = variableName;
		this.scope = scope;
	}

	/**
	 * @return the functionName
	 */
	public String getVariableName() {
		return variableName;
	}

	/**
	 * @return the scope
	 */
	public ArrayList<String> getScope() {
		return scope;
	}
}