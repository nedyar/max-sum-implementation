package graph;

import java.util.ArrayList;


import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleGraph;
import riverTree.RiverNode;

public class RiverGraph {

	private SimpleGraph<String, DefaultWeightedEdge> graph;
	
	public SimpleGraph<String, DefaultWeightedEdge> getGraph(){
		return graph;
	}
	
	public RiverGraph(ArrayList<RiverNode> allNodes){
		graph = new SimpleGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);

		//Add vertexes
		for (RiverNode riverNode : allNodes) {
			String nameId = riverNode.getVariableName();
			graph.addVertex(nameId);			
		}
		
		//Add Edges
		for (RiverNode riverNode : allNodes) {
			String nameId = riverNode.getVariableName();
			
			try {
				String v1 = riverNode.getOutflow().getVariableName();
				graph.addEdge(nameId, v1);
				graph.addEdge(v1, nameId);
			}
			catch (NullPointerException ex){

			}			
		}
	}
}
