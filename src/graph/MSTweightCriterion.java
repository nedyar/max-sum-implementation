package graph;

import java.util.ArrayList;
import java.util.List;

public enum MSTweightCriterion {
	UNIFORM, RANKING_UPSTREAM, RANKING_NODES, CONSTRAINT_LESS, CONSTRAINT_MORE, RANDOM, ACYCLIC;
		
	public static List<MSTweightCriterion> values2(){
		List<MSTweightCriterion> out = new ArrayList<MSTweightCriterion>();		
		out.add(UNIFORM);
		out.add(RANKING_UPSTREAM);
		out.add(RANKING_NODES);
		out.add(CONSTRAINT_LESS);
		out.add(CONSTRAINT_MORE);
		return out;
	}
}
