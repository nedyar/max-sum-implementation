package graph;

import java.util.ArrayList;

public class FunctionCompact {
	private String functionName;
	private ArrayList<String> scope;
	
	//se soddisfatta allora non vi � nessuna violazione, nemmeno soft (dato che si tratta dell'espressione pi� vincolante)
	private String expression;	
	
	public FunctionCompact(String functionName, ArrayList<String> scope, String expression) {
		super();
		this.functionName = functionName;
		this.scope = scope;
		this.expression = expression;
	}

	/**
	 * @return the functionName
	 */
	public String getFunctionName() {
		return functionName;
	}

	/**
	 * @return the scope
	 */
	public ArrayList<String> getScope() {
		return scope;
	}	
	
	public String getExpression() {
		return expression;
	}	
	
	public String toString(){
		return functionName + " expr: " + expression;
	}
}
