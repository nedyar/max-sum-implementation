package graph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import math.Utility;

//import oldStuff.JGraphAdapterDemo;

import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.alg.KruskalMinimumSpanningTree;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

import riverTree.ComparisonType;
import riverTree.River;

import Debug.Debug;

public class GraphHandler {

	private SimpleWeightedGraph<String, DefaultWeightedEdge> graph;

	/**
	 * 
	 * @return Return the cutted edges
	 */
	public Set<DefaultWeightedEdge> obtainMST(){
		KruskalMinimumSpanningTree<String, DefaultWeightedEdge> k = new KruskalMinimumSpanningTree<>(graph);
		Set<DefaultWeightedEdge> edges = graph.edgeSet();
		Set<DefaultWeightedEdge> edgesAfterMST = k.getEdgeSet();
		Set<DefaultWeightedEdge> out = new HashSet<DefaultWeightedEdge>();
		for (DefaultWeightedEdge defaultWeightedEdge : edges) {
			if (!edgesAfterMST.contains(defaultWeightedEdge)){
				out.add(defaultWeightedEdge);
			}
		}

		Debug.print("Edges before MST: " + edges,false);
		Debug.print("Edges after MST: " + edgesAfterMST, false);
		Debug.print("Cutted Edges: " + out, false);

		return out;		
	}

	public GraphHandler(ArrayList<String> variables , ArrayList<FunctionCompact> functions,
			MSTweightCriterion mstCriterion, River river) throws IOException {

		this.graph = new SimpleWeightedGraph<String, DefaultWeightedEdge> (DefaultWeightedEdge.class);

		HashMap<String, Integer> variableWeights = river.getVariableWeights();

		//generate all vertex
		for (FunctionCompact function : functions) {
			this.graph.addVertex(function.getFunctionName());
		}
		for (String variable : variables) {
			this.graph.addVertex(variable);
		}

		//keep edge saved for Ranking_nodes issues
		ArrayList<EdgeInformation> edgesInfo = new ArrayList<EdgeInformation>();

		//generate edges, in automatico elimina gli archi inseriti due volte
		for (FunctionCompact function : functions) {
			ArrayList<String> scope = function.getScope();
			for (String variable : scope) {
				/*
				 * versioni:
				 * 	- unitario
				 * 	- in base al numero di nodi a monte della varibile in question (ranking)
				 * 	- in base al numero di archi uscenti (w_i,j = s(i) + s(j), dove s(k) sono gli archi
				 * 		uscenti del nodo k)
				 *  - CL
				 *  - CM
				 */

				DefaultWeightedEdge e1 = graph.addEdge(function.getFunctionName(), variable);
				switch (mstCriterion) {
				case UNIFORM:{
					graph.setEdgeWeight(e1, 1);
					break;
				}					
				case RANKING_UPSTREAM:{
					int weight = 0; 
					weight = variableWeights.get(variable);

					graph.setEdgeWeight(e1, weight);
					break;
				}
				default:
					edgesInfo.add(new EdgeInformation(e1, variable, function));
					break;
				}
			}
		}

		//apply the weight for ranking_nodes once i have builded the graph
		if (mstCriterion == MSTweightCriterion.RANKING_NODES) {
			for (EdgeInformation edgeInformation : edgesInfo) {
				int weight = 0;

				//dovrebbero essere il numero di nodi uscenti dal nodo variabile
				int variableScore =  graph.degreeOf(edgeInformation.getVariableName());
				
				//la dimensione dello scope della funzione (cio� il numero di nodi uscenti da essa)
				//+ il punteggio legato alla variabile
				weight = edgeInformation.getFunctionCompact().getScope().size() + variableScore;
				graph.setEdgeWeight(edgeInformation.getEdge(), weight);
			}
		}

		if (mstCriterion == MSTweightCriterion.CONSTRAINT_LESS || mstCriterion == MSTweightCriterion.CONSTRAINT_MORE) {
			for (EdgeInformation edgeInformation : edgesInfo) {
				int weight = 0; 
				/* 
				 * find varName in function expression
				 * get the sign
				 * get the comparison operator (lt, gt, lte or gte)
				 * choose weight based on the last two
				 * 
				 */
				String tempExpr = edgeInformation.getFunctionCompact().getExpression();	
				weight = getWeight(mstCriterion, edgeInformation, tempExpr);
				graph.setEdgeWeight(edgeInformation.getEdge(), weight);
			}
		}
		
		if (mstCriterion == MSTweightCriterion.RANDOM){
			for (EdgeInformation edgeInformation : edgesInfo) {
				int weight = 0; 
				weight = Utility.randomInt(1, 40);
				graph.setEdgeWeight(edgeInformation.getEdge(), weight);
			}
		}
		Debug.print("Built graph: " + graph, false);
	}

	private int getWeight(MSTweightCriterion mstCriterion,
			EdgeInformation edgeInformation, String tempExpr) {

		int weight = 0;
		String variableName = edgeInformation.getVariableName();

		int variableIndex = tempExpr.indexOf(variableName);

		//controllo che la variabile sia contenuta nell'espressione
		if (variableIndex >= 0){
			ComparisonType compOperator = ComparisonType.getComparisonTypeFromExpression(tempExpr);
			int compIndex = tempExpr.indexOf(compOperator.toString());

			//left is true when the variable is on left of the operator
			boolean left = false;
			if (compIndex > variableIndex){
				left = true;
			}
			
			boolean concordant = Utility.isConcordant(variableName, tempExpr, compOperator);
	
			//if I cut this variable, the function is more constrained
			boolean constraintMore = isConstraintMore(left, concordant);

			//Va bene cos�, basta dire che CM indica la preferenza del taglio delle variabili che vincolano di pi�
			//CL preferisce tagliare le variabili che violano di meno
			if (constraintMore){				
				if (mstCriterion == MSTweightCriterion.CONSTRAINT_LESS){
					weight = 1;
				}
				else {
					weight = 100;
				}
			}	
			else{
				if (mstCriterion == MSTweightCriterion.CONSTRAINT_LESS){
					weight = 100;
				}
				else {
					weight = 1;
				}
			}
		}	
		else {
			weight = 1;
		}
		return weight;
	}	

	private boolean isConstraintMore(boolean left, boolean concordant) {		
		if (left){
			if (concordant){					
				return true;
			}
			else {
				return false;
			}		
		}
		else{
			if (concordant){
				return false;
			}
			else {
				return true;
			}
		}
	}

	public SimpleWeightedGraph<String, DefaultWeightedEdge> getGraph() {
		return graph;
	}

	public boolean isConnected(){
		boolean out;
		ConnectivityInspector<String, DefaultWeightedEdge> connectivityInspector = new ConnectivityInspector<>(graph);
		out = connectivityInspector.isGraphConnected();
		return out;
	}
}