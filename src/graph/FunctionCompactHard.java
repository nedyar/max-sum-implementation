package graph;

import java.util.ArrayList;

/**
 * This class is the same of FunctionCompact used to state and hard constraint
 */
public class FunctionCompactHard extends FunctionCompact {
	private String hardExpression;
	
	public FunctionCompactHard(String functionName, ArrayList<String> scope,
			String expression, String hardExpression) {
		super(functionName, scope, expression);
		this.hardExpression = hardExpression;
	}

	public String getHardExpression() {
		return hardExpression;
	}
}