package hypervolume;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import jmetal.qualityIndicator.Epsilon;
import jmetal.qualityIndicator.Hypervolume;
import jmetal.qualityIndicator.util.MetricsUtil;

public class HypervolumeJMetal {

	/**
	 * @param args
	 */
	//Il main ha solo uno scopo di testing
	public static void main(String[] args) {
		double [][] paretoFront = new MetricsUtil().readFront("frontier/p04.txt");
		double [][] paretoTrueFront = new MetricsUtil().readFront("frontier/b04.txt");

		double h = getHypervolume(paretoTrueFront, paretoFront);
		
		double generationalDistance = getGenerationalDistance(paretoTrueFront, paretoFront);

		double epsilonIndicator = getEpsilonIndicator(paretoTrueFront, paretoFront);
	
		System.out.println("H � " + h);
		System.out.println("Generational Distance paria a " + generationalDistance);
		System.out.println("Epsilone Indicator paria a " + epsilonIndicator);
	}

	/**
	 * 
	 * @param bruteforce The optimal Pareto frontier taken as reference.
	 * @param paretoFrontier The Pareto frontier to evaluate.
	 * @return This method returns the hypervolume metric of two Pareto frontiers given as parameters.
	 */
	public static double getHypervolume(double[][] bruteforce, double[][] paretoFrontier){
		Hypervolume hypervolume = new Hypervolume();

		int numberOfObjectives = bruteforce[0].length;

		Problem newProblem = checkForNullRange(paretoFrontier, bruteforce, numberOfObjectives);

		return hypervolume.hypervolume(newProblem.getParetoFrontier(), newProblem.getBruteforce(),
				newProblem.getNumberOfObjectives());
	}
	
	public static double getGenerationalDistance(double[][] bruteforce, double[][] paretoFrontier){
		//uso la mia versione a causa del bug di normalizzazione se esce dai limiti
//		GenerationalDistance generationalDistance = new GenerationalDistance();
		MyGenerationalDistance generationalDistance = new MyGenerationalDistance();

		int numberOfObjectives = bruteforce[0].length;
		Problem newProblem = checkForNullRange(paretoFrontier, bruteforce, numberOfObjectives);

		return generationalDistance.generationalDistance(newProblem.getParetoFrontier(), newProblem.getBruteforce(),
				newProblem.getNumberOfObjectives());
	}
	
	public static double getEpsilonIndicator(double[][] bruteforce, double[][] paretoFrontier){
		Epsilon epsilonIndicator = new Epsilon();
		int numberOfObjectives = bruteforce[0].length;
		return epsilonIndicator.epsilon(bruteforce, paretoFrontier, numberOfObjectives);
	}
	
	/**
	 * Copy of the previous one to use new implementation of Hypervolume
	 * @param bruteforce The optimal Pareto frontier taken as reference.
	 * @param paretoFrontier The Pareto frontier to evaluate.
	 * @return This method returns the hypervolume metric of two Pareto frontiers given as parameters.
	 */
	public static double getHypervolume2(double[][] bruteforce, double[][] paretoFrontier){
		MyHypervolume hypervolume = new MyHypervolume();		
		int numberOfObjectives = bruteforce[0].length;
		Problem newProblem = checkForNullRange(paretoFrontier, bruteforce, numberOfObjectives);
		return hypervolume.hypervolume(newProblem.getParetoFrontier(), newProblem.getBruteforce(),
				newProblem.getNumberOfObjectives());
	}

	/**
	 * Check if the frontiers given have some columns that never change so have a difference equal to 0 between every
	 * elements.
	 * @param paretoFront
	 * @param paretoTrueFront
	 * @param numberOfObjectives
	 * @return The new Problem with lower dimension if some nullRange occours
	 */
	public static Problem checkForNullRange(double[][] paretoFront,
			double[][] paretoTrueFront, int numberOfObjectives) {

		Problem out = new Problem(paretoTrueFront, paretoFront, numberOfObjectives);

		MetricsUtil utilities = new MetricsUtil();
		double [] max = utilities.getMaximumValues(paretoTrueFront, numberOfObjectives);
		double [] min = utilities.getMinimumValues(paretoTrueFront, numberOfObjectives);

		try{
			ArrayList<Integer> toDel = new ArrayList<Integer>();			
			
			for (int i = 0; i < max.length; i++){
				if (max[i] == min[i]){
					toDel.add(i);
				}
			}
			
			for (int i = toDel.size() - 1; i >= 0; i--) {
				paretoTrueFront = deleteCol(paretoTrueFront,toDel.get(i));
				paretoFront = deleteCol(paretoFront,toDel.get(i));
				out.setBruteforce(paretoTrueFront);
				out.setParetoFrontier(paretoFront);
				numberOfObjectives--;
			}			
		}
		catch (IndexOutOfBoundsException e){

		}		
		out.setNumberOfObjectives(numberOfObjectives);
		return out;	
	}

	//Delete a column at a given index
	private static double[][] deleteCol(double[][] matrix, int index) {
		int rows = matrix.length;
		int cols = matrix[0].length;
		double[][] out = new double[rows][cols - 1];
		for (int i = 0; i < rows; i++){		
			int j = 0;
			int t = 0;
			while (j < cols){
				if (j == index){
					j++;
				}
				out[i][t] = matrix[i][j];
				t++;
				j++;
			}
		}
		return out;
	}

	//ora uso l'utilit� di JMetal: jmetal.qualityIndicator.util.MetricsUtil
	@Deprecated
	public static double[][] loadParetoFromFile(String string) {

		ArrayList<String> rows = new ArrayList<String>();
		FileReader f;
		BufferedReader b;
		try {
			f = new FileReader(string);			
			b = new BufferedReader(f);		    
			String row = b.readLine();		    

			while (row != null && !row.contains("#")){		    	
				rows.add(row);		    	
				row = b.readLine();
			}
			f.close();
			b.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 

		int elements = rows.size();		

		double out [][] = new double[elements][];

		int i = 0;
		for (String row : rows) {
			out[i] = getArray(row);
			i++;
		}
		return out;
	}

	private static double[] getArray(String row) {
		String valuesStr[] = row.split(" ");
		int size = valuesStr.length;
		double[] out = new double[size];
		for (int i = 0; i < size; i++) {
			out[i] = Double.parseDouble(valuesStr[i]);
		}		
		return out;
	}
}
