package hypervolume;

public enum OptimizationType {
	COST, UTILITY, FEASIBLE_COST ;
	
	@Override
	public String toString(){
		switch (this) {
		case COST:
			return "cost";
		case UTILITY:
			return "utility";
		case FEASIBLE_COST:
			return "feasible_cost";
		default:
			return null;
		}
	}	
}
