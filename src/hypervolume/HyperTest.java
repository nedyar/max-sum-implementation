package hypervolume;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to save information about points of a solution that have hypervolume equal to 0
 * @author Stefano
 *
 */
public class HyperTest {
	private Integer colIndex;
	private List<Integer> rowIndexes;
	private List<String> variableNames;
	
	public HyperTest(Integer colIndex, List<Integer> rowIndexes){
		this.colIndex = colIndex;
		this.rowIndexes = rowIndexes;
		this.variableNames = new ArrayList<String>();
		for (Integer integer : rowIndexes) {
			int plusOne = integer + 1;
			variableNames.add("fo" + plusOne);
		}
	}

	public Integer getColIndex() {
		return colIndex;
	}

	public List<Integer> getRowIndexes() {
		return rowIndexes;
	}
	
	public String getRowIndexesStr(){
		String out = "";
		for (Integer rowIndex : rowIndexes) {
			out = out + rowIndex.toString() + " ";
		}
		return out;
	}
	
	public String getRowVariableNamesStr(){
		String out = "";
		for (String rowStr : variableNames) {
			out = out + rowStr + " ";
		}
		return out;
	}
	
	public String toString(){
		return colIndex + ": " + getRowIndexesStr();
	}	
}