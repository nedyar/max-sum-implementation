package hypervolume;

public class Problem {
	private double[][] bruteforce;
	private double[][] paretoFrontier;
	private int numberOfObjectives;
	
	public Problem(double[][] bruteforce, double[][] paretoFrontier,
			int numberOfObjectives) {
		super();
		this.bruteforce = bruteforce;
		this.paretoFrontier = paretoFrontier;
		this.numberOfObjectives = numberOfObjectives;
	}

	public double[][] getBruteforce() {
		return bruteforce;
	}
	
	public void setBruteforce(double[][] bruteforce) {
		this.bruteforce = bruteforce;
	}
	public double[][] getParetoFrontier() {
		return paretoFrontier;
	}
	public void setParetoFrontier(double[][] paretoFrontier) {
		this.paretoFrontier = paretoFrontier;
	}
	public int getNumberOfObjectives() {
		return numberOfObjectives;
	}
	public void setNumberOfObjectives(int numberOfObjectives) {
		this.numberOfObjectives = numberOfObjectives;
	}	
}