package hypervolume;

import jmetal.qualityIndicator.util.MetricsUtil;
import Debug.Debug;

//Versione modificata per cercare di eliminare l'hypervolume nullo, che per� era causato da soluzioni degeneri non a 
//un'implementazione errata
public class TestHypervolume {
	private double bestFontier[][];
	private double frontier[][];
	private int numberOfObjectives;
	private jmetal.qualityIndicator.util.MetricsUtil utils_;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		double b[][] = {{0,0},{1,1}};
//		double f[][] = {{0.2,0.8},{0.4,0.5},{0.6,0.3}};
		
//		double b[][] = utils_.
//		double f[][] = {{0.2,0.8},{0.4,0.5},{0.6,0.3}};
		double b[][] = new MetricsUtil().readFront("frontier/9HBF.txt");
		double f[][] = new MetricsUtil().readFront("frontier/9HU.txt");	
		
		TestHypervolume test = new TestHypervolume(b, f);
		double h = test.calculate();
		
		Debug.print("hyper vale: " + h);		
	}		
	
	private TestHypervolume(double[][] bestFontier, double[][] frontier) {
		super();
		this.bestFontier = bestFontier;
		this.frontier = frontier;
		if (bestFontier != null){
			numberOfObjectives = bestFontier[0].length;
		}
		else {
			numberOfObjectives = 0;
		}
		utils_ = new jmetal.qualityIndicator.util.MetricsUtil();
	}

	public double calculate(){
		/* get max min 
		 * normalize frontier
		 * calculate volume
		 */
		double out = 0.0;		
		
		double maxBest[] = utils_.getMaximumValues(bestFontier, numberOfObjectives); 
		double minBest[] = utils_.getMinimumValues(bestFontier, numberOfObjectives);
		
		//modifico i valori massimi e minimi aumentando il range
//		maximumValues = modifyMax(maximumValues);
//		minimumValues = modifyMin(minimumValues);
		
		double maxSol[] = utils_.getMaximumValues(frontier, numberOfObjectives);
		double minSol[] = utils_.getMinimumValues(frontier, numberOfObjectives);
		
		double max[] = mergeMax(maxBest,maxSol);
		double min[] = mergeMin(minBest,minSol);
		
		double normalizedFrontier[][] = utils_.getNormalizedFront(frontier, max, min);
		
		double p = normalizedFrontier[0][0];
		for (int j = 1; j < normalizedFrontier[0].length; j++) {
			p = p * (normalizedFrontier[0][j]);
		}
		out = out + Math.abs(p);
		
		for (int i = 1; i < normalizedFrontier.length; i++) {
			p = normalizedFrontier[i][0] - normalizedFrontier[i-1][0];
			for (int j = 1; j < normalizedFrontier[i].length; j++) {
				p = p * (normalizedFrontier[i][j] - normalizedFrontier[i-1][j]);
			}
			out = out + Math.abs(p);
		}		
		return out;
	}

	private double[] mergeMin(double[] minBest, double[] minSol) {
		double out[] = new double[minBest.length];
		for (int i = 0; i < out.length; i++) {
			out[i] = Math.min(minBest[i],minSol[i]);			
		}		
		return out;
	}

	private double[] mergeMax(double[] maxBest, double[] maxSol) {
		double out[] = new double[maxBest.length];
		for (int i = 0; i < out.length; i++) {
			out[i] = Math.max(maxBest[i],maxBest[i]);			
		}		
		return out;
	}
}
