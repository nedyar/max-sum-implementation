package execution;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom2.JDOMException;

import riverTree.ComparisonType;

import Debug.Debug;
import algorithm.Domain;
import algorithm.Variable;

import math.Evaluator;
import math.ExpressionSystem;
import math.Utility;
import math.VariableAssign;

import graph.MSTweightCriterion;
import hypervolume.OptimizationType;
import xml.XmlOptimizationHandler;
import xml.XmlRiverHandler;

public class EvaluateFeasibility {
	//questo systemInit si riferisce al modello ciclico originale
	private SystemInitializator systemInitializator;
	private String dir;
	private String path;
	private XmlOptimizationHandler xmlHandler;
	private static boolean acyclic;

	public EvaluateFeasibility(String path) {
		this.path = path;
		this.dir = new File(this.path).getParent();
		String xmlFilePath = dir + "\\optimization-log.xml";		
		xmlHandler = new XmlOptimizationHandler(xmlFilePath);
		xmlHandler.loadXmlFile();
		acyclic = xmlHandler.isAcyclic();
	}
	
	public static void execute(String path) {
//		public static void execute(String dir) {
		/*
		 * ottengo i modelli
		 * 		quindi ottengo tutte i vincoli
		 * ottengo la soluzione trovata, solo Uniform
		 * per ogni soluzione
		 * 		valuto se rispetta ciascun vincolo
		 */
		EvaluateFeasibility evaluateFeasability = new EvaluateFeasibility(path);
		if (!acyclic){
			evaluateFeasability.init();
			evaluateFeasability.checkFeasibility();	
			evaluateFeasability.constraintViolation();
		}
	}

	private void init() {
		systemInitializator = new SystemInitializator();
		systemInitializator.inizializzaDatiXML(dir + "/model.xml", false);
	}

	private void constraintViolation() {
		/*
		 * Carico i modelli aciclici generati con i vari metodi
		 * 		per ciascuno carico i modelli 
		 */
		//cancello le vecchie istanze di violazione
		xmlHandler.clearViolations();
		for (MSTweightCriterion criterion : MSTweightCriterion.values2()) {
			String acyclicModelPath = dir + "/model_" + criterion.toString() + "_acyclic.xml";
			String originalModelPath = dir + "/model.xml";
			try {
				XmlRiverHandler xmlAcyclic = new XmlRiverHandler(acyclicModelPath);
				xmlAcyclic.loadXmlFile();

				XmlRiverHandler xmlOriginal = new XmlRiverHandler(originalModelPath);
				xmlOriginal.loadXmlFile();

				List<String> rows = xmlHandler.getFrontierEnrico(criterion);

				if (rows.size() > 0){
					List<List<VariableAssign>> solutionAssignments = fromRowToAssignments(rows);

					Map<List<VariableAssign>, List<Float>> violationMap = new HashMap<List<VariableAssign>, List<Float>>();

					List<String> cuttedEdgesStr = xmlAcyclic.getCuttedEdges();				

					for (List<VariableAssign> solutionAssignment : solutionAssignments) {

						List<Float> tempViolations = new ArrayList<Float>();
						List<String> alreadyExaminatedFunctions = new ArrayList<String>();

						for (String cuttedString : cuttedEdgesStr) {

							//ogni cutted edge � salvato cos�: (fi : xj)
							String splittedStr[] = cuttedString.split(":");
							String functionName = splittedStr[0].replace('(', ' ');
							functionName = functionName.trim();

							//se effettua i tagli pi� volte sulla stessa funzione non ha senso ricalcolarlo!!
							if (!alreadyExaminatedFunctions.contains(functionName)){	
								alreadyExaminatedFunctions.add(functionName);

								String hardExpression = xmlOriginal.getHardFunctionExpression(functionName);

								if (hardExpression != null){
									Float ratioViolation = getRatioViolation(xmlOriginal, solutionAssignment,
											functionName, hardExpression);
									tempViolations.add(ratioViolation);
								}
								else {
									tempViolations.add((float) 0);
								}
							}
						}	
						violationMap.put(solutionAssignment, tempViolations);
						xmlHandler.addViolations(criterion, tempViolations);
					}
					
					//calcolo media e deviazione standard
					double mean = math.Utility.calculateMean(violationMap);
					double stDev = math.Utility.calculateStandardDeviation(violationMap);
					double max = math.Utility.calculateMax(violationMap);
					xmlHandler.addMean(criterion, mean);
					xmlHandler.addStandardDeviation(criterion, stDev);
					xmlHandler.addMaxViolation(criterion, max);
				}

			} catch (JDOMException | IOException e) {
				Debug.print("Problema con file " + acyclicModelPath + " o " + originalModelPath);
			}
		}
	}

	private Float getRatioViolation(XmlRiverHandler xmlOriginal,
			List<VariableAssign> solutionAssignment, String functionName,
			String hardExpression) {
		List<VariableAssign> worseAssignment = new ArrayList<VariableAssign>();

		//il tipo di operatore di confronto
		//		ComparisonType comparisonType = getComparisonType(hardExpression);
		ComparisonType comparisonType = ComparisonType.getComparisonTypeFromExpression(hardExpression);

		//l'espressione divisa tramite l'operatore
		String splittedExpr[] = splitExpression(hardExpression, comparisonType);

		String leftExpression = splittedExpr[0];
		String rightExpression = splittedExpr[1];

		ArrayList<String> originalScopes =	xmlOriginal.getFunctionScope(functionName);

		for (String scopeStr : originalScopes) {

			boolean concordant = Utility.isConcordant(scopeStr, hardExpression, comparisonType);
			boolean leftVariable = leftExpression.contains(scopeStr);

			Float value;
			Domain domain = systemInitializator.getVariableByName(scopeStr).getDomain();

			if (leftVariable){
				if (concordant){
					value = Collections.max(domain.getValues());
				}
				else {
					value = Collections.min(domain.getValues());
				}
			}
			else{
				if (concordant){
					value = Collections.min(domain.getValues());
				}
				else {
					value = Collections.max(domain.getValues());
				}							
			}
			worseAssignment.add(new VariableAssign(scopeStr, value));							
		}

		Float worseViolation = getViolation(worseAssignment, comparisonType,
				leftExpression, rightExpression);

		Float actualViolation = getViolation(solutionAssignment, comparisonType,
				leftExpression, rightExpression);

		Float ratioViolation;

		if (worseViolation != 0){
			ratioViolation = actualViolation / worseViolation;
		}
		else {
			if (actualViolation == 0){
				ratioViolation = (float) 0;
			}
			else {
				ratioViolation = (float) 1;
			}
		}
		return ratioViolation;
	}

	private Float getViolation(List<VariableAssign> assignment,
			ComparisonType comparisonType, String leftExpression,
			String rightExpression) {
		Float leftEval = Evaluator.evaluateFunction(leftExpression, assignment);
		Float righEval = Evaluator.evaluateFunction(rightExpression, assignment);

		Float delta = leftEval - righEval;

		Float violation;

		if (comparisonType.meansGreater()){
			if (delta < 0){
				violation = (float) 0;
			}
			else {
				violation = delta;
			}
		}
		else {
			if (delta > 0){
				violation = (float) 0;
			}
			else {
				violation = (-1)*delta;
			}
		}
		return violation;
	}

	private String[] splitExpression(String expression, ComparisonType comparisonType) {
		String out[] = new String[2];
		try { 
			out = expression.split(comparisonType.toString());
			//elimino l'eventuale uguale
			out[1] = out[1].replace('=', ' ');
		}
		catch (NullPointerException e){	}
		return out;
	}

	private void checkFeasibility() {
		List<ExpressionSystem> expressionSystems = systemInitializator.getFunctionExpressions();
		
		//inutile valutarlo se � Aciclico dato che trova la soluzione ottima
		for (MSTweightCriterion criterion : MSTweightCriterion.values2()) {	
			//		for (MSTweightCriterion criterion : MSTweightCriterion.getCriterions()) {
			try{
				List<String> rows = xmlHandler.getFrontierEnrico(criterion);
				double costs[][] = xmlHandler.getFrontierCost(criterion, OptimizationType.COST);		

				if (costs == null){
					Debug.print("Nessun costo trovato per il file: " + dir +  " tecnicna: " +  criterion.toString());
				}

				List<List<VariableAssign>> assignments = fromRowToAssignments(rows);
				List<List<VariableAssign>> feasibleAssignments = new ArrayList<List<VariableAssign>>();
				List<Integer> feasibleIndexes = new ArrayList<Integer>();

				int number = 0;
				for (int i = 0; i < assignments.size(); i++) {
					if (isFeasible(expressionSystems, assignments.get(i))){
						feasibleAssignments.add(assignments.get(i));
						feasibleIndexes.add(new Integer(i));
						number++;
					}
				}

				double feasibleCosts[][] = getFeasibleCost(costs, feasibleIndexes);

				try {
					xmlHandler.addFeasibleCosts(criterion, feasibleCosts);
				} catch (IOException e) {
					e.printStackTrace();
				}

				Debug.print("le soluzioni fattibli per il criterio " + criterion.toString() + 
						" sono: " + number  + " su " + rows.size());
				Debug.print(feasibleAssignments);

				if (feasibleCosts != null) {
					Debug.print("I costi relativi sono quindi: ");
					for (double[] ds : feasibleCosts) {
						Debug.print(Arrays.toString(ds));
					}
				}
			}
			catch (NullPointerException e){
				System.err.println("Problema con " + dir);
			}			
		}
	}

	private double[][] getFeasibleCost(double costs[][], List<Integer> feasibleIndexes) {
		if (costs == null) return null;
		double out[][] = new double[feasibleIndexes.size()][costs[0].length];
		int index = 0;
		for (Integer integer : feasibleIndexes) {
			out[index] = costs[integer];
			index++;
		}
		return out;
	}

	private List<List<VariableAssign>> fromRowToAssignments(List<String> rows) {
		List<List<VariableAssign>> out = new  ArrayList<List<VariableAssign>>();
		List<Variable> variables = systemInitializator.getVariables();

		for (String row : rows) {
			String subStrings[] = row.split("s");

			//da sistemare, fatto cos� � poco intelligente! Ma funziona!
			String assignmentStr = subStrings[0].replace('<', ' ');
			assignmentStr = assignmentStr.replace('>', ' ');
			assignmentStr = assignmentStr.replace('x', ' ');
			assignmentStr = assignmentStr.replace('=', ' ');
			assignmentStr = assignmentStr.replace('[', ' ');
			assignmentStr = assignmentStr.replace(']', ' ');

			String assignStrs[] = assignmentStr.split(",");
			List<VariableAssign> assignment = new ArrayList<VariableAssign>();

			for (int i = 0; i < variables.size(); i++) {
				assignment.add(new VariableAssign(variables.get(i).getName(), Float.parseFloat(assignStrs[i])));

			}
			out.add(assignment);
		}		
		return out;
	}

	private boolean isFeasible(List<ExpressionSystem> expressions, List<VariableAssign> assignment){
		for (ExpressionSystem expression : expressions) {	
			boolean temp = Evaluator.isSatisfied(expression, assignment);
			if (temp == false){
				return false;
			}
		}
		return true;
	}
}
