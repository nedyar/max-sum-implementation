package execution;

import java.io.File;

import fileSystemUtil.FileSystemUtils;


public class BruteforceMany {

	public static void execute(String path) {
		/*
		 * carica cartella 
		 * per ogni file xml
		 */
		File startingPath = new File(path);
		File[] files = FileSystemUtils.getModelsFolder(startingPath);
		
		for (File file : files) {
			File[] subfolders = FileSystemUtils.getModelsFolder(file);
			for (File innerFile : subfolders) {
				String absolutePath = innerFile.getAbsolutePath();
				BruteForce.execute(absolutePath);								
			}
		}
	}
}