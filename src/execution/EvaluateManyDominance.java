package execution;

import java.io.File;

import fileSystemUtil.FileSystemUtils;

public class EvaluateManyDominance {

//	public static void main(String[] args) {
//
//		String path = "";
//		if(args.length < 1){
//			//			path = "sistemi\\7ag8d\\";//			
//			path = "sistemi\\8ag7d2\\";
//			//			path = "sistemi\\12ag12d\\";
//			//			path = "sistemi\\10ag10d\\";
//			//			path = "sistemi/8ag15d/";			
//			//			path = "sistemi/5ag5d/";
//			//			path = "sistemi/5ag10d/";
//			//			path = "sistemi/5ag15d/";
//			//			path = "sistemi/acyclic5ag5d/";
//			//			path = "sistemi/acyclic6ag10d/";
//			//			path = "sistemi/acyclic7ag8d/";//			
//			//			path = "sistemi/acyclic6ag8d/";
//			//			path = "sistemi/acyclic6ag6d/";
//			//			path = "sistemi/3ag30d/";
//			//			path = "sistemi/4ag30d/";
//		}
//		else {
//			path = args[0];
//		}
//		File startingPath = new File(path);
//		File[] files = FileSystemUtils.getModelsFolder(startingPath);
//
//		for (File file : files) {
//			File[] subfolders = FileSystemUtils.getModelsFolder(file);
//			for (File innerFile : subfolders) {
//				String absolutePath = innerFile.getAbsolutePath();
//
//				String argouments[] = {absolutePath};
//				EvaluateDominance.main(argouments);				
//			}
//		}
//	}
	
	public static void execute(String path) {

//		String path = "";
//		if(args.length < 1){
//			//			path = "sistemi\\7ag8d\\";//			
//			path = "sistemi\\8ag7d2\\";
//			//			path = "sistemi\\12ag12d\\";
//			//			path = "sistemi\\10ag10d\\";
//			//			path = "sistemi/8ag15d/";			
//			//			path = "sistemi/5ag5d/";
//			//			path = "sistemi/5ag10d/";
//			//			path = "sistemi/5ag15d/";
//			//			path = "sistemi/acyclic5ag5d/";
//			//			path = "sistemi/acyclic6ag10d/";
//			//			path = "sistemi/acyclic7ag8d/";//			
//			//			path = "sistemi/acyclic6ag8d/";
//			//			path = "sistemi/acyclic6ag6d/";
//			//			path = "sistemi/3ag30d/";
//			//			path = "sistemi/4ag30d/";
//		}
//		else {
//			path = args[0];
//		}
		File startingPath = new File(path);
		File[] files = FileSystemUtils.getModelsFolder(startingPath);

		for (File file : files) {
			File[] subfolders = FileSystemUtils.getModelsFolder(file);
			for (File innerFile : subfolders) {
				String absolutePath = innerFile.getAbsolutePath();

//				String argouments[] = {absolutePath};
				EvaluateDominance.execute(absolutePath);				
			}
		}
	}	
}
