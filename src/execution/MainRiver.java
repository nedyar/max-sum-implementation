package execution;

import fileSystemUtil.FileSystemUtils;
import graph.GraphUtils;
import graph.RiverGraph;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.transform.TransformerConfigurationException;

import org.jdom2.JDOMException;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.xml.sax.SAXException;

import Debug.Debug;
import Debug.Log;

import parameters.FlowType;
import parameters.Init;
import parameters.NodeParameters;
import riverTree.City;
import riverTree.ConstraintHandler;
import riverTree.Dam;
import riverTree.LoadRiver;
import riverTree.NodeType;
import riverTree.River;
import riverTree.RiverNode;


/**
 * Given an already generated river, it returns various ...
 * @author Stefano
 *
 */
public class MainRiver{

	public static void execute(String path, String paramFileStr) {	
		new Init(paramFileStr);		
		try {
			generateScenariousFromExistingModels(path);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException | SAXException e) {
			e.printStackTrace();
		}	
	}

	//genera un modello partendo da una topologia di fiume
	private static void generateScenarioFromRiver(River river, String path) throws TransformerConfigurationException, SAXException, IOException{
		/*
		 * per ogni FlowType (low,med,high)
		 * 		genero un modello in una nuova sottocartella partendo dalla struttura data
		 * 	
		 */
		String dir; 		

		for (FlowType flowType : FlowType.values()) {
			dir = path + "/" + flowType.toString();

			File fileDir = new File(dir);
			if (!fileDir.exists()){
				fileDir.mkdir();
			}

			//	Inizializzo il log per lo scenario attuale
			Log log = new Log(dir + "/Generation-LOG.xml");
			river.clearRiver(log, flowType);

			try {
				generateScenario2(dir, log, river);
			} catch (JDOMException e) {
				e.printStackTrace();
			}
		}
	}

	private static void generateScenariousFromExistingModels(String path) throws TransformerConfigurationException, SAXException, IOException{
		/*
		 * per ogni cartella
		 * 		ottengo il fiume
		 * 	 	chiamo generateScenarioFromRiver
		 */
		File mainFolder = new File(path);
		File[] subFolders = FileSystemUtils.getModelsFolder(mainFolder);
		for (File file : subFolders) {
			String dir = file.getAbsolutePath().toString();
			River river = LoadRiver.loadRiver(dir + "/river.obj");
			MainRiver.generateScenarioFromRiver(river, dir);
		}
	}

	/**
	 * 
	 * @param dir
	 * @param log
	 * @param river
	 * @throws TransformerConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 * @throws JDOMException
	 */
	private static void generateScenario2(String dir, Log log,	River river) throws TransformerConfigurationException,
	SAXException, IOException, JDOMException {
		ArrayList<RiverNode> nodes = river.getAgentNodes();		
		ArrayList<String> agents = river.getAgents(nodes);

		Debug.print("nodi" + nodes.toString(), false);

		String riverStructure = river.getRiversList().toString();		

		//Ottengo il grafo del fiume e lo esporto
		RiverGraph riverGraph = new RiverGraph(river.getAllNodes());
		GraphUtils<String, DefaultWeightedEdge> graphExporter = new GraphUtils<String, DefaultWeightedEdge>(riverGraph.getGraph());
		graphExporter.export(dir + "/River-Graph");

		String riverNodes = river.getStartingNodes().toString();
		String modelPath = dir + "/model.xml";
		ConstraintHandler constraintHandler = new ConstraintHandler(modelPath, agents, riverStructure, riverNodes);

		for (RiverNode riverNode : nodes) {
			NodeType type = null;
			NodeParameters parameters = null;
			boolean addConstraint = false;
			if (riverNode instanceof City){
				type = NodeType.CITY;
				parameters = ((City)riverNode).getParameters();
				addConstraint = true;
			}
			else if (riverNode instanceof Dam){
				type = NodeType.DAM;
				parameters = ((Dam)riverNode).getParameters();
				addConstraint = true;
			}
			if (addConstraint) {
				try {
					constraintHandler.generateConstraint(riverNode.getVariableName(), riverNode.getInflowExpression(),
							riverNode.getOutflowExpression(), type, parameters, log);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}			
		}
	}
}
