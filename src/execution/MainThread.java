package execution;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.xml.transform.TransformerConfigurationException;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.Element;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.xml.sax.SAXException;

import riverTree.AcyclicException;
import riverTree.LoadRiver;
import riverTree.River;

import xml.XmlOptimizationHandler;
import xml.XmlRiverHandler;

import graph.FunctionCompact;
import graph.GraphHandler;
import graph.GraphUtils;
import graph.MSTweightCriterion;


//import Debug.Log;
//import Debug.Messages;
//import Debug.OptimizationLog;
import algorithm.Function;
import algorithm.FunctionNode;
//import Debug.Debug;

import algorithm.QMessage;
import algorithm.RMessage;

import algorithm.SolutionNode;
import algorithm.Variable;
import algorithm.VariableNode;

public class MainThread implements Callable<String> {

	private SystemInitializator systemInitializator;
	private String path;

	public MainThread(String path){
		this.path = path;
	}

	@Override
	public String call() {
		long start = System.nanoTime();
		String dir = new File(path).getParent();
		River river = LoadRiver.loadRiver(dir + "/../river.obj");

		XmlOptimizationHandler log;
		try {
			log = new XmlOptimizationHandler(dir + "/Optimization-LOG.xml");
			log.loadXmlFile();
			XmlRiverHandler xmlHandler;
			try {
				for (MSTweightCriterion mstCriterion : MSTweightCriterion.values2()) {
					//simula solo se non lo ha gi� fatto
					if (!alreadyComputed(mstCriterion, log)){

						log.clearCriterionSection(mstCriterion);

						this.systemInitializator = new SystemInitializator();

						//Carico file xml
						SAXBuilder builder2 = new SAXBuilder();
						Document doc;
						doc = builder2.build(path);

						//Ottengo elementi principali
						Element variablesEl = doc.getRootElement().getChild("variables");
						Element functionsEl = doc.getRootElement().getChild("functions");

						// ho inserito questa per creare il grafo corrispondente ed applicare MST
						GraphHandler graphHandler;

						//creo il grafo partendo dai nomi dei nodi estrapolati direttamente dal file xml
						graphHandler = new GraphHandler(this.getVariables(variablesEl), this.getFunctions(functionsEl),
								mstCriterion, river);

						//esporto il grafo
						String export = dir + "/Constraint-Graph";
						GraphUtils<String, DefaultWeightedEdge> graphExporter = new GraphUtils<String, DefaultWeightedEdge>(graphHandler.getGraph());
						graphExporter.export(export);

						boolean continueCond = graphHandler.isConnected();
						if (!continueCond){
							String message = "Il grafo non � connesso impossibile procedere";
							System.err.println(message);
							//creo un file chiamato NON-CONNESSO al posto di un messaggio in opt-log
							new File(dir + "/NON-CONNESSO").createNewFile();							
						}
						else {	
							double startMST = System.nanoTime();
							Set<DefaultWeightedEdge> cuttedEdges = graphHandler.obtainMST();					

							//calculate mst exec time
							double elapsedTimeInSec = (System.nanoTime() - startMST) * 1.0e-9;
							log.addMSTexecutionTimeInSec(elapsedTimeInSec, mstCriterion);

							//se � ciclico
							if (!cuttedEdges.isEmpty()){						
								String newPath = path.substring(0, path.length() - 4) + "_" + mstCriterion.toString() + "_acyclic.xml";	
								xmlHandler = new XmlRiverHandler(path);
								xmlHandler.loadXmlFile();
								xmlHandler.createAcyclicXml(cuttedEdges, newPath);
								systemInitializator.inizializzaDatiXML(newPath, true);
								GraphHandler acyclicGraph = new GraphHandler(xmlHandler.getVariables(), xmlHandler.getFunctions(), 
										mstCriterion, river);

								//esporto il grafo
								String exportPath = dir + "/Acyclic-Constraint-Graph" + "_" + mstCriterion.toString();
								GraphUtils<String, DefaultWeightedEdge> graphExporter2 = new GraphUtils<String, DefaultWeightedEdge>(acyclicGraph.getGraph());
								graphExporter2.export(exportPath);

							}
							//se � aciclico
							else {	
								throw new AcyclicException();
							}
							//esegue simulazione in caso di grafo inizialmente ciclico
							execute(start, log, mstCriterion);
						}
					}
				}
			}
			catch (AcyclicException e2){
				//se lo ha gi� calcolato non fa nulla
				if (!alreadyComputed(MSTweightCriterion.ACYCLIC, log)){
					xmlHandler = new XmlRiverHandler(path);
					xmlHandler.loadXmlFile();
					systemInitializator.inizializzaDatiXML(path, true);

					//esegue simulazione in caso di grafo inizialmente aciclico
					execute(start, log, MSTweightCriterion.ACYCLIC);
				}
			}
		} catch (TransformerConfigurationException | SAXException | IOException | JDOMException e1 ) {
			e1.printStackTrace();
		}	
		return path;
	}

	private void execute(long start, XmlOptimizationHandler log,
			MSTweightCriterion mstCriterion) {
		int max_iterations = systemInitializator.getVariables().size() * 4;						

		ArrayList<Variable> variablesTemp = systemInitializator.getVariables();	

		ArrayList<FunctionNode> functionNodes = new ArrayList<FunctionNode>();
		ArrayList<VariableNode> variableNodes = new ArrayList<VariableNode>();

		for(Variable v: variablesTemp){
			VariableNode vn = new VariableNode(v, max_iterations);
			variableNodes.add(vn);
		}
		ArrayList<Function> functionsTemp = systemInitializator.getFunctions();
		for(Function f: functionsTemp){
			f.activateNeighbours();
			//FIXME la soglia dovrebbe essere impostata come parametro in input, o cmq messa a -infinito!
			//compare anche nel file xml, non capisco a cosa serva messa qua
			f.setHardThreshold(-100000);						

			FunctionNode fn = new FunctionNode(f, max_iterations);
			functionNodes.add(fn);
		}				

		SolutionNode sol = new SolutionNode(systemInitializator.getSolution(), start, log, mstCriterion);	

		//To save messages uncomment the following!
		try {
			Thread t = sol.getThread();
			t.join();

			ArrayList<QMessage> qMessages = new ArrayList<QMessage>();
			ArrayList<RMessage> rMessages = new ArrayList<RMessage>();

			for(FunctionNode fn: functionNodes){						
				qMessages.addAll(fn.getFunction().getQMessages());
			}
			for(VariableNode vn : variableNodes){
				rMessages.addAll(vn.getVariable().getRMessages());
			}

			int nMessages = qMessages.size() + rMessages.size();
			log.addNumberOfMessages(mstCriterion, nMessages);

			//Debug.print("I messaggi sono: "  + nMessages);
			//Debug.print("Gli agenti sono: "  + variablesTemp.size());
			//Debug.print("Le funzioni sono: "  + functionsTemp.size());
			//Debug.print("Gli archi sono: "  + acyclicGraph.getGraph().edgeSet().size());
			//Debug.print("Il timestamp �...");						
			//Debug.print(qMessages);//
			t.interrupt();

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private boolean alreadyComputed(MSTweightCriterion mstCriterion, XmlOptimizationHandler log) {
		return log.alreadyComputed(mstCriterion);
	}

	private ArrayList<String> getVariables(Element variablesEl){
		ArrayList<String> out = new ArrayList<String>();

		List<Element> variablesChildren = variablesEl.getChildren("variable");
		for (Element element : variablesChildren) {
			String tempName = element.getAttribute("name").getValue();			
			out.add(tempName);			
		}	
		return out;
	}

	private ArrayList<FunctionCompact> getFunctions(Element functionsEl){
		ArrayList<FunctionCompact> out = new ArrayList<FunctionCompact>();

		List<Element> functionChildrens = functionsEl.getChildren("function");
		for (Element element : functionChildrens) {
			String tempName = element.getAttribute("name").getValue();

			String expression = element.getChild("booleans").getChildText("expr");

			out.add(new FunctionCompact(tempName, this.getFunctionScope(element), expression));			
		}
		return out;
	}

	private ArrayList<String> getFunctionScope(Element functionEl){
		ArrayList<String> out = new ArrayList<String>();

		List<Element> scopeChildrens = functionEl.getChild("scope").getChildren("var");
		for (Element element : scopeChildrens) {
			String tempName = element.getText();			
			out.add(tempName);			
		}
		return out;
	}
}
