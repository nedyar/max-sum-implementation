package execution;

import java.io.File;

import fileSystemUtil.FileSystemUtils;

public class EvaluateManyFeasibility {

	public static void execute(String path) {
		File startingPath = new File(path);
		File[] files = FileSystemUtils.getModelsFolder(startingPath);
		for (File file : files) {
			File[] subfolders = FileSystemUtils.getModelsFolder(file);
			for (File innerFile : subfolders) {
				String absolutePath = innerFile.getAbsolutePath();
				EvaluateFeasibility.execute(absolutePath + "/model.xml");				
			}
		}
	}
}