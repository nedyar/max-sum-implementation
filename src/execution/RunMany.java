package execution;

import java.io.File;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import fileSystemUtil.FileSystemUtils;

public class RunMany  {	

	public static void execute(String path, int timeoutInSec) {
		//Indica il numero di thread da eseguire contemporaneamente
		ExecutorService executor = Executors.newFixedThreadPool(4);

		//il timeout funziona bene ad eccezione ai casi in cui sarebbe utile, cio� in presenza di deadlock
		int timeoutSecs = timeoutInSec;	
		
		Future<String> future;
		/*
		 * carica cartella 
		 * per ogni file xml
		 */
		File startingPath = new File(path);
		File[] files = FileSystemUtils.getModelsFolder(startingPath);

		for (File file : files) {
			File[] subfolders = FileSystemUtils.getModelsFolder(file);
			for (File innerFile : subfolders) {
				String absolutePath = innerFile.getAbsolutePath();
				MainThread mt = new MainThread(absolutePath + "/model.xml");	
				
				future = executor.submit(mt);
				
				try {
					String prova = future.get(timeoutSecs, TimeUnit.SECONDS);
					System.out.println(prova);
				} catch (TimeoutException  e) {
					//ExecutionException: deliverer threw exception
					//TimeoutException: didn't complete within downloadTimeoutSecs
					//InterruptedException: the executor thread was interrupted

					System.err.println("Terminazione forzata (timeout) per il file: " + absolutePath);
					//interrupts the worker thread if necessary
					future.cancel(true);
				} catch (InterruptedException | ExecutionException e) {
					System.err.println("Errore di esecuzione per il file: " + absolutePath);
					//interrupts the worker thread if necessary
					future.cancel(true);
				}				
			}
		}	
		executor.shutdown();
	}
}