package execution;

import java.io.File;

import fileSystemUtil.FileSystemUtils;
import fileSystemUtil.FilterFolders;

public class MeasureManySolution {

	public static void execute(String path) {		
		File startingPath = new File(path);
		File[] files = startingPath.listFiles(new FilterFolders());
		
		for (File file : files) {
			File[] subfolders = FileSystemUtils.getModelsFolder(file);
			for (File innerFile : subfolders) {
				String absolutePath = innerFile.getAbsolutePath();
				MeasureSolution.execute(absolutePath + "/model.xml");
			}
		}
	}
}
