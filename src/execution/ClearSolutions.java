package execution;

import java.io.File;
import java.util.List;

import xml.XmlOptimizationHandler;

import fileSystemUtil.FileSystemUtils;
import fileSystemUtil.FilterFolders;
import graph.MSTweightCriterion;

public class ClearSolutions {

	/**
	 * Clear all the solution founded for the code specified criterions
	 */
	public static void execute(String path) {
		File startingPath = new File(path);
		File[] files = startingPath.listFiles(new FilterFolders());

		for (File file : files) {
			File[] subfolders = FileSystemUtils.getModelsFolder(file);
			for (File innerFile : subfolders) {
				String absolutePath = innerFile.getAbsolutePath();

				String xmlFilePath = absolutePath + "\\optimization-log.xml";

				XmlOptimizationHandler xmlHandler = new XmlOptimizationHandler(xmlFilePath);
				xmlHandler.loadXmlFile();

//				List<MSTweightCriterion> criterionsToClear = new ArrayList<MSTweightCriterion>();
//				criterionsToClear.add(MSTweightCriterion.CONSTRAINT_LESS);
//				criterionsToClear.add(MSTweightCriterion.CONSTRAINT_MORE);
//				criterionsToClear.add(MSTweightCriterion.ACYCLIC);
				List<MSTweightCriterion> criterionsToClear = MSTweightCriterion.values2();
				
				for (MSTweightCriterion criterion : criterionsToClear) {
					xmlHandler.clearCriterionSection(criterion);
				}
			}
		}
	}
}