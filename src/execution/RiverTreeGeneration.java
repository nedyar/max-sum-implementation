package execution;

import graph.GraphUtils;
import graph.RiverGraph;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.transform.TransformerConfigurationException;

import math.RangeInt;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.xml.sax.SAXException;

import Debug.Debug;
import Debug.Log;
import parameters.FlowType;
import parameters.Init;
import parameters.RiverGeneration;
import riverTree.River;
import riverTree.RiverNode;
import xml.XMLParamHandler;

/**
 * Generate a set of possible scenarios, without any model, but only the river object
 * 
 * @author Stefano
 *
 */
public class RiverTreeGeneration  {	

	public static void execute(String path, String paramFileStr, boolean acyclic) {	
		
		XMLParamHandler xmlParamHandler = new XMLParamHandler(paramFileStr);
		new Init(paramFileStr);
		int iteration = xmlParamHandler.getNumberOfIterations();
		RangeInt cityRange = xmlParamHandler.getNumberOfCities();
		RangeInt damRange = xmlParamHandler.getNumberOfDams();
		RangeInt riverRange = xmlParamHandler.getNumberOfRivers();
	
		try {
			//imposto staticamente flowtype a low, poich� questo � un fiume non un modello
			//deriva dalla vecchia implementazione del codice
			RiverTreeGeneration.generateManyRiver(path, iteration, cityRange, damRange, riverRange, FlowType.LOW, acyclic);
		} catch (TransformerConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}
	}
	
	private static void generateManyRiver(String folder, int iteration, RangeInt cityRange,
			RangeInt damRange, RangeInt riverRange, FlowType flowType, boolean acyclic) throws IOException, TransformerConfigurationException, SAXException{
		String path;
		File directory1 = new File(folder);
		if (!directory1.exists()){
			boolean isCreated = directory1.mkdir();
			
			if (isCreated){
				Debug.print("cartella creata");
			}
		}
		String zeroI = "0";
		String zeroR = "0";
		String zeroD = "0";
		String zeroC = "0";
		for (int i = riverRange.getStart(); i < riverRange.getEnd(); i++) {
			for (int j = damRange.getStart(); j < damRange.getEnd(); j++) {
				for (int k = cityRange.getStart(); k < cityRange.getEnd(); k++) {
					for (int l = 0; l < iteration; l++) {
						if (l > 9)	zeroI = "";
						if (k > 9)	zeroC = "";
						if (j > 9)	zeroD = "";
						if (i > 9)	zeroR = "";
						String subfolder = "/" + "R" + zeroR + i + "-" + "C" + zeroC + k + "-" + "D" + zeroD + j + "-" + zeroI + l;
						File directory = new File(folder + subfolder);
						if (!directory.exists()){
							directory.mkdir();
						}
						
						path = folder + subfolder + "/model.xml";
						generateScenario(path, i, k, j, flowType, acyclic);
					}
				}
			}
		}
	}
	
private static void generateScenario(String path, int rivers, int cities, int dams, FlowType flowType,
		boolean acyclic) throws IOException, TransformerConfigurationException, SAXException{
		
		String dir = new File(path).getParent();
		
//		Inizializzo il log per lo scenario attuale
		Log log = new Log(dir + "/Generation-LOG.xml");
		
		RiverGeneration riverGeneration = new RiverGeneration(rivers, cities, dams, flowType);
		River river = new River(riverGeneration, log, acyclic);	
		river.writeRiverTree(dir + "/river.obj");
		
		ArrayList<RiverNode> nodes = river.getAgentNodes();		

		Debug.print("nodi" + nodes.toString(), false);
		
		//Ottengo il grafo del fiume e lo esporto
		RiverGraph riverGraph = new RiverGraph(river.getAllNodes());
		GraphUtils<String, DefaultWeightedEdge> graphExporter = new GraphUtils<String, DefaultWeightedEdge>(riverGraph.getGraph());
		graphExporter.export(dir + "/River-Graph");
		
		if (!log.deleteLog()){
			//FIXME log � da eliminare una volta finito il processo, non funziona
			Debug.print("Impossibile eliminare il file: " + log.getPath());
		}
	}
}