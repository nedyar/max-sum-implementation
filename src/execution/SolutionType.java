package execution;

/**
 * Enumerator useful to determine problem on simulation. 
 * @author Stefano
 *
 */
public enum SolutionType {
	CORRECT, DEADLOCK, UNCONNECTED;
}
