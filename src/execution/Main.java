package execution;

import java.io.File;

import execution.retriveResults.RetriveManyMeasures;

public class Main {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//		/*
		//		 * Parametri:
		//		 * 	- eseguibile
		//		 *  - cartella o modello
		//		 *  - file dei parametri
		//		 *  - forza scenari aciclici (true|false)
		//		 *  
		//		 */
		Boolean multiple;
		Boolean acyclic = false;
		Boolean emptyParam = false; //true sse non � stato passato alcun parametro
		Boolean emptyAcyclic = false;
		String path;
		String paramFileStr = "";
		String simTimemoutStr = "";
		Integer simTimeout = 5;
		
		//FIXME andrebbe messo come ultimo parametro, l'ordine � infatti statico
		for (String string : args) {
			if (string.contains("-timeout")){
				simTimemoutStr = (string.split("="))[1];
			}						
		}
		if (!simTimemoutStr.equals("")){
			simTimeout = Integer.parseInt(simTimemoutStr);
		}
		

		if (args.length >= 2){
			//Secondo parametro file o cartella
			if (args[1] == null){
				System.err.println("Il secondo parametro non pu� essere vuoto.");
				showHelp();
			}
			path = args[1];
			File filePath = new File(path);
			if (!filePath.exists()){
				filePath.mkdir();
			}
			if (filePath.isDirectory()){
				//TODO controllo che contenga i modelli e che abbia senso uno scenario multiplo
				multiple = true;
			}
			else {
				multiple = false; 
			}

			//Terzo parametro file dei parametri
			if (args.length >= 3){
				if (args[2] == null){
					emptyParam = true;
				}		
				paramFileStr = args[2];
			}
			else {
				emptyParam = true;
			}

			//Quarto parametro forza la generazione di scenari aciclici
			if (args.length >= 4){
				if (args[3] == null){
						emptyAcyclic = true;
				}
				//Non posso usare parseBoolean in quanto restituisce false anche se non c'� scritto false ma pippo per es.
				else if (args[3].equals("false")){
					acyclic = false;
				}
				else if (args[3].equals("true")){
					acyclic = true;
				}
				else {
					System.err.println("Il quarto parametro non � corretto.");
					showHelp();
				}
			}
			else {
				emptyAcyclic = true;
			}

			//Primo parametro: eseguibile
			if (args[0] == null){
				System.err.println("Il primo parametro non pu� essere vuoto.");
				showHelp();
			}
			try {
				ExecutionType execType = ExecutionType.getExecutionType(args[0]);				
				switch (execType) {
				case HELP:	
					showHelp();
					break;
				case SIMULAZIONECOMPLETA:	
					if (multiple == false){
						System.err.println("Non si pu� eseguire una simulazione completa su un singolo file.");
						showHelp();
					}
					else if (emptyParam){
						System.err.println("Il terzo parametro non pu� essere vuoto.");
						showHelp();
					}
					else if (emptyAcyclic){
						System.err.println("Il quarto parametro non pu� essere vuoto.");
						showHelp();
					}
					//genero fiumi
					RiverTreeGeneration.execute(path, paramFileStr, acyclic);				
					//genero scenari relativi ai fiumi
					MainRiver.execute(path, paramFileStr);				
					//simulo per ogni scenario
					RunMany.execute(path, simTimeout);
					break;
				case GENERAFIUMI:	
					if (multiple == false){
						System.err.println("Non si possono generare fiumi su un singolo file.");
						showHelp();
					}
					else if (emptyParam){
						System.err.println("Il terzo parametro non pu� essere vuoto.");
						showHelp();
					}
					else if (emptyAcyclic){
						System.err.println("Il quarto parametro non pu� essere vuoto.");
						showHelp();
					}
					RiverTreeGeneration.execute(path, paramFileStr, acyclic);
					break;
				case GENERAMODELLI:	
					if (multiple == false){
						System.err.println("Non si possono generare modelli su un singolo file.");
						showHelp();
					}
					else if (emptyParam){
						System.err.println("Il terzo parametro non pu� essere vuoto.");
						showHelp();
					}
					MainRiver.execute(path, paramFileStr);
					break;	
				case GENERA:
					if (multiple == false){
						System.err.println("Non si possono generare fiumi e modelli su un singolo file.");
						showHelp();
					}
					else if (emptyParam){
						System.err.println("Il terzo parametro non pu� essere vuoto.");
						showHelp();
					}
					else if (emptyAcyclic){
						System.err.println("Il quarto parametro non pu� essere vuoto.");
						showHelp();
					}
					RiverTreeGeneration.execute(path, paramFileStr, acyclic);
					MainRiver.execute(path, paramFileStr);
					break;
				case SIMULA:
					if (multiple == false){
						new MainThread(path).call();
					}
					else{
						RunMany.execute(path, simTimeout);//, paramFileStr);					
					}
					break;
				case VALUTAFATTIBILITA:	
					if (multiple == false){
						EvaluateFeasibility.execute(path);
					}
					else{
						EvaluateManyFeasibility.execute(path);
					}
					break;			
				case CALCOLAMETRICHE:		
					if (multiple == false){
						MeasureSolution.execute(path);
					}
					else{
						MeasureManySolution.execute(path);
					}
					break;
				case OTTIENIRISULTATI:	
					if (multiple == false){
						System.err.println("Non si pu� ottenere la sintesi dei risultati su un sincolo modello.");
						showHelp();
					}
					else{
						RetriveManyMeasures.execute(path);
					}
					break;
				case BRUTEFORCE:
					if (multiple == false){
						BruteForce.execute(path);
					}
					else{
						BruteforceMany.execute(path);
					}
					break;
				case VALUTA:
					if (multiple == false){
						//TODO devo inserire la possibilit� di evitare il bruteforce
						boolean executeBF = true;
						EvaluateFeasibility.execute(path);
						if (!executeBF){
							MeasureManySolution.execute(path);
						}
					}
					else{
						//TODO devo inserire la possibilit� di evitare il bruteforce
						boolean executeBF = true;
						EvaluateManyFeasibility.execute(path);
						if (!executeBF){
							MeasureManySolution.execute(path);
						}
						RetriveManyMeasures.execute(path);
					}
					break;				
				default:
					System.err.println("Il primo parametro non � corretto.");
					showHelp();
					break;
				}				
			} catch (ExecutionTypeException e) {
				System.err.println("Primo parametro non riconosciuto!");
				showHelp();
			}
		}
		else {
			System.err.println("Errore nel numero dei parametri");
			showHelp();
		}
	}

	public static void showHelp(){
		System.out.println("Attenersi alla guida");
		System.out.println("Ordine dei parametri: <type> <path> <parameter_file> <force_acyclic> [-timeout=<integer_number>]");
		System.out.println("Dove:");
		System.out.println("<type> pu� essere:");
		System.out.println("	help o h");
		System.out.println("	simulazioneCompleta o sc");
		System.out.println("	generaFiumi o gf");
		System.out.println("	generaModelli o gm");
		System.out.println("	simula o s");
		System.out.println("	valutaFattibilita o vf");
		System.out.println("	calcolaMetriche o cm");
		System.out.println("	ottieniRisultati o or");
		System.out.println("	bruteforce o bf");
		System.out.println("	valuta o v");
		System.out.println("	genera o g");
		System.out.println("<path> pu� essere un file relativo a un modello XML oppure una cartella contenente pi� modelli, per la gerarchia di quest'ultima fare riferimento al fileREADME.txt.");
		System.out.println("<parameter_file> � il file XML contenente i parametri");
		System.out.println("<force_acyclic> pu� essere true o false a seconda che si voglia forzare la generazione di scenari con grafo fattorizzato aciclico (true) o meno (false). Di default vale false.");
		System.out.println("[-timeout=<integer_number>] un parametro facoltativo che permette di impostare il timeout utilizzato in caso di simulazione su un insieme di modelli, non � valido per un singolo modello.");
		System.exit(-1);
	}	
}
