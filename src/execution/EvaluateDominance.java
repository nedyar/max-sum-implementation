package execution;

import java.io.File;

import jmetal.qualityIndicator.util.MetricsUtil;

import Debug.Debug;

import fileSystemUtil.FileSystemUtils;
import graph.MSTweightCriterion;
import hypervolume.OptimizationType;
import xml.XmlOptimizationHandler;

public class EvaluateDominance {
	//questo systemInit si riferisce al modello ciclico originale
	private SystemInitializator systemInitializator;
	private String dir;
//	private String path;
	private XmlOptimizationHandler xmlHandler;
	private static boolean acyclic;

	public EvaluateDominance(String path) {
		this.dir = new File(path).getParent();
//		this.path = path;
		String xmlFilePath = dir + "\\optimization-log.xml";		
		xmlHandler = new XmlOptimizationHandler(xmlFilePath);
		xmlHandler.loadXmlFile();
		acyclic = xmlHandler.isAcyclic();
	}

	public static void execute(String path) {
		/*
		 * ottengo i modelli
		 * 		quindi ottengo tutte i vincoli
		 * ottengo la soluzione trovata, solo Uniform
		 * per ogni soluzione
		 * 		valuto se rispetta ciascun vincolo
		 */
		EvaluateDominance evaluateDominance = new EvaluateDominance(path);
		if (!acyclic){
			evaluateDominance.init();
			evaluateDominance.checkDominance();	
		}
	}

	private void init() {
		systemInitializator = new SystemInitializator();
		systemInitializator.inizializzaDatiXML(dir + "/model.xml", false);
	}

	private void checkDominance() {
		//inutile valutarlo se � Aciclico dato che trova la soluzione ottima
		for (MSTweightCriterion criterion : MSTweightCriterion.values2()) {	
			try{
				String bruteforcePath = dir + "/bruteforce.txt";				
				double bruteforce[][] = null;
				if (FileSystemUtils.existBruteforce(bruteforcePath)){
					bruteforce = new MetricsUtil().readFront(bruteforcePath);
				}
				else {
					System.err.println("Bruteforce non presente");
					System.exit(-1);
				}

				double costs[][] = xmlHandler.getFrontierCost(criterion, OptimizationType.COST);

				if (costs == null){
					Debug.print("Nessun costo trovato per il file: " + dir +  " tecnica: " +  criterion.toString());
				}
				else{
					
					boolean dominated = true;
					int i = 0;
					while (dominated && i < costs.length) {
						if (!dominatedByAtLeastOne(costs[i], bruteforce)){
							dominated = false;
						}
						i++;
					}
					xmlHandler.addDominance(criterion, dominated);
				}
			}
			catch (NullPointerException e){
				System.err.println("Problema con " + dir);
			}			
		}
	}

	private boolean dominatedByAtLeastOne(double[] ds, double[][] bruteforce) {
		int j=0;
		boolean out = false;
		while (!out && j < bruteforce.length) {
			
			if (isDominant(bruteforce[j],ds)){
				out = true;
			}
			j++;
		}
		return out;
	}

	private boolean isDominant(double[] ds, double[] dd) {
		//ds dominates dd
		boolean out = true;
		int i = 0;
		while (out == true && i < dd.length) {

			if (ds[i] > dd[i]){
				return false;
			}
			i++;
		}		
		return out;
	}
}
