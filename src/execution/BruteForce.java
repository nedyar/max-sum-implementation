package execution;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import xml.XmlOptimizationHandler;

import fileSystemUtil.FileSystemUtils;

import math.Evaluator;
import math.ExpressionSystem;
import math.VariableAssign;

import Debug.Debug;
import algorithm.Domain;
import algorithm.Variable;

/**
 * Bruteforce approach, to evaluate the Pareto frontier produced by Mo-Max-Sum.
 * @author Stefano
 *
 */
public class BruteForce {
	private static long start;
	private SystemInitializator systemInitializator;
	private ArrayList<Domain> domains;
	private XmlOptimizationHandler xmlOptimizationHandler;

	public static void execute(String path){
		start = System.nanoTime();
		Debug.print("start time: " + start * 1.0e-9);
		BruteForce bf = new BruteForce();
		bf.calculate(path);
	}
	
	/**
	 * Initialize the system to calculate the frontier by the bruteforce algorithm
	 * @param dir
	 */
	public void calculate(String path) {
		/*
		 * carico variabili
		 * carico funzioni
		 * 
		 * ottengo tutte possibili combinazioni delle variabili
		 * 	per ciascuna di queste calcolo valore
		 * 	le funzioni obbiettivo a parte
		 * 	poi le altre le sommo assieme e mi fermo se dovessi trovare il valore -oo
		 * 
		 */
		systemInitializator = new SystemInitializator();
		systemInitializator.inizializzaDatiXML(path, false);		
		
		File dirFile = (new File(path)).getParentFile();
		xmlOptimizationHandler = new XmlOptimizationHandler(dirFile.getAbsolutePath() + "/Optimization-LOG.xml");
		xmlOptimizationHandler.loadXmlFile();

		this.bruteforce(dirFile.getAbsolutePath() + "/bruteforce.txt");
	}

	/*
	 * Miglioramenti:
	 * 		- salvo i valori assunti dalle funzioni obbiettivo in base al valore del dominio, quindi non valuto
	 * 			ogni volta le espressioni
	 * 		- nel valutare le funzioni se supero la soglia allora � inutile continuare in quanto verrebbe comunque 
	 * 			scartato
	 * 		- eventualmente salvo le valutazioni precedenti e le uso nel caso in cui la funzione dipenda dai valori 
	 * 			del dominio rimasti invariati
	 * 		- ottimizzo l'inserimento di soluioni pareto dominanti rimuovendo in automatico quelle dominate
	 * 		- sfrutto appieno i core gestendo pi� thread
	 * 
	 * numero di combinazioni da testare:
	 * C = |D|^(#Agents)
	 * 
	 * quindi C * #f sono il numero di confonti con le funzioni
	 */
	//le prime iterazioni le fa abbastanza velocemente, verso la fine rallenta di molto
	//a causa del fatto che la frontiera � pi� vasta e quindi perde tempo a eliminare 
	//soluzioni dominate
	/**
	 * Generate the Pareto frontier using bruteforce approach
	 * @param path
	 */
	private void bruteforce(String path){
		Debug.print("Inizio bf per: " + path);
		/*
		 * per ogni variabile ottengo il suo dominio
		 * 	per ogni combinazione dei possibili domini
		 * 		valuto i valori per ogni condizione espressa dalle funzioni
		 * 		confronto il valore ottenuto con la frontiera di pareto temporanea
		 * 		aggiungo se  domina
		 */
		if (!FileSystemUtils.existBruteforce(path)){			
			domains = new ArrayList<Domain>();
			for (Variable variable : systemInitializator.getVariables()) {
				Domain d = variable.getDomain();
				domains.add(d);
			}

			HashMap<ArrayList<Float>, ArrayList<VariableAssign>> solutionMap = new HashMap<ArrayList<Float>, ArrayList<VariableAssign>>();
			ArrayList<ArrayList<Float>> paretoFrontier = new ArrayList<ArrayList<Float>>();

			//objectives ha come base 1, passo in convenzione con base 0 perch� � pi� comodo
			int objectives = systemInitializator.getObjectives() - 1;
			int debugIndex = 0;
			
			//soglia usata per simulare -infinito
			//FIXME da uniformare con resto del codice
			double threshold = -100000.0;

			//Tutte i vincoli del modello in esame
			List<ExpressionSystem> expressionSystems = systemInitializator.getFunctionExpressions();

			//genero le combinazioni dinamicamente per evitare di saturare la memoria!
			ArrayList<Float> combination = this.getFirstCombination();
			while (combination != null){
				debugIndex++;

				//elemento della forntiera di pareto potenziale
				ArrayList<Float> potentialParetoEl = new ArrayList<Float>(); 
				//che inizializzo a n+1 zeri
				for (int i = 0; i < objectives + 1; i++) {
					potentialParetoEl.add(new Float(0.0));
				}

				ArrayList<VariableAssign> varAssign = getVarAssignFromCombination(combination);

				//imposto una soglia per evitare che valori troppo negativi facciano parte della soluzione 
				//finale, non so quanto ci� possa essere corretto, ma Enrico fa una cosa simile bloccando i messaggi scambiati
				//inferiori alla soglia
				boolean overThreshold = false;

				//Qua devo ottenere tutte le funzioni del file xml e testarle sulla combinazione scelta
				//quindi calcolo il valore della soluzione potenziale che � composta dal valore delle funzioni obiettivo 
				//e nell'ultima posizione dalla somma dei valori delle funzioni vincolo			
				for (ExpressionSystem expressionSystem : expressionSystems) {
					Float value = Evaluator.evaluateFunction(expressionSystem, varAssign);
					if (value < threshold){
						overThreshold = true;
						break;
					}				

					//devo ottenede la posizione della funzione!
					//position ha come base 1, passo in convenzione con base 0 perch� � pi� comodo
					int position = expressionSystem.getPosition() - 1;

					if (position < objectives){
						potentialParetoEl.set(position, value);
					}
					else if (position == objectives){
						Float prevValue = potentialParetoEl.get(position);
						potentialParetoEl.set(position, value + prevValue);
					}
				}
				//Se non � gi� andato oltre la soglia valuto se � andato nel frattempo
				if (!overThreshold){
					for (Float float1 : potentialParetoEl) {
						if (float1 < threshold && !overThreshold){
							overThreshold = true;
						}
					}
				}
				//Se ancora non � oltre soglia controllo se � un elemento dominante
				if (!overThreshold){
					//valuto se potentialParetoEl � dominato, se non lo � lo aggiungo agli elementi della soluz di Pareto
					if (this.isParetoEl(potentialParetoEl, paretoFrontier)){
						paretoFrontier.add(potentialParetoEl);

						solutionMap.put(potentialParetoEl, varAssign);
					}
				}

				//ogni 10000 iterazioni stampo la combinazione in esame per monitorare l'andamento
				if (debugIndex > 10000){
					Debug.print(combination.toString());
					debugIndex = 0;
				}
				combination = this.nextCombination(combination);

				//lo faccio ad ogni combinazione poich� � pi� performante
				removeDominatedVectors(paretoFrontier);
			}

			//Rimuovo gli elementi dominati all'interno della frontiera di Pareto
			removeDominatedVectors(paretoFrontier);

			//da utilit� a costo
			ArrayList<ArrayList<Float>> costParetoFrontier = new ArrayList<ArrayList<Float>>();
			for (ArrayList<Float> arrayList : paretoFrontier) {
				ArrayList<Float> costSolution = new ArrayList<Float>();
				String toPrint = "";
				boolean printComa2 = false;

				ArrayList<VariableAssign> tempVarAssign = solutionMap.get(arrayList); 
				for (VariableAssign varA : tempVarAssign){
					if (printComa2){
						toPrint = toPrint + ", " + varA;
					}
					else {
						toPrint = toPrint + varA;
						printComa2 = true;
					}
				}
				toPrint = toPrint + " solution: ";

				boolean printComa = false;			

				for (Float float1 : arrayList) {
					costSolution.add(float1 * (-1));
					if (printComa){
						toPrint = toPrint + ", " + float1;
					}
					else {
						toPrint = toPrint + float1;
						printComa = true;
					}

				}
				costParetoFrontier.add(costSolution);
				Debug.print(toPrint);
			}
			
			long end = System.nanoTime();
			long execTime = end - start;				
			double execTimeSec = execTime * 1.0e-9;
			Debug.print("Elepsed time: " + execTimeSec);
			
			//save the elapsed time
			xmlOptimizationHandler.addBfExecutionTimeInSec(execTimeSec);
			
			saveParetoFrontier(path, costParetoFrontier);//, execTimeSec);			
			
			//check if the saved pareto it's equal to calculated one
//			ArrayList<ArrayList<Float>> costOnFile = loadParetoFrontier(path);
			
//			if (!costOnFile.equals(costParetoFrontier)){
//				System.err.println("soluzione salvata non coincidente con quella calcolata!");
//			}
		}
		else {
			System.err.println("bruteforce gi� calcolato per " + path);
		}
	}

	/**
	 * Save the file containing bruteforce Pareto frontier.
	 * At the end save the execution time in seconds.
	 */
	private void saveParetoFrontier(String path, ArrayList<ArrayList<Float>> partoFrontier) {//, double execTimeSec) {
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(path);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			for (ArrayList<Float> arrayList : partoFrontier) {
				int size = arrayList.size();
				for (int i = 0; i < size - 1; i++) {
					bufferedWriter.write(arrayList.get(i).toString());
					bufferedWriter.write(" ");
				}
				//stampa ultimo elemento
				bufferedWriter.write(arrayList.get(size - 1).toString());

				//ritorna a capo
				bufferedWriter.newLine();
			}
//			bufferedWriter.write("" + execTimeSec);
			bufferedWriter.flush();
			bufferedWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the next combination of assignment given the actual one.
	 * @param combination The actual combination
	 * @return This method returns the next combination tuple if it exists, or null if doesn't
	 */
	private ArrayList<Float> nextCombination(ArrayList<Float> combination) {
		ArrayList<Float> out = new ArrayList<Float>();
		boolean end = false;
		int size = combination.size();
		int index = size;
		index--;
		while (!end && index >= 0) {
			float tempVal = combination.get(index);
			Float nextValue = domains.get(index).nextDomainValue(tempVal);
			if (nextValue != null){
				for (int i = 0; i < index; i++) {
					out.add(combination.get(i));
				}
				out.add(nextValue);

				//fill up the remain position to get an arrayList of exactly "size" elements
				for (int i = index + 1; i < size; i++) {
					out.add(domains.get(i).getElement(0));
				}
				return out;
			}
			else {
				index--;
			}
		}		
		return null;
	}

	/**
	 * 
	 * @return This method returns the first combination tuple based on domains
	 */
	private ArrayList<Float> getFirstCombination() {
		ArrayList<Float> out = new ArrayList<Float>();
		for (Domain domain : domains) {
			out.add(domain.getElement(0));
		}
		return out;
	}

	//FIXME ottengo il nome delle variabili in base alla posizione, non � detto che vi sia corrispondenza per�
	private ArrayList<VariableAssign> getVarAssignFromCombination(ArrayList<Float> combination){
		ArrayList<VariableAssign> out = new ArrayList<VariableAssign>();
		int i = 1;
		for (Float float1 : combination) {
			out.add(new VariableAssign("x" + i,float1));
			i++;
		}		
		return out;
	}

	//remove also dominated elements
	private boolean isParetoEl(ArrayList<Float> potentialParetoEl, ArrayList<ArrayList<Float>> paretoFrontier){
		if (paretoFrontier.isEmpty()){
			return true;
		}
		for (ArrayList<Float> tempPareto : paretoFrontier) {
			for (int i = 0; i < tempPareto.size(); i++) {
				if (potentialParetoEl.get(i) > tempPareto.get(i)){
					//salvo l'elemento da rimouvere dalla frontiera
					return true;
				}
			}			
		}
		//Rimuovo elementi dalla frontiera
		return false;
	}

	//devo rimuovere anche quelli che hanno utilit� -infinito?
	private ArrayList<ArrayList<Float>> removeDominatedVectors(ArrayList<ArrayList<Float>> r_line){
		for(int i=0;i<r_line.size()-1;i++)
			for(int j=i+1;j<r_line.size();j++)
				if(dominates(r_line.get(i), r_line.get(j))){
					r_line.remove(j);
					j--;
				}
				else if(dominates(r_line.get(j), r_line.get(i))){
					r_line.remove(i);
					i--;
					break;
				}					
		return r_line;
	}	

	//Se v1 domina v2 restituisce true
	private boolean dominates(ArrayList<Float> v1, ArrayList<Float> v2){
		boolean dominated = true;
		for(int i=0;i< systemInitializator.getObjectives();i++)
			if(v1.get(i)<v2.get(i))
				dominated = false;
		return dominated;
	}
}
