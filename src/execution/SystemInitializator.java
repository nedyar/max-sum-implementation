package execution;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import math.Evaluator;
import math.ExpressionSystem;
import math.VariableAssign;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.ElementFilter;
import org.jdom2.input.SAXBuilder;

//import xml.XMLParamHandler;

import Debug.Debug;
import algorithm.Domain;
import algorithm.Function;
import algorithm.Solution;
import algorithm.Variable;

public class SystemInitializator {

	private ArrayList<Variable> variables;
	private  ArrayList<Function> functions;
	private  Solution solution;
	private  int objectives;

	private List<Element> functionElements;

	public SystemInitializator(){
		variables = new ArrayList<Variable>();
		functions = new ArrayList<Function>();
		solution = new Solution(variables);
		functionElements = new ArrayList<Element>();
	}

	public ArrayList<Variable> getVariables() {
		return variables;
	}

	public ArrayList<Function> getFunctions() {
		return functions;
	}

	public Solution getSolution() {
		return solution;
	}

	public int getObjectives() {
		return objectives;
	}	

	public void inizializzaDatiXML(String path, boolean allFunctionCombination){
		
		SAXBuilder builder = new SAXBuilder();
		try{
			Document doc;
			try {
				doc = builder.build(path);				
				objectives = Integer.parseInt(doc.getRootElement().getAttributeValue("objectives"));

				List<Element> vars = doc.getRootElement().getContent(new ElementFilter("variables", null)).get(0).getContent(new ElementFilter("variable", null));
				this.allocVariables(objectives, vars);

				functionElements = doc.getRootElement().getContent(new ElementFilter("functions", null)).get(0).getContent(new ElementFilter("function", null));
				this.allocFunctions(objectives, functionElements, allFunctionCombination);

			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (JDOMException e) {
			e.printStackTrace();
		}
	}

	//FIXED ha molto codice in comune con allocFunction()
	public List<ExpressionSystem> getFunctionExpressions(){
		List<ExpressionSystem> out = new ArrayList<ExpressionSystem>();
		List<Element> funs = this.getFunctionElements();
		for (Element ef : funs) {			
			int position = Integer.parseInt(ef.getAttributeValue("pos"));

			ExpressionSystem system = initExpressionSystem(ef, position);

			out.add(system);
		}
		return out;
	}

	private ExpressionSystem initExpressionSystem(Element functionElement,
			int position) {
		List<Element> utilityExpressionElements = functionElement.getContent(new ElementFilter("utilities", null)).get(0).getContent(new ElementFilter("expr", null));
		List<Element> booleanExpressionElements = functionElement.getContent(new ElementFilter("booleans", null)).get(0).getContent(new ElementFilter("expr", null));
		ArrayList<String> utilityExpressions = new ArrayList<String>();
		ArrayList<String> booleanExpressions = new ArrayList<String>();

		if (utilityExpressionElements.size() == booleanExpressionElements.size()){
			for (int i = 0; i < utilityExpressionElements.size(); i++) {
				utilityExpressions.add(utilityExpressionElements.get(i).getText());
				booleanExpressions.add(booleanExpressionElements.get(i).getText());
			}
		}
		else {
			Debug.print("Errore nel file xml");
		}
		ExpressionSystem system = new ExpressionSystem(position);
		system.setBooleanExpressions(booleanExpressions);
		system.setUtilityExpressions(utilityExpressions);
		return system;
	}

	private List<Element> getFunctionElements(){
		return this.functionElements;
	}

	//l'ho modificato con il il nuovo modo di inserimento domini pi� compatto
	@SuppressWarnings("rawtypes")
	public void allocVariables(int objs, List<Element> vars){
		Iterator itr = vars.iterator();
		while(itr.hasNext()){
			Element ev = (Element) itr.next();
			Element ed = ev.getContent(new ElementFilter("domain", null)).get(0);

			/*
			 * carico i valori del dominio in una lista e poi chiamo 
			 * Domain d = createDomain(lista);
			 * 
			 */
			//prendo la stringa dei domini
			String valuesStr = ed.getValue();
			Domain domain = new Domain();

			String domainValuesStr[] = valuesStr.split(",");
			for (String string : domainValuesStr) {
				domain.addValueToDomain(new Float(Float.parseFloat(string)));
			}
			Variable v = new Variable(ev.getAttributeValue("name"), domain, objs, solution);
			variables.add(v);
		}
	}


	@SuppressWarnings("rawtypes")
	public void allocFunctions(int objs, List funs, boolean allFunctionCombination){
		Iterator itr = funs.iterator();
		while(itr.hasNext()){
			Element ef = (Element) itr.next();
			int position = Integer.parseInt(ef.getAttributeValue("pos"));
			Function f = new Function(ef.getAttributeValue("name"), position, objs, Integer.parseInt(ef.getAttributeValue("arity")));
			
			List<Variable> variables = initVariables(ef, f);

			ExpressionSystem system = initExpressionSystem(ef, position);

			if (allFunctionCombination){
				allocFunctionTuples(f, variables, system);
			}

			functions.add(f);			
		}
	}

	private List<Variable> initVariables(Element functionElement,
			Function function) {
		List<Element> scope = functionElement.getContent(new ElementFilter("scope", null)).get(0).getContent(new ElementFilter("var", null));
		createFunctionScope(function,scope);

		Iterator<Element> itrScope = scope.iterator();
		List<Variable> variables = new ArrayList<Variable>();

		while(itrScope.hasNext()){

			Element varElement = itrScope.next();
			Variable var = getVariableByName(varElement.getText());
			variables.add(var);
		}
		return variables;
	}

	public void createFunctionScope(Function f, List<Element> vars){
		Iterator<Element> itr = vars.iterator();
		while(itr.hasNext()){
			Element ev = itr.next();
			f.addNeighbour(getVariableByName(ev.getValue()));
			getVariableByName(ev.getValue()).addNeighbour(f);
		}
	}

	//create the whole function combination for all possible combination of domain in the scope
	public static void allocFunctionTuples(Function f, List<Variable> scope, ExpressionSystem system){
		ArrayList<Domain> domains = new ArrayList<Domain>(3);

		for (Variable variable : scope) {
			domains.add(variable.getDomain());
		}

		ArrayList<VariableAssign> current = new ArrayList<VariableAssign>(1);
		for (int i = 0; i < domains.size(); i++){
			current.add(new VariableAssign(scope.get(i).getName(), new Float(0.0)));
		}

		ArrayList<ArrayList<VariableAssign>> tuples = math.Utility.combine((ArrayList<Variable>) scope, current, null, 0);
		for (ArrayList<VariableAssign> tuple : tuples) {			
			f.addVariableTuple(math.Utility.convertVarAssignToFloat(tuple));
			f.addFunctionValue(Evaluator.evaluateFunction(system, tuple));
		}
	}

	public Variable getVariableByName(String name){
		for(int i=0; i<variables.size(); i++)
			if(variables.get(i).getName().equals(name))
				return variables.get(i);
		return null;
	}	
}