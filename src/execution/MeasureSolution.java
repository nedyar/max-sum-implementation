package execution;

import fileSystemUtil.FileSystemUtils;
import graph.MSTweightCriterion;
import hypervolume.HyperTest;
import hypervolume.HypervolumeJMetal;
import hypervolume.OptimizationType;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jmetal.qualityIndicator.util.MetricsUtil;


import Debug.Debug;

import xml.XmlOptimizationHandler;

public class MeasureSolution {

	public static void execute(String path) {
		/*
		 * ottieni tutti i file Optimization-Log.xml
		 * per ciascuno di essi controllo che vi sia bruteforce
		 * altrimenti lo creo
		 * confronto le 4 soluzioni applicando hypervolume (chiamata a programma esterno)
		 * l'output � salvato in un file che leggo e deduco le metriche
		 * salvo le metriche nel log
		 * traggo conclusioni
		 */
		String dir = (new File(path)).getParent();
		String xmlFilePath = dir + "/optimization-log.xml";
		String bruteforcePath = dir + "/bruteforce.txt";
		String modelPath = dir + "/model.xml";
		try {
			//se gi� esiste un bruteforce non lo ricalcolo 
			if (!FileSystemUtils.existBruteforce(bruteforcePath)){
				BruteForce.execute(modelPath);
			}

			XmlOptimizationHandler xmlHandler = new XmlOptimizationHandler(xmlFilePath);
			xmlHandler.loadXmlFile();

			if (!xmlHandler.isAcyclic()){
				double bruteforce[][] = null;
				if (FileSystemUtils.existBruteforce(bruteforcePath)){
					bruteforce = new MetricsUtil().readFront(bruteforcePath);
				}

				double cost[][];
				double feasibleCosts[][];

				//inutile negli scenari aciclici quindi uso values2()
				for (MSTweightCriterion criterion : MSTweightCriterion.values2()) {

					//costi
					cost = xmlHandler.getFrontierCost(criterion, OptimizationType.COST);
					feasibleCosts = xmlHandler.getFrontierCost(criterion, OptimizationType.FEASIBLE_COST);

					//Hypervolume
					String hypervolumeStr = getHypervolumeStr(bruteforce, cost);
					xmlHandler.changeHypervolume(criterion, hypervolumeStr, false);	
					try{
						if(Double.parseDouble(hypervolumeStr) == 0.0){
							List<HyperTest> tests = new ArrayList<HyperTest>();
							tests = getHyperTests(cost, bruteforce);
							xmlHandler.changeHyperTest(criterion, tests, false);
						}
					}
					catch (NumberFormatException e){
						//do nothing
					}
					//feasible
					String feasibleHypervolumeStr = getHypervolumeStr(bruteforce, feasibleCosts);
					xmlHandler.changeHypervolume(criterion, feasibleHypervolumeStr, true);
					try{
						if(Double.parseDouble(feasibleHypervolumeStr) == 0.0){
							List<HyperTest> tests = new ArrayList<HyperTest>();							
							tests = getHyperTests(feasibleCosts, bruteforce);
							xmlHandler.changeHyperTest(criterion, tests, true);
						}
					}
					catch (NumberFormatException e){
						//do nothing
					}

					//Generational Distance
					String genDistStr = getGenerationalDistanceStr(bruteforce, cost);
					xmlHandler.changeGenerationalDistance(criterion, genDistStr, false);	
					//feasible
					String feasibleGenDistStr = getGenerationalDistanceStr(bruteforce, feasibleCosts);
					xmlHandler.changeGenerationalDistance(criterion, feasibleGenDistStr, true);	

					//Epsilon Indicator
					String epsilonStr = getEpsilonIndicatorStr(bruteforce, cost);
					xmlHandler.changeEpsilonIndicator(criterion, epsilonStr, false);	
					//feasible
					String feasibleEpsilonStr = getEpsilonIndicatorStr(bruteforce, feasibleCosts);
					xmlHandler.changeEpsilonIndicator(criterion, feasibleEpsilonStr, true);
				}
			}

		} catch (IOException e) {
			Debug.print("File xml errato o inesistente: " + dir);
		}
	}

	private static List<HyperTest> getHyperTests(double[][] cost, double[][] bruteforce) {
		List<HyperTest> out = new ArrayList<HyperTest>();
		double maxBf[] = new MetricsUtil().getMaximumValues(bruteforce, bruteforce[0].length);
		double minBf[] = new MetricsUtil().getMinimumValues(bruteforce, bruteforce[0].length);

		for (int i = 0; i < cost.length; i++) {

			List<Integer> zeroAt = zeroAt(cost[i], maxBf, minBf);
			if (zeroAt.size() > 0){
				out.add(new HyperTest(i, zeroAt));
			}
		}
		return out;
	}

	private static List<Integer> zeroAt(double[] ds, double[] maxBf, double[] minBf) {
		List<Integer> out = new ArrayList<Integer>();
		for (int i = 0; i < maxBf.length; i++) {

			//se max e min sono uguali salto alla posizione successiva
			if (maxBf[i] != minBf[i]){
				if (ds[i] >= maxBf[i]){
					out.add(i);
				}
			}
		}
		return out;
	}

	private static String getHypervolumeStr(double[][] bruteforce, double[][] cost) throws IOException {
		double hypervolume;
		if (cost == null && bruteforce == null){
			return "BF_Par_0_sol";
		}
		else if (cost == null){
			return "Pareto_0_sol";
		}
		else if (bruteforce == null){
			return "BF_0_sol";
		}
		else if (cost.length < 3  && bruteforce.length < 3){
			return "BF_Par_Poche_sol";
		}
		else if (cost.length < 3){
			return "Par_Poche_sol";
		}
		else if (bruteforce.length < 3){
			return "BF_Poche_sol";
		}
		else {
			hypervolume = HypervolumeJMetal.getHypervolume(bruteforce, cost);
			return "" + hypervolume;
		}
	}

	private static String getGenerationalDistanceStr(double[][] bruteforce, double[][] cost) throws IOException {
		double genDist;
		if (cost == null && bruteforce == null){
			return "BF_Par_0_sol";
		}
		else if (cost == null){
			return "Pareto_0_sol";
		}
		else if (bruteforce == null){
			return "BF_0_sol";
		}
		else if (cost.length < 2  && bruteforce.length < 2){
			return "BF_Par_Poche_sol";
		}
		else if (cost.length < 2){
			return "Par_Poche_sol";
		}
		else if (bruteforce.length < 2){
			return "BF_Poche_sol";
		}
		else {
			genDist = HypervolumeJMetal.getGenerationalDistance(bruteforce, cost);
			return "" + genDist;
		}
	}

	private static String getEpsilonIndicatorStr(double[][] bruteforce, double[][] cost) throws IOException {
		double epsilonInd;
		if (cost == null && bruteforce == null){
			return "BF_Par_0_sol";
		}
		else if (cost == null){
			return "Pareto_0_sol";
		}
		else if (bruteforce == null){
			return "BF_0_sol";
		}
		else if (cost.length < 2  && bruteforce.length < 2){
			return "BF_Par_Poche_sol";
		}
		else if (cost.length < 2){
			return "Par_Poche_sol";
		}
		else if (bruteforce.length < 2){
			return "BF_Poche_sol";
		}
		else {
			epsilonInd = HypervolumeJMetal.getEpsilonIndicator(bruteforce, cost);
			return "" + epsilonInd;
		}
	}
}
