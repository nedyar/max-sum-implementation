package execution.retriveResults;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//import org.jdom2.JDOMException;

import xml.XmlOptimizationHandler;
//import xml.XmlOptimizationHandler;


import fileSystemUtil.FilterFolders;
import graph.MSTweightCriterion;
import hypervolume.OptimizationType;

public class RetriveResults {
	
	public static void execute(String path) {

		BufferedWriter bufferedWriter;
		FileWriter fileWriter;

		try {
			fileWriter = new FileWriter(path + "results.txt");

			bufferedWriter = new BufferedWriter(fileWriter);

			File startingPath = new File(path);
			File[] files = startingPath.listFiles(new FilterFolders());

			bufferedWriter.write(path);
			bufferedWriter.newLine();

			for (File file : files) {
				File[] subfolders = file.listFiles(new FilterFolders());
				for (File innerFile : subfolders) {
					String absolutePath = innerFile.getAbsolutePath();

					bufferedWriter.write(file.getName() + "/" + innerFile.getName());
					bufferedWriter.newLine();

					XmlOptimizationHandler xmlHandler = new XmlOptimizationHandler(absolutePath + "/Optimization-LOG.xml");

					try {
						xmlHandler.loadXmlFile();

						writeNumberOfSolution(bufferedWriter, xmlHandler, OptimizationType.COST, false);

						if (!xmlHandler.isAcyclic()){
							//writeHypervolume(bufferedWriter, xmlHandler,  false);
							//writeGenerationalDistance(bufferedWriter, xmlHandler, false);
							//writeEpsilonIndicator(bufferedWriter, xmlHandler, false);

							writeNumberOfSolution(bufferedWriter, xmlHandler, OptimizationType.FEASIBLE_COST, true);

							writeHypervolume(bufferedWriter, xmlHandler, true);
							writeGenerationalDistance(bufferedWriter, xmlHandler, true);
							writeConstraintViolation(bufferedWriter, xmlHandler);							
							writeDominance(bufferedWriter, xmlHandler);
						}
						writeMessages(bufferedWriter, xmlHandler);
						writeExecutionTime(bufferedWriter,xmlHandler);

						bufferedWriter.newLine();
						bufferedWriter.newLine();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			bufferedWriter.flush();
			bufferedWriter.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private static void writeDominance(BufferedWriter bufferedWriter,
			XmlOptimizationHandler xmlHandler) throws IOException {
		writeMetric(bufferedWriter, xmlHandler, "bf_dominates");		
	}

	public static void writeExecutionTime(BufferedWriter bufferedWriter,
			XmlOptimizationHandler xmlHandler) throws IOException {
		bufferedWriter.write("   Execution Time [s]");
		bufferedWriter.newLine();
		String row = "      ";
		for (MSTweightCriterion criterion : getCriterions(xmlHandler)) {
			String valueStr = xmlHandler.getMetric(criterion, "B-MOMS_Execution_time_in_seconds");
			if (valueStr != null){
				row = row + " " + valueStr;
			}
			else {
				row = row + " ?";
			}			
		}
		bufferedWriter.write(row);
		bufferedWriter.newLine();

		bufferedWriter.write("   Bruteforce Execution Time [s]");
		bufferedWriter.newLine();
		String row2 = "      ";
		String valueStr = xmlHandler.getBruteforceExecTime();
		if (valueStr != null){
			row2 = row2 + " " + valueStr;
		}
		else {
			row2 = row2 + " ?";
		}			
		bufferedWriter.write(row2);
		bufferedWriter.newLine();
	}

	private static void writeMessages(BufferedWriter bufferedWriter,
			XmlOptimizationHandler xmlHandler) throws IOException {
		bufferedWriter.write("   No. of messages");
		bufferedWriter.newLine();
		String row = "      ";
		for (MSTweightCriterion criterion : getCriterions(xmlHandler)) {
			String valueStr = xmlHandler.getMetric(criterion, "messages_numbers");
			if (valueStr != null){
				row = row + " " + valueStr;
			}
			else {
				row = row + " ?";
			}			
		}
		bufferedWriter.write(row);
		bufferedWriter.newLine();		
	}

	private static void writeConstraintViolation(BufferedWriter bufferedWriter,
			XmlOptimizationHandler xmlHandler) throws IOException {

		writeMetric(bufferedWriter, xmlHandler, "mean_violation");
		writeMetric(bufferedWriter, xmlHandler, "stDev_violation");
		writeMetric(bufferedWriter, xmlHandler, "max_violation");			
	}

	private static void writeMetric(BufferedWriter bufferedWriter, XmlOptimizationHandler xmlHandler, 
			String metricStr) throws IOException {
		bufferedWriter.write("   " + metricStr);
		bufferedWriter.newLine();
		String row = "      ";
		for (MSTweightCriterion criterion : getCriterions(xmlHandler)) {
			String valueStr = xmlHandler.getMetric(criterion, metricStr);
			if (valueStr != null){
				row = row + " " + valueStr;
			}
			else {
				row = row + " ?";
			}			
		}
		bufferedWriter.write(row);
		bufferedWriter.newLine();		
	}

	private static void writeNumberOfSolution(BufferedWriter bufferedWriter,
			XmlOptimizationHandler xmlHandler, OptimizationType optimizationType,			
			boolean feasible) throws IOException {

		String feasString = "";
		if (feasible) feasString = "feasible";
		bufferedWriter.write("   No. of " + feasString  + " solutions");
		bufferedWriter.newLine();
		String row = "      ";
		
		for (MSTweightCriterion criterion : getCriterions(xmlHandler)) {
			String numberOfSol = xmlHandler.getNumberOfSolution(criterion, optimizationType);
			if (numberOfSol != null){
				row = row + " " + numberOfSol;
			}
			else {
				row = row + " 0";
			}
		}
		bufferedWriter.write(row);
		bufferedWriter.newLine();
	}

	private static void writeHypervolume(BufferedWriter bufferedWriter,	
			XmlOptimizationHandler xmlHandler, boolean feasible) throws IOException {	
		writeGenericMetricValue("hypervolume", bufferedWriter, xmlHandler, feasible);
	}

	private static void writeGenerationalDistance(BufferedWriter bufferedWriter,	
			XmlOptimizationHandler xmlHandler,boolean feasible) throws IOException {
		writeGenericMetricValue("generational_distance", bufferedWriter, xmlHandler, feasible);		
	}

	@SuppressWarnings("unused")
	private static void writeEpsilonIndicator(BufferedWriter bufferedWriter,
			XmlOptimizationHandler xmlHandler, boolean feasible) throws IOException {
		writeGenericMetricValue("epsilon_indicator", bufferedWriter, xmlHandler, feasible);			
	}

	private static void writeGenericMetricValue(String metricName, BufferedWriter bufferedWriter,
			XmlOptimizationHandler xmlHandler, boolean feasible) throws IOException {

		String feasString = "";
		if (feasible) feasString = "   feasible_";
		else feasString = "   ";
		String newMetricName = feasString + metricName;
		bufferedWriter.write(newMetricName);
		bufferedWriter.newLine();

		String row = "      ";
		for (MSTweightCriterion criterion : getCriterions(xmlHandler)) {
			String valueStr = xmlHandler.retriveGenericMeasureString(newMetricName.trim(), criterion);
			if (valueStr != null){
				row = row + " " + valueStr;
			}
			else {
				row = row + " " + "?";
			}
		}
		bufferedWriter.write(row);
		bufferedWriter.newLine();
	}	

	public static List<MSTweightCriterion> getCriterions(XmlOptimizationHandler xmlHandler){
		List<MSTweightCriterion> out = new ArrayList<MSTweightCriterion>();
		if (xmlHandler.isAcyclic()){
			out.add(MSTweightCriterion.ACYCLIC);
		}
		else {
			return MSTweightCriterion.values2();
		}
		return out;
	}
}