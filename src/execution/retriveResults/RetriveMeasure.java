package execution.retriveResults;

import graph.MSTweightCriterion;
import hypervolume.OptimizationType;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import xml.XmlOptimizationHandler;
import fileSystemUtil.FilterFolders;

public class RetriveMeasure {

	public static void execute(String path, String measureName) {
		
		BufferedWriter bufferedWriter;
		FileWriter fileWriter;

		try {
			fileWriter = new FileWriter(path + measureName + ".csv");

			bufferedWriter = new BufferedWriter(fileWriter);

			File startingPath = new File(path);
			File[] files = startingPath.listFiles(new FilterFolders());

			String colNames = "#scenario";
			for (MSTweightCriterion criterion : MSTweightCriterion.values2()) {
				colNames = colNames + "," + criterion.toString();
			}
			bufferedWriter.write(colNames);
			bufferedWriter.newLine();

			for (File file : files) {
				File[] subfolders = file.listFiles(new FilterFolders());
				for (File innerFile : subfolders) {
					String absolutePath = innerFile.getAbsolutePath();

					XmlOptimizationHandler xmlHandler = new XmlOptimizationHandler(absolutePath + "/Optimization-LOG.xml");

					try {
						xmlHandler.loadXmlFile();

						writeGenericMeasure(bufferedWriter, xmlHandler, innerFile, measureName);
						

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			bufferedWriter.flush();
			bufferedWriter.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public static void writeGenericMeasure(BufferedWriter bufferedWriter,
			XmlOptimizationHandler xmlHandler, File filePath, String measureName) throws IOException {		
		
		String parent = filePath.getAbsoluteFile().getParentFile().getName();
		String name = filePath.getAbsoluteFile().getName();
		String row = "" + parent + "/" + name;
		for (MSTweightCriterion criterion : MSTweightCriterion.values2()) {
			String metricValue = null;
			
			if (measureName.equals("number_of_solutions")){
				metricValue = xmlHandler.getNumberOfSolution(criterion, OptimizationType.COST);
			}
			else if (measureName.equals("number_of_feasible_solutions")){
				metricValue = xmlHandler.getNumberOfSolution(criterion, OptimizationType.FEASIBLE_COST);
			}
			else if (measureName.equals("ratio_feasible")){
				String sol = xmlHandler.getNumberOfSolution(criterion, OptimizationType.COST);
				String feasSol = xmlHandler.getNumberOfSolution(criterion, OptimizationType.FEASIBLE_COST);
				try{
					double ratio = Double.parseDouble(feasSol)/Double.parseDouble(sol);
					metricValue = "" + ratio;
				} //l'eccezione � inutile tanto � NaN senza sollevare eccezioni (o forse NaN � perch� uno dei due � gi� ?)?
				catch (Exception e){
					metricValue = "?";
				}
			}
			else {
				metricValue = xmlHandler.getMetric(criterion, measureName);
			}
			if (metricValue != null){
				row = row + "," + metricValue;
			}
			else {
				row = row + ",?";
			}	
		}
		bufferedWriter.write(row);
		bufferedWriter.newLine();
	}
}