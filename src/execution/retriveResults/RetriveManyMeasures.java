package execution.retriveResults;

public class RetriveManyMeasures {

	public static void execute(String path) {		
		String measures[] = {"hypervolume","feasible_hypervolume","generational_distance",
				"feasible_generational_distance","mean_violation","max_violation","stDev_violation","messages_numbers",
				"number_of_feasible_solutions", "number_of_solutions", "ratio_feasible", "B-MOMS_Execution_time_in_seconds",
				"MST_Execution_time_in_seconds", "bf_dominates"};
		
		RetriveResults.execute(path);
		
		for (String measureName : measures) {
			RetriveMeasure.execute(path, measureName);
		}			
	}
}
