package execution.retriveResults;

import graph.MSTweightCriterion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import xml.XmlOptimizationHandler;
import fileSystemUtil.FilterFolders;

public class RetriveScalability {
	
	public static void execute(String path) {
		BufferedWriter bufferedWriter;
		FileWriter fileWriter;

		try {
			fileWriter = new FileWriter(path + "execution-time.txt");

			bufferedWriter = new BufferedWriter(fileWriter);

			File startingPath = new File(path);
			File[] files = startingPath.listFiles(new FilterFolders());

			bufferedWriter.write("#scenario, MO-MaxSum Time, Bf Time");
			bufferedWriter.newLine();

			for (File file : files) {
				File[] subfolders = file.listFiles(new FilterFolders());
				for (File innerFile : subfolders) {
					String absolutePath = innerFile.getAbsolutePath();

					XmlOptimizationHandler xmlHandler = new XmlOptimizationHandler(absolutePath + "/Optimization-LOG.xml");

					try {
						xmlHandler.loadXmlFile();						
						writeRowExecutionTime(bufferedWriter, xmlHandler, innerFile);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			bufferedWriter.flush();
			bufferedWriter.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public static void writeRowExecutionTime(BufferedWriter bufferedWriter,
			XmlOptimizationHandler xmlHandler, File filePath) throws IOException {

		String parent = filePath.getAbsoluteFile().getParentFile().getName();
		String name = filePath.getAbsoluteFile().getName();
		String row = "" + parent + "/" + name + ", ";
		MSTweightCriterion criterion = MSTweightCriterion.ACYCLIC;
		String moMaxSumExecTimeStr = xmlHandler.getMetric(criterion, "B-MOMS_Execution_time_in_seconds");
		if (moMaxSumExecTimeStr != null){
			row = row +  moMaxSumExecTimeStr + ", ";
		}
		else {
			row = row + "?, ";
		}	

		String bfExecTimeStr = xmlHandler.getBruteforceExecTime();
		if (bfExecTimeStr != null){
			row = row + bfExecTimeStr;
		}
		else {
			row = row + "?";
		}	
		bufferedWriter.write(row);
		bufferedWriter.newLine();
	}
}