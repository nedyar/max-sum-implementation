package execution;

public enum ExecutionType {
	HELP, GENERAFIUMI, GENERAMODELLI, GENERA, SIMULA, SIMULAZIONECOMPLETA,
	VALUTAFATTIBILITA, CALCOLAMETRICHE, OTTIENIRISULTATI, VALUTA, BRUTEFORCE;
	
	public String getAbbreviation(){
		switch (this) {
		case HELP:
			return "h";
		case GENERAFIUMI:
			return "gf";
		case GENERAMODELLI:
			return "gm";
		case GENERA:
			return "g";
		case SIMULA:
			return "s";
		case SIMULAZIONECOMPLETA:
			return "sc";
		case VALUTAFATTIBILITA:
			return "vf";
		case CALCOLAMETRICHE:
			return "cm";
		case OTTIENIRISULTATI:
			return "or";
		case VALUTA:
			return "v";
		case BRUTEFORCE:
			return "bf";			
		default:
			return null;
		}
	}
	
	@Override
	public String toString(){
		switch (this) {
		case HELP:
			return "help";
		case GENERAFIUMI:
			return "generaFiumi";
		case GENERAMODELLI:
			return "generaModelli";
		case GENERA:
			return "genera";
		case SIMULA:
			return "simula";
		case SIMULAZIONECOMPLETA:
			return "simulazioneCompleta";
		case VALUTAFATTIBILITA:
			return "valutaFattibilita";
		case CALCOLAMETRICHE:
			return "calcolaMetriche";
		case OTTIENIRISULTATI:
			return "ottieniRisultati";
		case VALUTA:
			return "valuta";
		case BRUTEFORCE:
			return "bruteforce";			
		default:
			return null;
		}
	}	
	
	public static ExecutionType getExecutionType(String execTypeStr) throws ExecutionTypeException{		
		for (ExecutionType type : ExecutionType.values()) {
			if (execTypeStr.equals(type.toString()) || execTypeStr.equals(type.getAbbreviation())){
				return type;
			}
		}
		throw new ExecutionTypeException();
	}
}
