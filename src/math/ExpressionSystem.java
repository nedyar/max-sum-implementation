package math;

import java.util.ArrayList;

public class ExpressionSystem {

	//indica l'indice a cui si riferisce il vincolo
	private int position;
	private ArrayList<String> utilityExpressions;
	private ArrayList<String> booleanExpressions;
	
	public ExpressionSystem(int position){
		this.position = position;
	}
	
	public int getPosition(){
		return this.position;
	}
	
	public ArrayList<String> getUtilityExpressions() {
		return utilityExpressions;
	}
	public void setUtilityExpressions(ArrayList<String> utilityExpressions) {
		this.utilityExpressions = utilityExpressions;
	}
	public ArrayList<String> getBooleanExpressions() {
		return booleanExpressions;
	}
	public void setBooleanExpressions(ArrayList<String> constraintExpressions) {
		this.booleanExpressions = constraintExpressions;
	}
	
	public String toString(){
		return "{boolExpr: " + booleanExpressions.toString() + ", utilityExpr: " + utilityExpressions.toString() + 
				", pos: " + position + "}";
	}	
}
