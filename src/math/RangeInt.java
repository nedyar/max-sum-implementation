package math;

//FIXME non sembra funzionare bene sui cicli, se i due numeri sono uguali non cicla su nulla
public class RangeInt {
	private boolean inclusive;
	private int start;
	private int end;
	
	public RangeInt(boolean inclusive, int start, int end) {
		super();
		this.inclusive = inclusive;
		if (!inclusive){
			this.start = start + 1;
			this.end = end -1;
		}
		else {
			this.start = start;
			this.end = end;
		}		
	}
	
	public RangeInt(int start, int end) {
		this (true, start, end);
	}

	public boolean isInclusive() {
		return inclusive;
	}

	public void setInclusive(boolean inclusive) {
		this.inclusive = inclusive;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}	
}
