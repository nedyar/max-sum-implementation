package math;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mvel2.MVEL;

import mathexpressioncalculator.Calculator;

public class Evaluator {
	//Da usare per inizializzare il valore assunto da una funzione per un dato vincolo
	@SuppressWarnings("unused")
	private static ArrayList<Float> functionEvaluation(String expression, ArrayList<ArrayList<VariableAssign>> tuples){
		ArrayList<Float> out = new ArrayList<Float>();

		for (ArrayList<VariableAssign> tuple : tuples){
			out.add(evaluateFunction(expression, tuple));
		}		
		return out;
	}
		
	public static boolean isSatisfied(ExpressionSystem system, List<VariableAssign> tuple){
		Float value = Evaluator.evaluateFunction(system, tuple);
		//FIXME la soglia � da impostare a parte!
		if (value < -50000){
			return false;
		}
		return true;
	}
	
	public static Float evaluateFunction(ExpressionSystem system, List<VariableAssign> tuple){
		ArrayList<String> booleanExpressions = system.getBooleanExpressions();
		ArrayList<String> utilityExpressions = system.getUtilityExpressions();

		String tempBooleanExpression = booleanExpressions.get(0);
		Map<String, Float> vars = new HashMap<String, Float>();	

		//carico le variabili
		for (VariableAssign variableAssign : tuple) {
			String name = variableAssign.getName();
			Float value = variableAssign.getValue();
			vars.put(name, value);			
		}		
		Boolean boolResult = (Boolean) MVEL.eval(tempBooleanExpression,vars);

		int i = 1;
		while (!boolResult && i < booleanExpressions.size()){
			tempBooleanExpression = booleanExpressions.get(i);
			boolResult = (Boolean) MVEL.eval(tempBooleanExpression,vars);
			i++;
		}
		i--;
		Object temp = MVEL.eval(utilityExpressions.get(i),vars);
		String str = temp.toString();
		return Float.parseFloat(str);
	}	

	//FIXME perch� non uso anche qua MVEL??
	//Credo sia codice vecchio quando non usavo ancora MVEL poi mai modificato con 
	//l'introduzione di ques'ultimo
	public static Float evaluateFunction(String expression, /*Array*/List<VariableAssign> tuple){
		Calculator cal = new Calculator(expression);
		for (VariableAssign variableAssign : tuple) {
			String name = variableAssign.getName();
			Float value = variableAssign.getValue();
			cal.setDoubleVariable(name,	value);			
		}
		return (float) cal.getResult();
	}
}