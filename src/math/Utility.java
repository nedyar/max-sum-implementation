package math;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import riverTree.ComparisonType;

import algorithm.Domain;
import algorithm.Variable;

import Debug.Debug;

public class Utility {

	/** 
	 * @param start
	 * @param end
	 * @return Return a random int between start(inclusive) and end(inclusive)
	 */
	public static int randomInt(int start, int end){
		Random random = new Random();
		int out = start + random.nextInt((end - start) + 1);
		return out;
	}

	public static double randomGaussian(double mean, double standardDev) {
		Random random = new Random();
		double x = random.nextGaussian();
		double out = -0.5;
		out = mean + x * standardDev * standardDev;

		//arrotondo alla terza decimale
		out = Utility.round3(out);
		return out;
	}

	public static double randomPositiveGaussian(double mean, double standardDev){
		double out = -0.5;
		int i = 0;
		while (out < 0) {
			out = Utility.randomGaussian(mean, standardDev);			
			if (i > 1000){
				i = 0;
				Debug.print("Generazione valore positivo Gaussiana improbabile!");
			}
			i++;
		}				
		return out;
	}

	/**
	 * 
	 * @param d The number to approximate
	 * @return The approximation to the third decimal number
	 */
	public static double round3(double d) {
		double out = d;				
		//arrotondo alla terza decimale
		out = Math.round(out*1000);
		out = out/1000;
		return out;
	}

	//versione con variableAssign
	@SuppressWarnings("unchecked")
	public static ArrayList<ArrayList<VariableAssign>> combine(ArrayList<Variable> variables, ArrayList<VariableAssign> current, ArrayList<ArrayList<VariableAssign>> all, int k){ //, int[] current, int k) {
		ArrayList<Domain> domains = new ArrayList<Domain>();
		for (Variable variable : variables) {
			domains.add(variable.getDomain());
		}


		if (all == null){
			all = new ArrayList<ArrayList<VariableAssign>>();
		}

		if(k == domains.size()) {
			all.add(current);
		} else {            
			for(int j = 0; j < domains.get(k).getValues().size(); j++) {
				VariableAssign varAssing = new VariableAssign(variables.get(k).getName(), domains.get(k).getValues().get(j));
				current.set(k, varAssing);	 
				combine(variables, (ArrayList<VariableAssign>) current.clone(), all, k + 1);
			}       
		}	    
		return all;
	}

	public static ArrayList<Float> convertVarAssignToFloat(ArrayList<VariableAssign> variableAssignments){
		ArrayList<Float> out = new ArrayList<Float>();
		for (VariableAssign variableAssign : variableAssignments) {
			out.add(variableAssign.getValue());
		}
		return out;
	}

	//Restituisce una lista di lista con tutte le possibili combinazioni delle variabili prese come dominio, insomma il prodotto scalare
	//Per una versione pi� generale vedi progetto Guava.Sets.cartesianProduct
	//https://code.google.com/p/guava-libraries/
	@SuppressWarnings("unchecked")
	public static ArrayList<ArrayList<Float>> combine2(ArrayList<Domain> domains, ArrayList<Float> current, ArrayList<ArrayList<Float>> all, int k){ //, int[] current, int k) {
		if (all == null){
			all = new ArrayList<ArrayList<Float>>();
		}
		if(k == domains.size()) {
			all.add(current);
		} else {            
			for(int j = 0; j < domains.get(k).getValues().size(); j++) {
				current.set(k, domains.get(k).getValues().get(j));	            
				combine2(domains, ((ArrayList<Float>) current.clone()), all, k + 1);
			}       
		}	    
		return all;
	}

	public static double calculateMean(Map<List<VariableAssign>, List<Float>> violationMap) {
		double sum = 0;
		int elements = 0;

		for (List<VariableAssign> assign : violationMap.keySet()) {
			List<Float> tempViolations = violationMap.get(assign);
			sum = sum + Utility.sum(tempViolations);
			elements = elements + tempViolations.size();
		}		
		return sum/elements;
	}

	public static double calculateStandardDeviation(Map<List<VariableAssign>, List<Float>> violationMap) {
		double sum = 0;
		int elements = 0;
		double mean = Utility.calculateMean(violationMap);

		for (List<VariableAssign> assign : violationMap.keySet()) {
			List<Float> tempViolations = violationMap.get(assign);
			for (Float float1 : tempViolations) {
				double deviation = float1 - mean;
				sum = sum + (deviation * deviation);
				elements = elements + 1;
			}	
		}		
		return Math.sqrt(sum/elements);
	}

	public static Float sum(List<Float> list) {
		Float sum= (float) 0; 
		for (Float i : list)
			sum = sum + i;
		return sum;
	}

	public static double calculateMax(Map<List<VariableAssign>, List<Float>> violationMap) {
		double max = 0;
		for (List<VariableAssign> assign : violationMap.keySet()) {
			List<Float> tempViolations = violationMap.get(assign);
			double tempMax = Collections.max(tempViolations);
			max = Math.max(max, tempMax);				
		}		
		return max;
	}

	//Return true iff the scope is positive and operator is of type greater, 
	//or scope is negative and operator is less than
	//FIXME con un'espressione del tipo "--x" x sar� segnata come negativa!!
	public static boolean isConcordant(String scopeStr, String expression, ComparisonType comparisonType) {
		boolean positive;
		int variableIndex = expression.indexOf(scopeStr);

		if (variableIndex >= 0){
			//the character before the variable
			char tempChar;
			if (variableIndex > 0){
				tempChar = expression.charAt(variableIndex - 1);
			}
			else {
				tempChar = ' ';
			}

			if (tempChar == '-'){
				positive = false;
			}
			else {
				positive = true;
			}

			//greater is true when the operator state greater than or greater or equal
			boolean greater = (comparisonType.equals(ComparisonType.GT) || comparisonType.equals(ComparisonType.GTE));

			if (greater){
				if (positive){
					return true;
				}
				else {
					return false;
				}
			}
			else {
				if (!positive){
					return true;
				}
				else {
					return false;
				}
			}
		}
		else {
			System.err.println("Variabile " + scopeStr + " non presente nell'espressione: " + expression);
		}
		return false;
	}
}
