package math;

public class VariableAssign {	
	private String name;
	private Float value;

	public VariableAssign(String name, Float value) {
		super();
		this.value = value;
		this.name = name;
	}
	public Float getValue() {
		return value;
	}
	public void setValue(Float value) {
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	
	@Override
	public Object clone(){
		return new VariableAssign(this.getName(), this.getValue());
	}
	@Override
	public String toString() {
		return "<" + name + ", " + value + ">";
	}	
}
