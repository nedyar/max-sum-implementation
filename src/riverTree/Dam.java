package riverTree;

import parameters.DamNodeParameters;
import parameters.FlowType;

public class Dam extends RiverNode {
	private static final long serialVersionUID = 8059378117301495335L;

	public Dam(FlowType flowType) {
		super();
		super.setVariableName(RiverUtility.getRndVariableName());
		super.setParameters(new DamNodeParameters(flowType));
	}
	
	@Override
	public String calculateFlow(){
		String out = "";
		RiverNode inflow = super.getInflow();
		//Caso base		
		if (inflow == null){
			return out;
		}
		else {
			String previous = inflow.calculateFlow();
			if (previous != null){
				super.setInflowExpression(previous);
			}
			else {
				System.err.print("errore, espressione in uscita al nodo precedente nulla!");
			}
			
			out = super.getVariableName();
			super.setOutflowExpression(out);
			return out ;
		}
	}

	public void updateparameters(FlowType flowType) {
		super.setParameters(new DamNodeParameters(flowType));
	}
}