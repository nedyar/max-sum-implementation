package riverTree;

public enum ConstraintType {
	HARD, SOFT, MIXED, OBJECTIVE;
}
