package riverTree;

public enum ComparisonType {
	GT,	GTE, LT, LTE; 

	public ComparisonType opposite(){
		switch (this) {
		case GT:
			return LTE;
		case GTE:
			return LT;
		case LT:
			return GTE;
		case LTE:
			return GT;
		default:
			return null;
		}
	}
	
	@Override
	public String toString(){
		switch (this) {
		case GT:
			return ">";
		case GTE:
			return ">=";
		case LT:
			return "<";
		case LTE:
			return "<=";
		default:
			return null;
		}
	}

	public boolean meansGreater() {
		if (this.equals(GT) || this.equals(GTE)){
			return true;
		}
		return false;
	}
	
	public boolean meansLessThan() {
		return !meansGreater();
	}
	
	public static ComparisonType getComparisonTypeFromExpression(String expression) {
		if (expression.contains(ComparisonType.GTE.toString())) {
			return ComparisonType.GTE;
		}
		else if (expression.contains(ComparisonType.LTE.toString())) {
			return ComparisonType.LTE;
		}		
		else if (expression.contains(ComparisonType.GT.toString())) {
			return ComparisonType.GT;
		}
		else if (expression.contains(ComparisonType.LT.toString())) {
			return ComparisonType.LT;
		}		
		return null;
	}
}
