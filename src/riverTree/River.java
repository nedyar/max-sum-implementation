package riverTree;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import Debug.Debug;
import Debug.Log;

import math.Utility;

import parameters.FlowType;
import parameters.Init;
import parameters.RiverGeneration;

public class River implements Serializable{
	private static final long serialVersionUID = -3993277288375355814L;
	private int stackDimension;
	private RiverGeneration riverGenParam;
	
	//the list of list of river such as: [[C,C],[D,C,D],[D,D]]
	private ArrayList<ArrayOfTypes> riversList;

	//l'ultimo nodo del fiume
	private RiverNode endingNode;

	//il primo nodo di ciascun fiume
	private ArrayList<InitialFlow> startingNodes;

	//a random selection of node for instance: [C,C,D,C,D,D,D], thath with split=[2,5] makes the riverlist of the example
	private Stack<NodeType> randomStack;

	/**
	 * 
	 * @param parameters Parameters for the river generation, such as number of cities, dams and rivers
	 * @param log
	 * @throws IOException
	 */
	public River(RiverGeneration parameters, Log log, boolean acyclic) throws IOException{
		this.riverGenParam = parameters;
		this.stackDimension = parameters.getNumberOfCity() + parameters.getNumberOfDam();
		this.startingNodes = new ArrayList<InitialFlow>();
		initGlobalEnv();
		ArrayList<NodeType> allType = initAllType();
		randomStack = new Stack<NodeType>();
		
		for (int i = 0; i < stackDimension; i++) {
			Debug.print("" + i, false);
			randomStack.add(getRandomType(allType));
		}

		Debug.print("randome stack: " + randomStack.toString());
		genRivers();
		if (acyclic){
			modifyRiverList();
		}
		generateTreeStructure();
		setRiversStrictLength();
		joinRivers();
		testOutflows();
		initInitialflow(log);
		expandExpressions();
	}	

	//FIXME l'ultimo nodo potrebbe essere una citt�!
	//i nodi con indice > 0 diventano tutti dighe
	private void modifyRiverList() {
		for (ArrayOfTypes subRiver : riversList) {
			List<NodeType> list = subRiver.getTypes();
			int endIndex = list.size();

			//salto la prima posizione in quanto il primo nodo pu� essere una citt�
			for (int j = 1; j < endIndex ; j++) {
				if (list.get(j).equals(NodeType.CITY)){
					subRiver.setNode(j,NodeType.DAM);
				}
			}		
		}
	}

	/**
	 * Used to clear information regarding model parameters, so allow to create new model based on the same river's
	 *  structure.
	 * @param log
	 * @param flowType
	 * @throws IOException
	 */
	public void clearRiver(Log log, FlowType flowType) throws IOException{		
		initGlobalEnv();
		this.riverGenParam.setFlowType(flowType);	
		updateDamLevel(this.endingNode);
		initInitialflow(log);
		expandExpressions();
	}

	private void initGlobalEnv(){
		RiverUtility.previousNumberFunction = 0;
		RiverUtility.alreadyAssignedFunction = new ArrayList<String>();
		RiverUtility.previousNumber = 0;
		RiverUtility.alreadyAssigned = new ArrayList<String>();		
		RiverUtility.previousChar = 'z';
		RiverUtility.dumbAlreadyAssigned = new ArrayList<String>();
	}

	/**
	 * Change the dam level appropriately starting from ending node
	 * @param riverNode is the ending node of a river
	 */
	private void updateDamLevel(RiverNode riverNode) {
		/*
		 * per ogni nodo diga cambio il livello in base al flowType scelto
		 */
		if (riverNode != null){
			if (riverNode instanceof Dam){
				((Dam)riverNode).updateparameters(this.riverGenParam.getFlowType());
			}
			if (riverNode instanceof UnionNode){
				ArrayList<RiverNode> inflows = ((UnionNode) riverNode).getAdditionalInflows();
				for (RiverNode	inflow	: inflows) {
					updateDamLevel(inflow);
				}
			}			
			updateDamLevel(riverNode.getInflow());
		}		
	}

	/**
	 * Getter of final node of the river.
	 * @return Return the final node of the river.
	 */
	public RiverNode getEndingNode() {
		return endingNode;
	}

	public ArrayList<ArrayOfTypes> getRiversList() {
		return riversList;
	}

	public ArrayList<InitialFlow> getStartingNodes() {
		return startingNodes;
	}

	/**
	 * Check if all rivers flow in the same endingNode
	 */
	private void testOutflows(){
		String msg = "";
		for (InitialFlow river : startingNodes) {
			msg = msg + " " + obtainOutflow(river);
		}
		Debug.print("ramo finale: (dovrebbero essere tutti uguali) " + msg);
	}

	/**
	 * Initialize riverStructure saved in a tree traversed, up to now only, from leaves to root using the list {@code startingNodes}
	 * @throws IOException
	 */
	public void generateTreeStructure() throws IOException{
		for (ArrayOfTypes river : riversList) {
			ArrayList<RiverNode> allNodes = new ArrayList<RiverNode>();

			for (NodeType riverNodeType : river.getTypes()) {
				RiverNode tempNode = null;
				if (riverNodeType.equals(NodeType.CITY)) {
					tempNode = new City();
				}
				else if (riverNodeType.equals(NodeType.DAM)){
					tempNode = new Dam(riverGenParam.getFlowType());
				}
				allNodes.add(tempNode);				
			}

			InitialFlow actualNodeStart = new InitialFlow();

			actualNodeStart.setOutflow(allNodes.get(0));
			allNodes.get(0).setInflow(actualNodeStart);

			for (int i = 0; i < allNodes.size(); i++) {

				if (i > 0){
					allNodes.get(i).setInflow(allNodes.get(i - 1));
				}

				if (i < allNodes.size() - 1){
					allNodes.get(i).setOutflow(allNodes.get(i + 1));
				}
			}
			this.startingNodes.add(actualNodeStart);
		}
		Debug.print("lista dei fiumi: " + startingNodes);
	}

	/**
	 * Write entire river into a file by serialization.
	 * @param path The complete path in which the save is performed.
	 * @throws IOException
	 */
	public void writeRiverTree(String path) throws IOException{
		FileOutputStream fos = new FileOutputStream(path);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(this);
		oos.close();
	}

	/**
	 * @return Return a list of splitting indexes and adding two boundaries at starting and ending positions respectively on -1 and 
	 * stack dimension - 1.
	 */
	private ArrayList<Integer> generateRandomSplit(){
		if (riverGenParam.getNumberOfRiver() + 1 >= stackDimension){
			System.err.println("impossibile generare pi� split degli agenti in questione!");
		}

		int i = 0;
		int start = 0;
		int end = randomStack.size() - 1;
		Random rnd = new Random();
		ArrayList<Integer> randomSplit =  new ArrayList<Integer>();
		Integer randomInt =  (int) (start + ((end-start) * rnd.nextDouble()));
		randomSplit.add(randomInt);
		while (i < riverGenParam.getNumberOfRiver() - 2) {
			randomInt =  (int) (start + ((end-start) * rnd.nextDouble()));
			if (!(randomSplit.contains(randomInt))){
				randomSplit.add(randomInt);
				i++;
			}			
		}
		Collections.sort(randomSplit);

		randomSplit.add(0, new Integer(-1));
		randomSplit.add(new Integer(stackDimension - 1));
		Debug.print("random split: "+ randomSplit.toString());
		return randomSplit;
	}

	/**
	 * Initialize riversList, so after that you will have a list rivers, each river is a list of CITY or DAM
	 */
	private void genRivers(){
		ArrayList<Integer> randomSplit = generateRandomSplit();
		riversList = new ArrayList<>(riverGenParam.getNumberOfRiver() - 1);
		int k = 1;
		int t = 0;
		while (t < stackDimension){
			int numberOfLoops = randomSplit.get(k) - randomSplit.get(k - 1);
			ArrayOfTypes subRiver = new ArrayOfTypes();
			for (int p = 0; p < numberOfLoops; p++){				
				subRiver.add(randomStack.get(t));
				t++;
			}
			riversList.add(subRiver);
			k++;
		}
		
		//Ordino in base alla lunghezza della lista
		SizeComparator sizeComparator = new SizeComparator();
		Collections.sort(riversList, sizeComparator);

		Debug.print("river list: " + riversList.toString());
	}

	/**
	 * 
	 * @return Return a list of {@code NodeType} one element for each node in the river's topology, 
	 * with at first positions all the cities and then dams.
	 */
	private ArrayList<NodeType> initAllType(){
		ArrayList<NodeType> allType = new ArrayList<NodeType>();
		for (int i = 0; i < riverGenParam.getNumberOfCity(); i++) {
			allType.add(NodeType.CITY);
		}
		for (int i = 0; i < riverGenParam.getNumberOfDam(); i++) {
			allType.add(NodeType.DAM);
		}

		Debug.print("ordered list: " + allType.toString());
		return allType;
	}

	/**
	 * 
	 * @param allType The list of all remaining nodes type.
	 * @return NodeType extracted for {@code allType} list, where will be deleted.
	 */
	private NodeType getRandomType(ArrayList<NodeType> allType){
		Random randomGen = new Random();
		NodeType type = null;
		//se la dimensione di all type � nulla � un ciclo infinito che evito cos�
		if (allType.size() < 1){
			return null;
		}
		while (type == null){
			if (allType.size() < 1){
				Debug.print(allType.toString());
			}
			else {				
				int typeInt = randomGen.nextInt(allType.size());
				type = allType.get(typeInt);
				allType.remove(typeInt);
				Debug.print("indice " + typeInt + ", tipo " + type, false);
				return  type;
			}
		}
		return null;
	}


	/**
	 * This method joins all rivers together  
	 */
	private void joinRivers(){		
		ArrayList<InitialFlow> updatedStartingNodes = new ArrayList<InitialFlow>(startingNodes);

		updatedStartingNodes.remove(0);

		int range = obtainRiversLength(updatedStartingNodes);

		for (int i = 0; i < (startingNodes.size() - 1); i++){

			//position deve essere > 0
			int position = Utility.randomInt(1, range);

			Debug.print("posizione di inserimento: " + position + ", del fiume con indice: " + i);

			UnionNode unionNode = new UnionNode();
			RiverNode endingRiver = obtainOutflow(startingNodes.get(i));
			endingRiver.setOutflow(unionNode);
			unionNode.addInflow(endingRiver);

			int k = 0;
			int riversIndex = i + 1;
			while (k < position){
				int delta = insertNode(unionNode, startingNodes.get(riversIndex), position - k);				
				if (delta == -1){
					break;
				}
				k = k + delta;
				riversIndex++;
			}
			updatedStartingNodes.remove(0);
			range = obtainRiversLength(updatedStartingNodes);
		}
		Debug.print("joined rivers: " + startingNodes);
	}

	/**
	 * @return Return the last node of a river
	 */	
	private RiverNode obtainOutflow(InitialFlow river){
		RiverNode out = river;
		while (out.getOutflow() != null) {
			out = out.getOutflow();
		}		
		return out;
	}

	/**
	 * @param river The river, taken as {@code InitialFlow}, to which calculate length.
	 * @return Return the length of a given river.
	 */
	private int obtainRiverLength(InitialFlow river) {
		int out = 0;
		RiverNode node = river;
		while (node.getOutflow() != null) {
			node = node.getOutflow();
			out = out + 1;
		}		
		return out;
	}

	/**
	 * 
	 * @param rivers The rivers, taken as  list of {@code InitialFlow}, to which calculate sum of length.
	 * @return Return the sum of lengths of a given rivers.
	 */
	private int obtainRiversLength(ArrayList<InitialFlow> rivers){
		int out = 0;
		for (InitialFlow initialFlow : rivers) {
			out = out + obtainRiverLength(initialFlow);
		}		
		return out;
	}

	/**
	 * For each element of {@code startingNodes}, that represent a river, calculate its length. Striclty means that doesn't
	 * consider the merge of rivers.
	 */
	//va fatta prima di unire i fiumi
	private void setRiversStrictLength(){
		for (InitialFlow river : startingNodes) {
			int strictLeght = obtainRiverLength(river);
			river.setStrictLength(strictLeght);
		}
	}

	/**
	 * Assign to each river its initial flow.
	 * @param log The log object used to report some log informations.
	 * @throws IOException
	 */
	private void initInitialflow(Log log) throws IOException{
		for (InitialFlow river : startingNodes) {
			int strictRiverLength = river.getStrictLength();
			double value = Init.getInitialFlow(riverGenParam.getFlowType(), strictRiverLength);
			river.setInitialFlow(value);			

			log.addMessage("Flow of river " + river.getVariableName() + " = " + value);
		}		
		Debug.print(startingNodes.toString());
	}


	/**
	 * Calculate the expression, both outflow and inflow, to each node in the river structure.
	 */
	private void expandExpressions(){
		//prendo un fiume arbitrario tanto se � dopo l'unione dovrebbe avere lo stesso risultato per ciascuno
		endingNode = obtainOutflow(startingNodes.get(0));
		endingNode.calculateFlow();
		Debug.print(startingNodes.toString());
	}

	/**
	 * 
	 * @param node The node to add in the river.
	 * @param river The river in which adding new node.
	 * @param position The position at which it is going to be the new node.
	 * @return Return -1 if it is inserted correctly, or the number of the nodes in the river otherwise
	 */
	//tempNode � il nodo in esame nel ciclo
	//node � il nodo da inserire, quindi � di tipo UnionNode
	//swapNode il nodo di comodo, � il successore di tempNode
	private int insertNode(RiverNode node, InitialFlow river, int position){
		int out = -1;
		RiverNode tempNode = river.getOutflow();
		int i = 0;
		boolean stopCond = false;

		while (tempNode != null && !stopCond) {
			//provo a riconsiderare anche gli unionnode
			if (i == (position - 1)){
				RiverNode swapNode = tempNode.getOutflow();

				node.setOutflow(swapNode);
				tempNode.setOutflow(node);

				if (swapNode != null){
					swapNode.setInflow(node);
				}
				node.setInflow(tempNode);
				stopCond = true;
				out = -1;
			}

			tempNode = tempNode.getOutflow();		
			i++;
		}
		if (!stopCond){
			out = i;
		}
		else {
			out = -1;
		}
		return out;
	}

	public ArrayList<RiverNode> getAllNodes(){
		ArrayList<RiverNode> out = endingNode.getChildren();
		out.add(endingNode);
		return out;
	}

	public ArrayList<RiverNode> getAgentNodes(){
		ArrayList<RiverNode> out = this.getAllNodes();
		ArrayList<RiverNode> toRemove = new ArrayList<RiverNode>();

		for (RiverNode riverNode : out) {
			if (riverNode instanceof UnionNode || riverNode instanceof InitialFlow){
				toRemove.add(riverNode);
			}			
		}
		out.removeAll(toRemove);
		return out;
	}

	public ArrayList<String> getAgents(ArrayList<RiverNode> nodes) {
		ArrayList<String> out = new ArrayList<String>();
		Collections.sort(nodes, new AgentComparator());
		for (RiverNode riverNode : nodes) {
			out.add(riverNode.getVariableName());
		}
		return out;
	}

	//ordina in base al livello all'interno dell'albero
	public HashMap<String, Integer> getVariableWeights(){
		return endingNode.getNodeLevels2(stackDimension);
	}
}
