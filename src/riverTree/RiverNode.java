package riverTree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import parameters.NodeParameters;

public abstract class RiverNode implements Serializable {
	private static final long serialVersionUID = 4234563469656231271L;

	private String variableName;
	
	//Rappresenta l'albero
	private RiverNode inflow;
	private RiverNode outflow;	

	private String inflowExpression;
	private String outflowExpression;
	
	private NodeParameters parameters;
	
	public RiverNode(){
		this.inflow = null;
		this.outflow = null;
		this.inflowExpression = null;
		this.outflowExpression = null;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public RiverNode getInflow() {
		return inflow;
	}

	public void setInflow(RiverNode inflow) {
		this.inflow = inflow;
	}

	public RiverNode getOutflow() {
		return outflow;
	}

	public void setOutflow(RiverNode outflow) {
		this.outflow = outflow;
	}

	public String getInflowExpression() {
		return inflowExpression;
	}

	public void setInflowExpression(String inflowExpression) {
		this.inflowExpression = inflowExpression;
	}

	public String getOutflowExpression() {
		return outflowExpression;
	}

	public void setOutflowExpression(String outflowExpression) {
		this.outflowExpression = outflowExpression;
	}	
	
	public String toString(){
		String inStr = null;
		if (this.inflow == null){
			inStr = "-";
		}
		else {
			inStr = inflow.getVariableName();
		}
		String outStr = null;
		if (this.outflow == null){
			outStr = "-";
		}
		else {
			outStr = outflow.getVariableName();
		}		
		
		String flow = "(in:" + inStr + ", out:" + outStr + ")";
		String out = "<" + this.getVariableName() + "," + flow + ">"; 
		return out;
	}
	
	public abstract String calculateFlow();
		
	/**
	 * @return return a list of all nodes that are children of @this, in sense of nodes that flows in @this
	 */
	public ArrayList<RiverNode> getChildren(){
		ArrayList<RiverNode> out = new ArrayList<RiverNode>();
		RiverNode inflow = this.getInflow();
		
		//Caso base		
		if (inflow == null){
			return out;
		}
		else {
			out.add(inflow);
			out.addAll(inflow.getChildren());		
			return out;
		}				
	}
		
	/**
	 * 
	 * @param level The starting value of weight, usually set equal to the number of agents
	 * @return Its returns the map of all agents with their weight, giving more importance to upstream with respect to 
	 * 			downstream nodes. So the most upstream node have lower weight, according to minimization of MST
	 *  
	 */
	public HashMap<String, Integer> getNodeLevels2(int level){
		 HashMap<String, Integer> out = new HashMap<String, Integer>();
		 
		 if (this instanceof Dam || this instanceof City){
			 out.put(this.getVariableName(), level);
			 level--;
		 }
		 RiverNode inflow = this.getInflow();
		 if (inflow == null){
				return out;
			}
		 out.putAll(inflow.getNodeLevels2(level));
		 return out;		
	}

	public NodeParameters getParameters() {
		return parameters;
	}

	public void setParameters(NodeParameters parameters) {
		this.parameters = parameters;
	}	
}