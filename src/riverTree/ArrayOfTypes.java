package riverTree;

import java.io.Serializable;
import java.util.ArrayList;

public class ArrayOfTypes implements Serializable {
	private static final long serialVersionUID = 4727951823205351918L;
	
	private ArrayList<NodeType> types;	

	public ArrayOfTypes() {
		super();
		types = new ArrayList<NodeType>();
	}
	
	public ArrayOfTypes(int dim) {
		super();
		types = new ArrayList<NodeType>(dim);
	}

	public ArrayList<NodeType> getTypes() {
		return types;
	}
	
	public int size(){
		return types.size();
	}
	
	public void add(NodeType adding){
		types.add(adding);
	}
	
	public String toString(){
		return types.toString();
	}

	public void setNode(int j, NodeType type) {
		types.set(j, type);
	}	
}
