package riverTree;

import java.util.ArrayList;
import java.util.HashMap;

public class UnionNode extends RiverNode {
	private static final long serialVersionUID = -1679768774003558163L;

	private ArrayList<RiverNode> additionalInflows;
	private ArrayList<String> additionalInflowsExpr;

	public UnionNode(){
		super();
		super.setVariableName(RiverUtility.getRndDumbNodeName());
		additionalInflows = new ArrayList<RiverNode>();
		additionalInflowsExpr = new ArrayList<String>();
	}

	public void addInflow(RiverNode riverNode){
		additionalInflows.add(riverNode);
	}

	public ArrayList<RiverNode> getAdditionalInflows() {
		return additionalInflows;
	}

	public String toString(){
		String inStr = null;
		if (super.getInflow() == null){
			inStr = "-";
		}
		else {
			inStr = super.getInflow().getVariableName();
		}
		for (RiverNode node : additionalInflows) {
			if (node == null){
				inStr =  inStr + "-";
			}
			else {
				inStr = inStr + "," + node.getVariableName();
			}
		}		
		String outStr = null;
		if (super.getOutflow() == null){
			outStr = "-";
		}
		else {
			outStr = super.getOutflow().getVariableName();
		}		
		String flow = "(in:(" + inStr + "), out:" + outStr + ")";
		String out = "<" + this.getVariableName() + "," + flow + ">"; 
		return out;
	}

	@Override
	public String calculateFlow(){
		String out = "";
		RiverNode inflow = super.getInflow();
		//Caso base		
		if (inflow == null){
			return out;
		}
		else {
			String previous = inflow.calculateFlow();
			if (previous != null){
				super.setInflowExpression(previous);
			}
			else {
				System.err.print("errore, espressione in uscita al nodo precedente nulla!");
			}			
			out =  inflow.calculateFlow();			
			for (int i = 0; i < additionalInflows.size(); i++) {
				String additional = additionalInflows.get(i).calculateFlow();
				this.additionalInflowsExpr.add(additional);				
				out = out + "+" + additional;
			}			
			super.setOutflowExpression(out);
			return out ;
		}
	}

	/**
	 * @return return a list of all nodes that are children of @this, in sense of nodes that flows in @this
	 */
	@Override
	public ArrayList<RiverNode> getChildren(){
		ArrayList<RiverNode> out = new ArrayList<RiverNode>();
		RiverNode inflow = this.getInflow();		

		//Caso base		
		if (inflow == null){
			return out;
		}
		else {
			out.add(inflow);
			out.addAll(inflow.getChildren());

			for (int i = 0; i < additionalInflows.size(); i++) {
				out.add(additionalInflows.get(i));
				ArrayList<RiverNode> additional = additionalInflows.get(i).getChildren();				
				out.addAll(additional);				
			}						
			return out;
		}			
	}

	@Override
	public HashMap<String, Integer> getNodeLevels2(int level){
		HashMap<String, Integer> out = new HashMap<String, Integer>();

		RiverNode inflow = this.getInflow();
		if (inflow == null){
			return out;
		}

		out.putAll(inflow.getNodeLevels2(level));

		for (RiverNode additional : additionalInflows) {
			out.putAll(additional.getNodeLevels2(level));
		}
		return out;
	}
}
