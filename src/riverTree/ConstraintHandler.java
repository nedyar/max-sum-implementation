package riverTree;

import java.io.IOException;
import java.util.ArrayList;

import org.jdom2.JDOMException;

import Debug.Log;

import parameters.CityNodeParameters;
import parameters.DamNodeParameters;
import parameters.Init;
import parameters.NodeParameters;
import xml.XmlRiverHandler;

public class ConstraintHandler {
	private XmlRiverHandler xmlHandler;
	ArrayList<String> agents;

	public ConstraintHandler(String path, ArrayList<String> agents) throws JDOMException, IOException {
		super();
		this.xmlHandler = new XmlRiverHandler(path);
		xmlHandler.createModel(agents.size() + 1);
		this.agents = agents;
	}

	//usata per debug, salva nell'xml la struttura del fiume
	public ConstraintHandler(String path, ArrayList<String> agents, String riverStructure, String riverNodes) throws JDOMException, IOException {
		super();
		this.xmlHandler = new XmlRiverHandler(path);
		xmlHandler.createModelFromRiver(agents.size() + 1, riverStructure, riverNodes);
		this.agents = agents;
	}

	public void generateConstraint(String variableName, String inflowExpr, String outflowExpr,
			NodeType type, NodeParameters parameters, Log log) throws IOException{

		int position = agents.indexOf(variableName) + 1;

		//Obtain domain based on parabolic function
		String domain = Init.getDomain(parameters);		

		log.addMessage("Agent " + variableName);
		log.addMessage("Type " + type.toString());
		log.addMessage("Domain : " + domain);
		log.addMessage("Parameters: " + parameters);
		log.addMessage("");

		xmlHandler.addVariable(variableName, position, domain);

		addGenericConstraint(variableName, ConstraintType.OBJECTIVE, null, null, null, parameters);

		if (type == NodeType.CITY){
			//q-x>=0 && q-x>=b  HARD + SOFT
			addGenericConstraint(variableName, ConstraintType.MIXED, outflowExpr,
					new String("" + parameters.getMinOut()), ComparisonType.GTE, parameters);

			//x>=a SOFT	
			String right = "" + ((CityNodeParameters) parameters).getMinDrain();
			addGenericConstraint(variableName, ConstraintType.SOFT, variableName, right, ComparisonType.GTE, parameters);
		}
		else if (type == NodeType.DAM){
			//vincolo x >= b SOFT
			String right = "" + parameters.getMinOut();
			addGenericConstraint(variableName, ConstraintType.SOFT, outflowExpr, right, ComparisonType.GTE, parameters);

			//vincolo x <= Q + s HARD
			String rightPart = ((DamNodeParameters) parameters).getLevel() + "+" + inflowExpr;
			addGenericConstraint(variableName, ConstraintType.HARD, outflowExpr, rightPart, ComparisonType.LTE, parameters);
		}
		xmlHandler.saveDocument();
	}

	//vale anche per mixed a patto di prendere come rigth la costante chiamata b e basta, 
	//che eventualmente varr� 0 per un caso particolare
	private void addGenericConstraint(String variableName, ConstraintType type, String leftBoolExpr, String rightBoolExpr,
			ComparisonType comparison, NodeParameters parameters){
		ArrayList<String> booleansExpr = new ArrayList<String>();
		ArrayList<String> utilitiesExpr = new ArrayList<String>();

		int position = -1;
		String name = "$$$";
		int arity = -1;
		ArrayList<String> variablesScope = new ArrayList<String>();		

		if (type == ConstraintType.OBJECTIVE){
			//Funzione obbiettivo
			booleansExpr.add("true");
			String xSquare = variableName + "*" + variableName;
			double dA = parameters.getA();
			dA = math.Utility.round3(dA);
			String firstTerm = dA + "*" + xSquare;
			String secondTerm = parameters.getB() + "*" + variableName;
			String thirdTerm = "" + parameters.getC();
			String equationExpr = firstTerm + "+" + secondTerm + "+" + thirdTerm;
			utilitiesExpr.add(equationExpr);

			name = "fo" + variableName.subSequence(1, variableName.length());

			position = agents.indexOf(variableName) + 1;
			arity = 1;
			variablesScope.add(variableName); 
		}
		else {
			//1b
			//1u
			booleansExpr.add(leftBoolExpr + comparison.toString() + rightBoolExpr);
			utilitiesExpr.add("0");

			if (type == ConstraintType.MIXED) {
				//2bM
				booleansExpr.add(leftBoolExpr + comparison.toString() + "0");
				String utility2;
				if (comparison == ComparisonType.GTE || comparison == ComparisonType.GT){				
					utility2 = leftBoolExpr + "-" + rightBoolExpr;
				}
				else {
					utility2 = rightBoolExpr + "-" + leftBoolExpr;
				}
				//2uM
				utilitiesExpr.add(utility2);

				//3uM
				utilitiesExpr.add("" + Integer.MIN_VALUE);
				//3bM
				booleansExpr.add(leftBoolExpr + comparison.opposite().toString() + "0");
			}
			else {
				//2b			
				booleansExpr.add(leftBoolExpr + comparison.opposite().toString() + rightBoolExpr);
			}			

			String utility = "$$$";

			switch (type) {
			case SOFT:
				if (comparison == ComparisonType.GTE || comparison == ComparisonType.GT){
					utility = leftBoolExpr + "-" + rightBoolExpr;
				}
				else {
					utility = rightBoolExpr + "-" + leftBoolExpr;
				}
				//2u
				utilitiesExpr.add(utility);
				break;
			case HARD:				
				//simulo -infinito
				utility = "" + Integer.MIN_VALUE;

				//2u
				utilitiesExpr.add(utility);
				break;
			default:
				break;
			}
			name = RiverUtility.getRndFunctionName();
			position = getNumbersOfAgent() + 1;

			//il pi� � arbitrario per dividere le variabili
			variablesScope = getScope(leftBoolExpr + "+" + rightBoolExpr);
			arity = variablesScope.size();		
		}
		xmlHandler.addFunction(name, position, arity, variablesScope, utilitiesExpr, booleansExpr);
	}

	private int getNumbersOfAgent(){
		return agents.size();
	}

	private ArrayList<String> getScope(String expression){
		int i = 0;
		ArrayList<String> out = new ArrayList<String>();
		while (i < expression.length()){
			char e = expression.charAt(i);
			i++;
			if(e == 'x'){
				String s = "" + expression.charAt(i);
				boolean t = true;

				while (isNumeric(s) && i < expression.length()){
					i++;
					if (i >= expression.length()){
						t = false;
					}
					else{
						s = s + expression.charAt(i);
					}
				}
				if (t){
					out.add("x" + s.substring(0, s.length()-1));
				}
				else {
					out.add("x" + s);
				}
			}
		}
		return out;
	}

	private boolean isNumeric(String str) {  
		try {  
			Integer.parseInt(str);  
		}  
		catch(NumberFormatException nfe) {  
			return false;  
		}  
		return true;  
	}
}
