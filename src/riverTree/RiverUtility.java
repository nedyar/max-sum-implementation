package riverTree;

import java.util.ArrayList;

public class RiverUtility {
	static ArrayList<String> alreadyAssigned;
	static int previousNumber;
	static ArrayList<String> alreadyAssignedFunction;
	static int previousNumberFunction;
	static ArrayList<String> dumbAlreadyAssigned;
	static int previousChar;

	public RiverUtility() {
		alreadyAssignedFunction = new ArrayList<String>();
		dumbAlreadyAssigned = new ArrayList<String>();
		previousNumberFunction = 0;
		previousChar = 'z';
	}

	/**
	 * @return a string representing next variable name, chose incrementally
	 */
	public static String getRndVariableName(){
		String out = "x1";
		int number = 1;
		while (alreadyAssigned.contains(out)){
			number = previousNumber++;
			out = new String("x" + number);
		}
		previousNumber = number;
		alreadyAssigned.add(out);
		return out;				
	}
	
	public static String getRndFunctionName(){
		String out = "f1";
		int number = 1;
		while (alreadyAssignedFunction.contains(out)){
			number = previousNumberFunction++;
			out = new String("f" + number);
		}
		previousNumberFunction = number;
		alreadyAssignedFunction.add(out);
		return out;				
	}
	
	//TODO per ora genera solo 26 lettere sensate, il trucchetto con pan non sembra funzionare
	public static String getRndDumbNodeName(){		
		String out = "A";
		char c = 'A';
		String pan = "";
		while (dumbAlreadyAssigned.contains(out)){
			if (c == 'Z'){
				pan = pan + "^";
				c = 'A'; 
			}
			else {
				c = (char) (previousChar + 1);
			}
			out = new String(pan + c);
		}
		previousChar = c;
		dumbAlreadyAssigned.add(out);
		return out;
	}
}
