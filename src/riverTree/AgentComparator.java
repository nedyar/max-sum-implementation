package riverTree;

import java.util.Comparator;

public class AgentComparator implements Comparator<RiverNode> {	
	@Override
	public int compare(RiverNode o1, RiverNode o2) {
		String o1Str = o1.getVariableName().substring(1);
		String o2Str = o2.getVariableName().substring(1);

		int int1 = Integer.parseInt(o1Str);
		int int2 = Integer.parseInt(o2Str);
		return int1 - int2;
	}
}
