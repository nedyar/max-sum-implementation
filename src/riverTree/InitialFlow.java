package riverTree;

public class InitialFlow extends RiverNode{
	private static final long serialVersionUID = 4918716813976761305L;

	private double initialFlow;	
	private int strictLength;

	public InitialFlow(){
		super();
		super.setVariableName(RiverUtility.getRndDumbNodeName());
		initialFlow = (float) 0;
		strictLength = 0;
	}

	public int getStrictLength() {
		return strictLength;
	}

	public void setStrictLength(int strictLenght) {
		this.strictLength = strictLenght;
	}

	public double getInitialFlow() {
		return initialFlow;
	}

	public void setInitialFlow(double initialFlow) {
		this.initialFlow = initialFlow;
	}

	public String toString(){
		RiverNode temp = super.getOutflow();
		if (temp == null){					
			return "" + this.initialFlow;
		}
		String out = this.initialFlow + "(" + super.getVariableName() + ")" + ":[" + temp.getVariableName();
		while (temp.getOutflow() != null) {
			temp = temp.getOutflow();
			if (temp instanceof UnionNode){
				out = out +",("+  temp.getVariableName() + ")";
			}
			else {
				out = out +","+  temp.getVariableName();
			}
		}
		out = out + "]";
		return out;
	}

	@Override
	public String calculateFlow(){
		String out = "";
		RiverNode inflow = super.getInflow();
		//Caso base		
		if (inflow == null){
			out = "" + this.getInitialFlow();
			super.setOutflowExpression(out);
			return out;
		}
		return out;
	}
}
