package riverTree;

import java.util.Comparator;

public class SizeComparator implements Comparator<ArrayOfTypes> {

    @Override
    public int compare(ArrayOfTypes o1, ArrayOfTypes o2) {
        return o1.size() - o2.size();
    }
}
