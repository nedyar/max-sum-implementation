package riverTree;

import parameters.CityNodeParameters;

public class City extends RiverNode {
	private static final long serialVersionUID = -930376017738079636L;

	public City() {
		super();
		super.setVariableName(RiverUtility.getRndVariableName());
		super.setParameters(new CityNodeParameters());
	}

	@Override
	public String calculateFlow(){
		String out = "";
		RiverNode inflow = super.getInflow();
		//Caso base		
		if (inflow == null){
			return out;
		}
		else {
			String previous = inflow.calculateFlow();
			if (previous != null){
				super.setInflowExpression(previous);
			}
			else {
				System.err.print("errore, espressione in uscita al nodo precedente nulla!");
			}
			
			out = previous + "-" + super.getVariableName();
			super.setOutflowExpression(out);
			return out ;
		}
	}
}
