package fileSystemUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import execution.SolutionType;

public class FileSystemUtils {
	public static File[] getModelsPath(File startingPath){
		return startingPath.listFiles(new FilterModelFile());
	}

	public static File[] getModelsFolder(File startingPath){
		return startingPath.listFiles(new FilterFolders());
	}

	public static void copyDirectory(File srcDir, File dstDir) throws IOException {
		if (srcDir.isDirectory()) {
			if (!dstDir.exists()) {
				dstDir.mkdir();
			}

			String[] children = srcDir.list();
			for (int i=0; i<children.length; i++) {
				copyDirectory(new File(srcDir, children[i]),
						new File(dstDir, children[i]));
			}
		}
	}

	public static SolutionType isEmpty(File file) throws IOException{
		SolutionType out = SolutionType.DEADLOCK;
		if (file.exists()){
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = reader.readLine();
			boolean t = true;
			while(line!=null && t) {
				if (line.contains("connesso")){
					out = SolutionType.UNCONNECTED;
					t = false;
				}
				if (line.contains("Pareto")){
					out = SolutionType.CORRECT;
					t = false;
				}
				line = reader.readLine();
			}
			reader.close();
		}
		return out;
	}
	
	public static boolean existBruteforce(String bruteforcePath) {
		File bf = new File(bruteforcePath);
		if (bf.exists()){
			return true;
		}
		return false;
	}
}