package fileSystemUtil;

import java.io.File;
import java.io.FilenameFilter;

public class FilterXmlFiles implements FilenameFilter {
	@Override
	public boolean accept(File arg0, String arg1) {
		if (arg1.endsWith(".xml")){
			return true;
		}
		return false;
	}
}