package fileSystemUtil;

import java.io.File;

public class FilterModelFile extends FilterXmlFiles {
	@Override
	public boolean accept(File arg0, String arg1) {
		boolean out = super.accept(arg0, arg1) && !arg1.contains("acyclic");
		return out;
	}
}