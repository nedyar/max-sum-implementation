package fileSystemUtil;

import java.io.File;
import java.io.FilenameFilter;

public class FilterOptimizationFile implements FilenameFilter {
	@Override
	public boolean accept(File arg0, String arg1) {
		return (arg1.contains("Optimization"));
	}
}
