package parameters;

import riverTree.NodeType;

public class CityNodeParameters extends NodeParameters {
	private static final long serialVersionUID = -7244974755960047088L;
	
	//fabbisogno minimo di acqua da prelevare
	private double minDrain;	
	
	public CityNodeParameters(double a, double b, double c, double minOut, double minDrain) {
		super(a, b, c, minOut);
		this.minDrain = minDrain;
	}		

	public CityNodeParameters() {
		this(Init.getA(NodeType.CITY), Init.getB(NodeType.CITY), Init.getC(NodeType.CITY),
				Init.getMinOut(), Init.getMinDrain());
	}

	public double getMinDrain() {
		return minDrain;
	}

	public void setMinDrain(double minDrain) {
		this.minDrain = minDrain;
	}
	
	public String toString(){
		return super.toString() + ", minDrain= " + minDrain;		
	}	
}
