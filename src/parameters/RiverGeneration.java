package parameters;

import java.io.Serializable;

public class RiverGeneration implements Serializable{
	private static final long serialVersionUID = 738428085646077429L;
	
	private int numberOfRiver;
	private int numberOfCity;
	private int numberOfDam;
	private FlowType flowType;
	
	public RiverGeneration(int numberOfRiver, int numberOfCity, int numberOfDam, FlowType flowType) {
		super();
		this.numberOfRiver = numberOfRiver;
		this.numberOfCity = numberOfCity;
		this.numberOfDam = numberOfDam;
		this.flowType = flowType;
	}		

	public FlowType getFlowType() {
		return flowType;
	}
	
	public void setFlowType(FlowType flowType) {
		this.flowType = flowType;
	}

	public int getNumberOfRiver() {
		return numberOfRiver;
	}

	public void setNumberOfRiver(int numberOfRiver) {
		this.numberOfRiver = numberOfRiver;
	}

	public int getNumberOfCity() {
		return numberOfCity;
	}

	public void setNumberOfCity(int numberOfCity) {
		this.numberOfCity = numberOfCity;
	}

	public int getNumberOfDam() {
		return numberOfDam;
	}

	public void setNumberOfDam(int numberOfDam) {
		this.numberOfDam = numberOfDam;
	}	
}
