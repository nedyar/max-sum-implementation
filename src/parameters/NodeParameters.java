package parameters;

import java.io.Serializable;

public class NodeParameters implements Serializable{
	private static final long serialVersionUID = 2947365733427087070L;
	
	//Parametri della funzione obbiettivo
	private double a;
	private double b;
	private double c;	
	
	//portata di acqua minima in uscita da un nodo
	private double minOut;			

	public NodeParameters(double a, double b, double c, double minOut) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		this.minOut = minOut;
	}

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}

	public double getMinOut() {
		return minOut;
	}

	public void setMinOut(double minOut) {
		this.minOut = minOut;
	}	
	
	public String toString(){
		return "a= " + a + ", b= " + b + ", c= " + c + ", minOut= " + minOut;		
	}	
}
