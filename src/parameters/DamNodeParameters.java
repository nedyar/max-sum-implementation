package parameters;

import riverTree.NodeType;

public class DamNodeParameters extends NodeParameters {
	private static final long serialVersionUID = 8160941025964476706L;
	
	//riserva della diga
	private double level;	
	
	public DamNodeParameters(double a, double b, double c, double minOut, double level) {
		super(a, b, c, minOut);
		this.level = level;
	}
	
	public DamNodeParameters(FlowType flowType){		
		
		this(Init.getA(NodeType.DAM), Init.getB(NodeType.DAM), Init.getC(NodeType.DAM),
				Init.getMinOut(), Init.getLevel(flowType));
	}

	public double getLevel() {
		return level;
	}

	public void setLevel(double level) {
		this.level = level;
	}
	
	public String toString(){
		return super.toString() + ", level= " + level;		
	}	
}
