package parameters;

public enum FlowType {
	HIGH, MEDIUM, LOW;

	@Override
	public String toString(){
		String out = "";
		switch (this) {
		case HIGH:
			out = "high";
			break;
		case MEDIUM:
			out = "medium";
			break;
		case LOW:
			out = "low";
			break;
		default:
			out = "error";
			break;
		}
		return out;
	}
}
