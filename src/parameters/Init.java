package parameters;

import Debug.Debug;
import riverTree.NodeType;
import xml.XMLParamHandler;
import math.Utility;

public class Init {	

	private static XMLParamHandler xmlParamHandler;
	
	public Init(String paramFileStr){
		Init.xmlParamHandler = new XMLParamHandler(paramFileStr);
	}	
	
	public static double getLevel(FlowType flowType){
		return Init.xmlParamHandler.getLevel(flowType);
	}
	
	//valore portata iniziale
	public static double getInitialFlow(FlowType flowType, int agents){
		double value = Init.xmlParamHandler.getInitialFlow(flowType, agents);
		double out = value * agents;
		return out;
	}
	
	//valori funzione obbiettivo
	public static double getA(NodeType nodeType){
		double out = Init.xmlParamHandler.getA(nodeType);
		//� inutile fare il round a 3 cifre, tanto nel creare il float commette questo errore,
		//la soluzione sarebbe usare double per tutto il progetto!
		out = new Float(Utility.round3(out));
		return out;
	}
	
	public static double getB(NodeType nodeType){
		double out = Init.xmlParamHandler.getB(nodeType);
		return out;
	}
	
	public static double getC(NodeType nodeType){
		double out = Init.xmlParamHandler.getC(nodeType);
		return out;
	}
	
	//valore portata minima necessaria ai nodi citt�
	public static double getMinDrain(){
		return Init.xmlParamHandler.getMinDrain();
	}
	
	//valore portata minima in uscita da un nodo
	public static double getMinOut(){
		return Init.xmlParamHandler.getMinOut();
	}

	public static String getDomain(NodeParameters parameters){
		int domainSize = Init.xmlParamHandler.getDomainSize();
	    
		double a = parameters.getA();
		double b = parameters.getB();
		double c = parameters.getC();
		double d;
	    double s1 = -1.0;
	    		double s2 = 1.0;

	    d=b*b-4*a*c;

	    if( d==0 ) {
	      s1=(-b - Math.sqrt( b*b -4*a*c ))/(2*a);
	      s2=s1;
	      Debug.print("Soluzioni coincidenti: " + s1);
	    }

	    if( d>0 ) {
	      s1=(-b - Math.sqrt( b*b -4*a*c ))/(2*a);
	      s2=(-b + Math.sqrt( b*b -4*a*c ))/(2*a);
	    }

	    if( d<0 ) {
	      Debug.print("Soluzioni complesse");
	    }	    
	    
	    if (s2 < s1){
	    	double temp = s1;
	    	s1 = s2;
	    	s2 = temp;
	    }
	    //evito di avere domini negativi
	    if (s1 < 0){
	    	s1 = 0.0;
	    }
	    if (s2 < 0){
	    	s2 = 10.0;
	    	Debug.print("parametri parabola errati");
	    }	    
	    
	    double length = s2 - s1;
	    
	    //Per ottenere sempre valori del dominio distinti e interi devo per forza fare cos�
	    if (length < domainSize){
	    	length = domainSize;
	    }
	    
	    domainSize--;
	    double deltaDomain = length / domainSize;
	    
	    //il primo sempre per difetto
	    double i = Math.floor(s1);
	    String out = "" + i;

	    for (int k = 2; k < domainSize + 1; k++) {
	    	i = math.Utility.round3(k * deltaDomain);
			out = out + "," + i;
		}      
	    
	    out = out + "," + Math.ceil(s2) * 1.05;
	    return out;
	}	
}
