package algorithm;
import java.util.ArrayList;

public class Marginal {	
	private String variable_name;
	private ArrayList<Float> variable_values;
	private ArrayList<ArrayList<ArrayList<Float>>> marginal_values;
	private int timestamp;
	
	@SuppressWarnings("unchecked")
	public Marginal(String variable_name, ArrayList<RMessage> rlist, int timestamp){
		this.variable_name=variable_name;
		ArrayList<RMessage> cur_rmessages = extractPresentRMessages(rlist, timestamp);
		this.timestamp=timestamp;
		this.variable_values=cur_rmessages.get(0).getVariableValues();
		this.marginal_values=new ArrayList<ArrayList<ArrayList<Float>>>();
		ArrayList<ArrayList<Float>> m_line;
		ArrayList<ArrayList<Float>> r_line;
		ArrayList<ArrayList<Float>> temp;
		for(int i=0;i<variable_values.size();i++){
			m_line = (ArrayList<ArrayList<Float>>) rlist.get(0).getRValues().get(i).clone();
			for(int j=1;j<rlist.size();j++){
				temp = new ArrayList<ArrayList<Float>>();
				r_line = (ArrayList<ArrayList<Float>>) rlist.get(j).getRValues().get(i).clone();
				//sommo q_line a rline e metto il risultato in temp
				for(ArrayList<Float> ql:m_line)
					for(ArrayList<Float> rl:r_line)
						temp.add(sumVectors(ql,rl));
				m_line = (ArrayList<ArrayList<Float>>) temp.clone();
			}
			this.marginal_values.add(m_line);
		}
	}
	
	private ArrayList<RMessage> extractPresentRMessages(ArrayList<RMessage> rmessages, int timestamp){
		ArrayList<RMessage> cur_rmessages = new ArrayList<RMessage>();
		for(RMessage r:rmessages){
			if(r.getTimeStamp()==timestamp)
				cur_rmessages.add(r);
		}
		return cur_rmessages;
	}
	
	private ArrayList<Float> sumVectors(ArrayList<Float> a1, ArrayList<Float> a2){
		ArrayList<Float> sum = new ArrayList<Float>();
		for(int i=0;i<a1.size();i++)
			sum.add(a1.get(i)+a2.get(i));
		return sum;
	}
	
	public int getTimeStamp(){
		return this.timestamp;
	}
	
	public ArrayList<Float> getVariableValues(){
		return this.variable_values;
	}
	
	public ArrayList<ArrayList<ArrayList<Float>>> getMarginalValues(){
		return this.marginal_values;	
	}
	
	public String getVariableName(){
		return this.variable_name;
	}
}
