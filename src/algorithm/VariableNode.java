package algorithm;

import Debug.Debug;
//import Debug.Messages;

public class VariableNode implements Runnable{

	private Variable variable;
	private int max_iterations;
	private int last_iteration;

	public VariableNode(Variable v, int max_iterations){
		this.variable=v;
		this.max_iterations=max_iterations;
		new Thread(this, variable.getName()).start();
	}

	@Override
	public void run(){		
		//Prima iterazione in cui tutti i messaggi q contengono 0
		for(Function f:variable.getNeighbours()){
			variable.sendZeroQMessage(f, 0);
		}
		variable.computeMarginalFunction(0);

		//Ciclo esterno che rappresenta ogni iterazione di max-sum
		for(int i=1;i<max_iterations;i++){
			//System.out.println(i);
			Debug.print("Variable " + variable.getName() + ", iteration: " + i, false);

			for(Function f:variable.getNeighbours()){
				variable.QReady(f,i);

				variable.sendQMessage(f, i/*, messages*/);
			}
			variable.computeMarginalFunction(i);
			if(variable.checkInvariantMarginals(i)){
				last_iteration=i;
				break;
			}
		}

		for(Function f:variable.getNeighbours())
			f.deactivateNeighbour(this.variable);
		variable.addLastMarginalToSolution(last_iteration);

		//Visualizzo l'ultima marginale		
		/*int s=0;
		if(this.variable.getName().equals("x8")){
		System.out.println("Marginale di " + variable.getName() + " t = " + last_iteration);
		for(int k=0;k<variable.getMarginalFunctionOfIteration(last_iteration).getVariableValues().size();k++){
			System.out.print(variable.getName() + "=" + variable.getMarginalFunctionOfIteration(last_iteration-1).getVariableValues().get(k));
			System.out.println(" | " + variable.getMarginalFunctionOfIteration(last_iteration-1).getMarginalValues().get(k));
			s+=variable.getMarginalFunctionOfIteration(last_iteration-1).getMarginalValues().get(k).size();
		}
		System.out.println("s = " + s);
		}*/
	}

	public Variable getVariable() {
		return this.variable;
	}
}
