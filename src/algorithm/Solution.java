package algorithm;

import java.util.ArrayList;

public class Solution {
	private ArrayList<Variable> variables;
	private ArrayList<Marginal> marginals;
	private ParetoFrontier frontier;
	
	public Solution(ArrayList<Variable> variables){
		this.variables=variables;
		this.marginals=new ArrayList<Marginal>();
	}
	
	public ArrayList<Marginal> getMarginals(){
		return this.marginals;
	}
	
	public ArrayList<Variable> getVariables() {
		return variables;
	}
	
	public ParetoFrontier getParetoFrontier(){
		return this.frontier;
	}
	
	public synchronized void addMarginal(Marginal m){
		this.marginals.add(m);
		notify();
	}

	public synchronized void checkAllMarginalsReady(){
		while(this.marginals.size()!=this.variables.size()){
			try{
				wait();
			}catch (InterruptedException e){
				e.printStackTrace();
			}
		}
	}
	
	public void checkMarginalsOrder(){
		
		for(int i=0;i<variables.size();i++){
			if(!variables.get(i).getName().equals(marginals.get(i).getVariableName()))
				for(int j=i+1;j<variables.size();j++)
					if(variables.get(i).getName().equals(marginals.get(j).getVariableName()))
						swapMarginals(i,j);
		}
	}
	
	private void swapMarginals(int i, int j){
		Marginal temp = marginals.get(i);
		marginals.set(i, marginals.get(j));
		marginals.set(j, temp);
	}
	
	public void processParetoFrontier(){
		this.frontier = new ParetoFrontier();
		
		//ciclo esterno cicla su tutti i vettori contenuti nella prima marginale		
		ArrayList<ArrayList<Float>> vectors = extractVectorsFromMarginal(marginals.get(0));
		
		for(int i=0;i<vectors.size();i++){
			//ciclo interno cerca ogni vettore contenuto in vectors in tutte le altre marginali
			boolean found=true;
			for(int j=1;j<marginals.size();j++){
				if(!findVector(marginals.get(j),vectors.get(i))){
					found=false;
					break;
				}
			}
			if(!found){
				vectors.remove(i);
				i--;
			}
		}
		
		for(ArrayList<Float> v:vectors){
			SimpleSolution s=buildSimpleSolution(v);
			frontier.addSolution(s);
		}
			
	}
	
	private ArrayList<ArrayList<Float>> extractVectorsFromMarginal(Marginal m){
		ArrayList<ArrayList<Float>> vectors = new ArrayList<ArrayList<Float>>();
		for(ArrayList<ArrayList<Float>> v_line:m.getMarginalValues())
			for(ArrayList<Float> v:v_line)
				vectors.add(v);
		return vectors;
	}
	
	private boolean findVector(Marginal m, ArrayList<Float> vector){
		ArrayList<ArrayList<Float>> vectors = extractVectorsFromMarginal(m);
		if(vectors.contains(vector))
			return true;
		return false;
	}
	
	private SimpleSolution buildSimpleSolution(ArrayList<Float> solution_vector){
		ArrayList<Float> variables_tuple=new ArrayList<Float>();
		for(Marginal m:marginals){
			variables_tuple.add(getVariableValueFromVector(m, solution_vector));
		}
		SimpleSolution sol=new SimpleSolution(variables_tuple,solution_vector);
		return sol;
	}
	
	private float getVariableValueFromVector(Marginal m, ArrayList<Float> solution_vector){
		for(int i=0;i<m.getVariableValues().size();i++)
			if(m.getMarginalValues().get(i).contains(solution_vector))
				return m.getVariableValues().get(i);
		return 0;
	}	
}
