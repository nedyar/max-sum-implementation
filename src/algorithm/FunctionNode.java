package algorithm;

public class FunctionNode implements Runnable{
	private Function function;
	
	public Function getFunction() {
		return function;
	}	

	private int max_iterations;
	private Thread thread;		
	
	public FunctionNode(Function f, int max_iterations){
		this.function=f;
		this.max_iterations=max_iterations;
		this.thread = new Thread(this, function.getName());
		this.thread.setPriority(thread.getPriority() - 1);
		this.thread.start();
	}
	
	@Override
	public void run() {
		//Ciclo esterno che rappresenta ogni iterazione di max-sum
		for(int i=0;i<max_iterations;i++){
			if(!function.checkAllNeighboursInactive()){
				break;
			}
			
			for(Variable v:function.getNeighbours()){
				//System.out.println("THREAD " + Thread.currentThread().getName() + "| controllo se posso inviare messaggio r" + this.function.getName() + "->" + v.getName() + "; t = " + i);
				
				function.RReady(v,i);				
				//System.out.println("THREAD " + Thread.currentThread().getName() + "| invio messaggio r" + this.function.getName() + "->" + v.getName() + "; t = " + i);
								
				function.sendRMessage(v, i/*, messages, this.thread*/);				
			}
		}
//		this.log.addMessage("Messaggi scambiati da " +  this.function.getName() + " : " + rMessages);
		
		//Visualizzazione messaggi q ricevuti
		/*System.out.println(function.getName());
		for(int l=0;l<function.getQMessages().size();l++){
			System.out.println("MESSAGGIO q" + function.getQMessages().get(l).getSender().getName() + "->" + function.getQMessages().get(l).getReceiver().getName() + " t = " + function.getQMessages().get(l).getTimeStamp());
			for(int m=0;m<function.getQMessages().get(l).getVariableValues().size();m++){
				System.out.print(function.getQMessages().get(l).getVariableValues().get(m) + " ");
				System.out.println(function.getQMessages().get(l).getQValues().get(m));						
			}
		}*/			
	}
}
