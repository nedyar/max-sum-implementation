package algorithm;

import java.util.ArrayList;

public class QMessage {
	private Variable sender;
	private Function receiver;
	private int timestamp;
	private ArrayList<Float> variable_values;
	private ArrayList<ArrayList<ArrayList<Float>>> q_values;
	
	public QMessage(Variable sender, Function receiver, int timestamp, ArrayList<Float> variable_values, ArrayList<ArrayList<ArrayList<Float>>> q_values){
		this.sender=sender;
		this.receiver=receiver;
		this.timestamp=timestamp;
		this.variable_values=variable_values;
		this.q_values=q_values;
	}
	
	public Variable getSender(){
		return sender;
	}
	
	public Function getReceiver(){
		return receiver;
	}
	
	public int getTimeStamp(){
		return timestamp;
	}
	
	public ArrayList<Float> getVariableValues(){
		return variable_values;
	}

	public ArrayList<ArrayList<ArrayList<Float>>> getQValues(){
		return q_values;
	}
	
	public String toString(){
		return "type q: " + this.sender + " to " + this.receiver + ", t = " + timestamp;// + ", values: " + this.q_values.get(timestamp);
	}
}
