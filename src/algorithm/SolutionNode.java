package algorithm;

import graph.MSTweightCriterion;

import java.io.IOException;

import xml.XmlOptimizationHandler;

public class SolutionNode implements Runnable{

	private Solution solution;
	private long start;
	private XmlOptimizationHandler log;
	private MSTweightCriterion mstCriterion;

	private Thread thread;
	
	 //MSTweightCriterion � usato per salvare la frontiera nell'elemento corretto del file xml 
	public SolutionNode(Solution solution, long start, XmlOptimizationHandler log, MSTweightCriterion mstCriterion){
		this.solution=solution;
		this.start=start;
		this.log = log;
		this.mstCriterion = mstCriterion;
		thread = new Thread(this, "SolutionNode");
		thread.start();
	}

	@Override
	public void run(){
		//Verifico che siano state computate tutte le marginali
		solution.checkAllMarginalsReady();
		//L'ordine di computazione delle marginali � casuale
		//Controllo l'ordine delle marginali nell'array e se serve riordino l'array
		solution.checkMarginalsOrder();
		solution.processParetoFrontier();

		double elapsedTimeInSec = (System.nanoTime() - start) * 1.0e-9;		
		System.out.println("tempo di esecusione in secondi " + elapsedTimeInSec);
		log.addOptimizationExecutionTimeInSec(elapsedTimeInSec, mstCriterion);

		try {
			solution.getParetoFrontier().printFrontier(log, mstCriterion);			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Thread getThread(){
		return thread;
	}
}
