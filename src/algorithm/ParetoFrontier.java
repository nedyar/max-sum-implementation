package algorithm;
import graph.MSTweightCriterion;

import java.io.IOException;
import java.util.ArrayList;

import xml.XmlOptimizationHandler;

public class ParetoFrontier {
	private ArrayList<SimpleSolution> solutions;	
	
	public ParetoFrontier(){
		this.solutions=new 	ArrayList<SimpleSolution>();
	}
	
	public ArrayList<SimpleSolution> getSolutions(){
		return this.solutions;
	}
	
	public void addSolution(SimpleSolution solution){
		this.solutions.add(solution);
	}	
	
	public void printFrontier(XmlOptimizationHandler log, MSTweightCriterion mstCriterion) throws IOException{
		ArrayList<String> messages = new ArrayList<String>();
		String message = "Frontiera di Pareto composta da " + solutions.size() + " soluzioni:";
		System.out.println(message);
		messages.add(message);
		ArrayList<ArrayList<Float>> solutionVetcors = new ArrayList<ArrayList<Float>>();
		ArrayList<ArrayList<Float>> variableTuples = new ArrayList<ArrayList<Float>>();
		
		for(SimpleSolution s:this.solutions){
			String solutionStr = "x = " + s.getVariablesTuple() + ", s = " + s.getSolutionVector(); 
			solutionVetcors.add(s.getSolutionVector());
			variableTuples.add(s.getVariablesTuple());
			messages.add(solutionStr);
		}
		log.addPareto(solutionVetcors, variableTuples, mstCriterion);
	}
}
