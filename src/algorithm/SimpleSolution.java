package algorithm;
import java.util.ArrayList;

public class SimpleSolution {
	private ArrayList<Float> variables_tuple;
	private ArrayList<Float> solution_vector;
	
	public SimpleSolution(ArrayList<Float> variables_tuple, ArrayList<Float> solution_vector){
		this.variables_tuple=variables_tuple;
		this.solution_vector=solution_vector;
	}
	
	public ArrayList<Float> getVariablesTuple() {
		return variables_tuple;
	}

	public ArrayList<Float> getSolutionVector() {
		return solution_vector;
	}
}
