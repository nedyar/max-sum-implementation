package algorithm;
import java.util.ArrayList;


/*
 * TODO: dovrebbe contenere un insieme non un arraylist
 */
public class Domain {
	private ArrayList<Float> values;	
	
	public Float getElement(int index){
		return values.get(index);
	}
	
	public Domain(){
		this.values=new ArrayList<Float>();
	}
	
	public void addValueToDomain(Float value){
		this.values.add(value);
	}
	
	public ArrayList<Float> getValues(){
		return values;		
	}
	

	public String toString(){
		return this.values.toString();
	}

	//restituisce il prossimo valore del dominio, oppure null se tempval � l'ultimo
	public Float nextDomainValue(Float tempVal) {
		int size = values.size();
		int index = values.indexOf(tempVal);
		
		index = index + 1;
		
		if (index < size){
			return values.get(index);
		}
		else {
			return null;
		}		
	}
}
