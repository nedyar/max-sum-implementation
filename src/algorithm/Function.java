package algorithm;
import java.util.ArrayList;
import java.util.Vector;

import Debug.Debug;

public class Function {
	private String name;
	private int objectives;
	private int arity;
	private int hardthreshold;

	private int position;
	//L'attributo position determina la colonna dell'obiettivo in cui v� collocato il contributo di questa funzione
	//Due funzioni diverse con la stessa position sommano automaticamente i contributi (nel meccanismo dei messaggi r)

	private ArrayList<Variable> neighbours;
	//I neighbours corrispondono alle variabili dello scope della funzione
	// A ciascuna di queste variabili corrisponde una posizione delle tuple dell'array seguente 

	private ArrayList<Boolean> active_neighbours;

	private ArrayList<ArrayList<Float>> variable_tuples;
	//Contiene la tabella delle tuple delle variabili dello scope

	private ArrayList<Float> function_values;
	//Contiene i valori delle funzioni calcolati con i valori presi dalle tuple corrispondenti

	private Vector<QMessage> qmessages;

	public Function(String name, int position, int objectives, int arity){
		this.name=name;
		this.position=position;
		this.objectives=objectives;
		this.arity=arity;
		this.neighbours=new ArrayList<Variable>();
		this.variable_tuples=new ArrayList<ArrayList<Float>>();
		this.function_values=new ArrayList<Float>();		

		this.qmessages= new Vector<QMessage>();

		this.active_neighbours=new ArrayList<Boolean>();
	}

	public void addNeighbour(Variable var){
		neighbours.add(var);
	}

	public ArrayList<Variable> getNeighbours(){
		return this.neighbours;
	}

	public int getObjectivesCount(){
		return this.objectives;
	}

	public String getName(){
		return this.name;
	}

	public void addVariableTuple(ArrayList<Float> values){
		this.variable_tuples.add(values);
	}

	public ArrayList<ArrayList<Float>> getVariableTuples(){
		return this.variable_tuples;
	}

	public void addFunctionValue(Float value){
		this.function_values.add(value);
	}

	public ArrayList<Float> getFunctionValues(){
		return this.function_values;
	}

	public int getPosition(){
		return this.position;
	}

	public int getArity(){
		return this.arity;
	}

	public Vector<QMessage> getQMessages(){
		return this.qmessages;
	}

	public void setHardThreshold(int threshold){
		this.hardthreshold=threshold;
	}

	public void activateNeighbours(){
		for(int i=0;i<neighbours.size();i++)
			active_neighbours.add(true);
	}

	public void deactivateNeighbour(Variable v){
		active_neighbours.set(neighbours.indexOf(v), false);
	}

	@SuppressWarnings("unchecked")
	public synchronized void sendRMessage(Variable receiver, int timestamp/*, Messages messages, Thread thread*/){
		RMessage r;
		ArrayList<ArrayList<ArrayList<Float>>> r_values = new ArrayList<ArrayList<ArrayList<Float>>>();
		ArrayList<ArrayList<Float>> r_line;
		//Recupero i messaggi q che mi servono per calcolare r
		ArrayList<QMessage> qlist = getQMessageListInIteration(receiver, timestamp);			
		if(timestamp>0 && arity==1){
			RMessage r1 = receiver.getRMessageFromFunctionAtIteration(this,timestamp-1);
			r = new RMessage(this, receiver, timestamp, r1.getVariableValues(), r1.getRValues());

			neighbours.get(neighbours.indexOf(receiver)).receiveRMessage(r);
		}
		else{
			//Ciclo esterno: devo computare una riga per ogni valore del dominio della variabile receiver
			for(int i=0;i<receiver.getDomain().getValues().size();i++){
				float x = receiver.getDomain().getValues().get(i);
				//Devo trovare la massima somma tra il valore di f(con receiver=x) e q
				r_line = new ArrayList<ArrayList<Float>>();
				for(int j=0;j<variable_tuples.size();j++){
					ArrayList<ArrayList<Float>> q_line = new ArrayList<ArrayList<Float>>();
					//neighbours.indexOf(receiver) corrisponde alla colonna della variabile receiver
					if(variable_tuples.get(j).get(neighbours.indexOf(receiver)).equals(x)){
						//tupla con x_receiver=x_i					
						float f = function_values.get(j);
						if(arity == 1){
							//Elaborazione del messaggio r di una funzione unaria, non devo sommare alcun messaggio q
							ArrayList<Float> temp = new ArrayList<Float>();
							for(int g=0;g<objectives;g++)
								temp.add((float) 0);
							temp.set(position-1, f);
							if(temp.get(objectives-1)>hardthreshold)
								r_line.add(temp);
						}
						else{
							//r_line viene inizializzato con il valore della funzione fvector  e man mano riaggiornato con le somme dei q
							ArrayList<ArrayList<Float>> r_l = new ArrayList<ArrayList<Float>>();
							ArrayList<Float> fvector = new ArrayList<Float>();
							for(int g=0;g<objectives;g++)
								fvector.add((float) 0);
							fvector.set(position-1, f);
							r_l.add(fvector);
							for(int k=0;k<arity;k++){
								if(k != neighbours.indexOf(receiver)){
									//k corrisponde a una variabile che non � il receiver
									//prendo il messaggio q che � arrivato dalla variabile k
									QMessage q = getQMessageFromVariable(qlist, neighbours.get(k));

									//cerco nel messaggio q la riga corrispondente alla variabile del ciclo in corso
									for(int m=0;m<q.getVariableValues().size();m++){
										float qm = q.getVariableValues().get(m);
										float qt = variable_tuples.get(j).get(k);
										if(qm==qt){										
											//metto in q_line i vettori contenuti nella riga corretta del messaggio q
											q_line = q.getQValues().get(m);


											Debug.print(q.getSender().getName() + " " + q_line, false);
											/*if(timestamp==1 && this.name.equals("f4") && receiver.getName().equals("x1")){
											System.out.println(q.getSender().getName() + " " + q_line);
										}*/

											//sommo ogni vettore di q_line alla somma parziale precedente
											ArrayList<ArrayList<Float>> templine = new ArrayList<ArrayList<Float>>();
											for(ArrayList<Float> vr:r_l)
												for(ArrayList<Float> vq:q_line)
													templine.add(sumVectors(vr,vq));
											r_l=(ArrayList<ArrayList<Float>>) templine.clone();

											Debug.print("r_l: " + r_l, false);
											/*if(timestamp==1 && this.name.equals("f4") && receiver.getName().equals("x1")){
											System.out.println("      " + r_line);
										}*/
										}
									}
								}
							}
							for(ArrayList<Float> ar:r_l)
								if(ar.get(objectives-1)>hardthreshold)
									r_line.add(ar);
						}//Fine del ramo else per funzioni non unarie
					}//Fine if match valore dominio con valore tupla
				}//Fine ciclo j
				if(r_line.size()>1)
					removeDominatedVectors(r_line);
				r_values.add(r_line);
			}//Fine ciclo i

			r = new RMessage(this, receiver, timestamp, receiver.getDomain().getValues(), r_values);
			neighbours.get(neighbours.indexOf(receiver)).receiveRMessage(r);
		}	

		//		Debug.print("Messaggio r" + this.name + "->" + receiver.getName() + " t=" + timestamp, true);
		/*if(timestamp==1 && this.name.equals("f7") && receiver.getName().equals("x2")){
		System.out.println("Messaggio r" + this.name + "->" + receiver.getName() + " t=" + timestamp);
		for(int i=0;i<receiver.getDomain().getValues().size();i++){
			System.out.println(receiver.getDomain().getValues().get(i) + " | " + r_values.get(i));
		}
		}*/
	}

	public synchronized void receiveQMessage(QMessage q){		
		this.qmessages.add(q);		
		//		Debug.print("THREAD " + Thread.currentThread().getName() + 
		//				"| notifica ricezione messaggio q" + q.getSender().getName() + 
		//				"->" + q.getReceiver().getName() + "; t = " + q.getTimeStamp(), false);		
		notify();	

		notifyAll();		
		//System.out.println("THREAD " + Thread.currentThread().getName() + "| notifica ricezione messaggio q" + q.getSender().getName() + "->" + q.getReceiver().getName() + "; t = " + q.getTimeStamp());
	}


	@SuppressWarnings("unchecked")
	public synchronized void RReady(Variable receiver, int timestamp){
		//Controllo di aver ricevuto tutti i messaggi q con timestamp precedente da tutti i nodi variabile vicini meno il receiver
		ArrayList<Variable> av = (ArrayList<Variable>)neighbours.clone();
		if(av.size() > 1)
			av.remove(receiver);

		boolean rready = false;
		while(!rready){
			for(Variable v:av){				
				while(!checkQMessageReceived(v,timestamp)){
					if(!active_neighbours.get(neighbours.indexOf(v))){
						copyLastQMessage(v,timestamp);
					}
					else{
						try{
							//System.out.println("THREAD " + Thread.currentThread().getName() + "| aspetto il messaggio q" + v.getName() + this.getName() + "; t = " + tt);

							//							
							//							Debug.print("THREAD " + Thread.currentThread().getName() + 
							//									"| aspetto il messaggio q" + v.getName() + this.getName() + " t = " +
							//									timestamp, true);

							//Stefano: FIXME non capisco perch� c'� wait(1), come fa a sapere in anticipo
							//quanto impiega il messaggio effettivamente ad arrivare?
							wait(1);
						}
						catch(InterruptedException e){
							e.printStackTrace();
						}
					}
				}
				rready = true;
			}
		}
	}

	private synchronized boolean checkQMessageReceived(Variable v, int timestamp){
		for(QMessage q:qmessages){
			if(q.getSender().equals(v) && q.getTimeStamp()==timestamp)
				return true;
		}
		return false;
	}

	private ArrayList<QMessage> getQMessageListInIteration(Variable receiver, int timestamp){
		ArrayList<QMessage> qlist = new ArrayList<QMessage>();
		for(QMessage q:qmessages)
			if(q.getTimeStamp() == timestamp && !q.getSender().equals(receiver))
				qlist.add(new QMessage(q.getSender(),q.getReceiver(),q.getTimeStamp(),q.getVariableValues(),q.getQValues()));
		return qlist;
	}

	private QMessage getQMessageFromVariable(ArrayList<QMessage> qms, Variable v){
		QMessage qm = null;
		for(QMessage q:qms)
			if(q.getSender().equals(v))
				qm = new QMessage(q.getSender(),q.getReceiver(),q.getTimeStamp(),q.getVariableValues(),q.getQValues());
		return qm;
	}


	private QMessage getQMessageFromVariableInIteration(Vector<QMessage> qms, Variable v, int timestamp){
		QMessage qm = null;
		for(QMessage q:qms)
			if(q.getSender().equals(v) && q.getTimeStamp()==timestamp)
				qm = new QMessage(q.getSender(),q.getReceiver(),q.getTimeStamp(),q.getVariableValues(),q.getQValues());
		return qm;
	}

	private ArrayList<ArrayList<Float>> removeDominatedVectors(ArrayList<ArrayList<Float>> r_line){
		for(int i=0;i<r_line.size()-1;i++)
			for(int j=i+1;j<r_line.size();j++)
				if(dominates(r_line.get(i), r_line.get(j))){
					r_line.remove(j);
					j--;
				}
				else if(dominates(r_line.get(j), r_line.get(i))){
					r_line.remove(i);
					i--;
					break;
				}
		return r_line;
	}

	private boolean dominates(ArrayList<Float> v1, ArrayList<Float> v2){
		//Se v1 domina v2 restituisce true
		boolean dominated = true;
		for(int i=0;i<objectives;i++)
			if(v1.get(i)<v2.get(i))
				dominated = false;
		return dominated;
	}

	private synchronized void copyLastQMessage(Variable v, int timestamp){
		QMessage qm = getQMessageFromVariableInIteration(qmessages, v, timestamp-1);		
		this.qmessages.add(new QMessage(v,this,timestamp,qm.getVariableValues(),qm.getQValues()));
	}

	public boolean checkAllNeighboursInactive(){
		boolean res=false;
		for(boolean active:active_neighbours)
			res=res || active;
		return res;
	}

	@SuppressWarnings("unchecked")
	private ArrayList<Float> sumVectors(ArrayList<Float> v1, ArrayList<Float> v2){
		ArrayList<Float> vec1 = (ArrayList<Float>) v1.clone();
		ArrayList<Float> vec2 = (ArrayList<Float>) v2.clone();
		if(vec1.size()==vec2.size())
			for(int i=0;i<vec1.size();i++)
				vec1.set(i, vec1.get(i)+vec2.get(i));
		return vec1;
	}

	/**
	 * @author Stefano
	 */
	public String toString(){
		return this.name;
	}
}
