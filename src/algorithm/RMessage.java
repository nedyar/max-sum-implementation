package algorithm;

import java.util.ArrayList;

public class RMessage {
	private Function sender;
	private Variable receiver;
	private int timestamp;
	private ArrayList<Float> variable_values;
	private ArrayList<ArrayList<ArrayList<Float>>> r_values;
	
	public RMessage(Function sender, Variable receiver, int timestamp, ArrayList<Float> variable_values, ArrayList<ArrayList<ArrayList<Float>>> r_values){
		this.sender=sender;
		this.receiver=receiver;
		this.timestamp=timestamp;
		this.variable_values=variable_values;
		this.r_values=r_values;
	}
	
	public Function getSender(){
		return sender;
	}
	
	public Variable getReceiver(){
		return receiver;
	}
	
	public int getTimeStamp(){
		return timestamp;
	}
	
	public ArrayList<Float> getVariableValues(){
		return variable_values;
	}

	public ArrayList<ArrayList<ArrayList<Float>>> getRValues(){
		return r_values;
	}
	
	public String toString(){
		return "type r: " + this.sender + " to " + this.receiver + " t = " + timestamp;// + ", values: " + this.r_values;
	}
}
