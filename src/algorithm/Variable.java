package algorithm;

import java.util.ArrayList;
import java.util.Vector;

import Debug.Debug;

public class Variable {
	private String name;
	private int objectives;
	private Domain domain;
	private ArrayList<Function> neighbours;

	private Vector<RMessage> rmessages;

	private ArrayList<Marginal> marginalfunctions;
	private Solution solution;

	public Variable(String name, Domain domain, int objectives, Solution solution){
		this.name=name;
		this.objectives=objectives;
		this.domain=domain;
		this.neighbours=new ArrayList<Function>();

		this.rmessages=new Vector<>();

		this.marginalfunctions=new ArrayList<Marginal>();
		this.solution=solution;
	}

	public String getName(){
		return this.name;
	}

	public Domain getDomain(){
		return this.domain;
	}

	public void addNeighbour(Function f){
		neighbours.add(f);
	}

	public ArrayList<Function> getNeighbours(){
		return this.neighbours;
	}

	public int getObjectivesCount(){
		return this.objectives;
	}

	public Solution getSolution(){
		return this.solution;
	}

	public synchronized void sendZeroQMessage(Function receiver, int timestamp/*, Messages messages*/){
		//Messaggio q relativo alla prima iterazione, i cui q_values sono un array di 0
		ArrayList<Float> temp1 = new ArrayList<Float>();
		for(int o=0;o<objectives;o++)
			temp1.add((float) 0);
		ArrayList<ArrayList<Float>> temp2 = new ArrayList<ArrayList<Float>>();
		temp2.add(temp1);
		ArrayList<ArrayList<ArrayList<Float>>> q_values = new ArrayList<ArrayList<ArrayList<Float>>>();
		for(int i=0;i<domain.getValues().size();i++){
			q_values.add(temp2);
		}
		/*System.out.println("Messaggio q" + this.name + "->" + receiver.getName() + " t=" + timestamp);
		for(int i=0;i<domain.getValues().size();i++){
			System.out.println(domain.getValues().get(i) + " | " + q_values.get(i));
		}*/

		QMessage q = new QMessage(this, receiver, timestamp, domain.getValues(), q_values);		
		neighbours.get(neighbours.indexOf(receiver)).receiveQMessage(q);
	}


	public Vector<RMessage> getRMessages(){
		return this.rmessages;
	}

	public ArrayList<Marginal> getMarginalFunctions(){
		return this.marginalfunctions;
	}

	public Marginal getMarginalFunctionOfIteration(int timestamp){
		for(Marginal m:marginalfunctions)
			if(m.getTimeStamp()==timestamp)
				return m;
		return null;
	}

	@SuppressWarnings("unchecked")
	public synchronized void sendQMessage(Function receiver, int timestamp/*, Messages messages*/){
		ArrayList<ArrayList<ArrayList<Float>>> q_values = new ArrayList<ArrayList<ArrayList<Float>>>();
		ArrayList<ArrayList<Float>> q_line;
		ArrayList<ArrayList<Float>> r_line;
		ArrayList<ArrayList<Float>> temp;

		//Recupero i messaggi r che mi servono per calcolare q
		ArrayList<RMessage> rlist = getRMessageListExceptQReceiverInIteration(receiver, timestamp);

		//Ciclo ogni valore del mio dominio
		for(int i=0;i<domain.getValues().size();i++){
			//q_line contiene la somma parziale dei messaggi r
			q_line = (ArrayList<ArrayList<Float>>) rlist.get(0).getRValues().get(i).clone();
			for(int j=1;j<rlist.size();j++){
				temp = new ArrayList<ArrayList<Float>>();
				r_line = (ArrayList<ArrayList<Float>>) rlist.get(j).getRValues().get(i).clone();
				//sommo q_line a rline e metto il risultato in temp
				for(ArrayList<Float> ql:q_line)
					for(ArrayList<Float> rl:r_line)
						temp.add(sumVectors(ql,rl));
				q_line = (ArrayList<ArrayList<Float>>) temp.clone();
			}
			q_values.add(q_line);
		}

		/*if(timestamp==1){
		System.out.println("Messaggio q" + this.name + "->" + receiver.getName() + " t=" + timestamp);
		for(int i=0;i<domain.getValues().size();i++){
			System.out.println(domain.getValues().get(i) + " | " + q_values.get(i));
		}
		}*/

		QMessage q = new QMessage(this, receiver, timestamp, domain.getValues(), q_values);

		//FIXME credo che il deadlock si formi qui: infatti la variabile this tenta di modificare 
		// la funzione relativa a receiver, ma se in quell'istante receiver sta lavorando e attende 
		// il messaggio dalla varibile che ha appena chiamato receiveQmessagge allora si forma il deadlock!
		neighbours.get(neighbours.indexOf(receiver)).receiveQMessage(q);
	}	

	//FIXME questo metodo � un po' assurdo, viene chiamato da Function per inviare un messaggio a Variable
	//Dato che lo fa andando a scrivere direttamente su una variabile, se Variable fosse in attesa di Function 
	// allora si formerebbe un deadlock!!
	public synchronized void receiveRMessage(RMessage r){
		this.rmessages.add(r);
		notify();
		//XXX probabilmente uno dei due nella versione originaria non c'era!
		notifyAll();
		
		//System.out.println("THREAD " + Thread.currentThread().getName() + "| notifica ricezione messaggio r" + r.getSender().getName() + "->" + r.getReceiver().getName() + "; t = " + r.getTimeStamp());
	}

	public synchronized void QReady(Function receiver, int timestamp){
		//Controllo di aver ricevuto tutti i messaggi r con timestamp precedente da tutti i nodi funzione vicini meno il receiver
		@SuppressWarnings("unchecked")
		ArrayList<Function> af = (ArrayList<Function>)neighbours.clone();
		if(af.size()>1)
			af.remove(receiver);
		boolean qready = false;
		while(!qready){

			for(Function f:af){

				if(!checkRMessageReceived(f,timestamp-1)){
					try{
						Debug.print("THREAD " + Thread.currentThread().getName() + 
								"| aspetto il messaggio r" + f.getName() + this.getName(), false);
						//System.out.println("THREAD " + Thread.currentThread().getName() + "| aspetto il messaggio r" + f.getName() + this.getName());
						wait();
						break;
					}
					catch(InterruptedException e){
						e.printStackTrace();
					}
				}
				qready = true;

				//				Debug.print("THREAD " + Thread.currentThread().getName() + 
				//						"| ho ricevuto tutti i messaggi r necessari", false);
				//System.out.println("THREAD " + Thread.currentThread().getName() + "| ho ricevuto tutti i messaggi r necessari");
			}	
		} 
	}

	/**
	 * Checks if the variable {@code this} already received the {@code rMessage} from f.
	 * @param f
	 * @param timestamp
	 * @return
	 */
	private synchronized boolean checkRMessageReceived(Function f, int timestamp){
		for(RMessage r:rmessages){
			if(r.getSender().equals(f) && r.getTimeStamp()==timestamp)
				return true;
		}
		return false;
	}

	private ArrayList<RMessage> getRMessageListExceptQReceiverInIteration(Function receiver, int timestamp){
		ArrayList<RMessage> rlist = new ArrayList<RMessage>();
		for(RMessage r:rmessages)
			if(r.getTimeStamp() == timestamp-1 && !r.getSender().equals(receiver))
				rlist.add(new RMessage(r.getSender(),r.getReceiver(),r.getTimeStamp(),r.getVariableValues(),r.getRValues()));			
		return rlist;
	}

	private ArrayList<RMessage> getRMessageListInIteration(int timestamp){
		ArrayList<RMessage> rlist = new ArrayList<RMessage>();
		for(RMessage r:rmessages){
			if(r.getTimeStamp() == timestamp){
				rlist.add(new RMessage(r.getSender(),r.getReceiver(),r.getTimeStamp(),r.getVariableValues(),r.getRValues()));
			}
		}
		return rlist;
	}

	private ArrayList<Float> sumVectors(ArrayList<Float> a1, ArrayList<Float> a2){
		ArrayList<Float> sum = new ArrayList<Float>();
		for(int i=0;i<a1.size();i++)
			sum.add(a1.get(i)+a2.get(i));
		return sum;
	}

	public synchronized void computeMarginalFunction(int timestamp){
		while(getRMessageListInIteration(timestamp).size()!=this.neighbours.size()){
			try{
				wait();
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.marginalfunctions.add(new Marginal(this.name, getRMessageListInIteration(timestamp),timestamp));
		notify();		
	}

	public synchronized void addLastMarginalToSolution(int iterations){
		this.solution.addMarginal(this.getMarginalFunctionOfIteration(iterations));
	}

	public synchronized RMessage getRMessageFromFunctionAtIteration(Function f, int timestamp){
		for(RMessage r:this.rmessages)
			if(r.getSender().equals(f) && r.getTimeStamp()==timestamp)
				return r;
		return null;
	}

	public synchronized boolean checkInvariantMarginals(int last_iteration){
		Marginal m1 = this.getMarginalFunctionOfIteration(last_iteration);
		Marginal m2 = this.getMarginalFunctionOfIteration(last_iteration-1);
		return (m1.getMarginalValues().equals(m2.getMarginalValues()));
	}

	/**
	 * @author Stefano
	 */
	public String toString(){
		return this.getName();
	}

	public synchronized void QReady2(int timestamp) {
		//Controllo di aver ricevuto tutti i messaggi r con timestamp precedente da tutti i nodi funzione vicini meno il receiver
		@SuppressWarnings("unchecked")
		ArrayList<Function> af = (ArrayList<Function>)neighbours.clone();
		
		@SuppressWarnings("unchecked")
		ArrayList<Function> remainFunctions = (ArrayList<Function>)neighbours.clone();
		
		while(remainFunctions.size() > 0){
			for(Function f : af){
				if(checkRMessageReceived(f, timestamp - 1)){
					remainFunctions.remove(f);
				}
				else {
					try{
						Debug.print("THREAD " + Thread.currentThread().getName() + 
								"| aspetto il messaggio r" + f.getName() + this.getName(), true);

						wait();
						break;
					}
					catch(InterruptedException e){
						e.printStackTrace();
					}
				}
			}			
		}
	}
}
